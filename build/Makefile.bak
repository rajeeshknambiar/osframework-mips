#/********************************************************************************
# *										*
# *	Copyright (c) 2007, 2008 Rajeesh					*
# *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
# *	Copyright (c) 2005 Varghese Mathew					*
# *										*
# *	This program is free software; you can redistribute it and/or		*
# *	modify it under the terms of the GNU General Public License		*
# *	as published by the Free Software Foundation; either			*
# *	version 3 of the License, or (at your option) any later version.	*
# *										*
# *	This program is distributed in the hope that it will be useful,		*
# *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
# *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
# *	General Public License for more details.				*
# *										*
# ********************************************************************************/
#
#  Rajeesh
# 16/01/2006

CC		= g++
LD		= g++
RM		= rm
CFLAGS		= -g  -Wsign-promo -Wabi -D__WITH_COLOR
LEX		= flex
YACC		= bison
THREADLIBS	= -lpthread
YACCLIBS	= -lfl

COLOR_H		= ../include/color.h
INSTR_H		= ../include/instruction.h
TYPES_H		= ../include/types.h
OPCODE_H	= ../include/opcodes.h
GLOBAL_H	= ../include/globalvars.h
SYSCALL_H	= ../include/syscalls.h
EXEHEADER_H	= ../include/exeheader.h
INCLUDE_ALL	= $(COLOR_H) $(INSTR_H) $(OPCODE_H) $(TYPES_H) $(GLOBAL_H) $(SYSCALL_H) $(EXEHEADER_H)

MIPS_H		= ../mips/latch.h \
		../mips/memory.h \
		../mips/portmanager.h \
		../mips/processor.h \
		../mips/simple_cache.h \
		../mips/translate.h \
		../mips/main.h
MIPS_C		= ../mips/latch.cpp \
		../mips/main.cpp \
		../mips/memory.cpp \
		../mips/pclock.cpp \
		../mips/portmanager.cpp \
		../mips/processor.cpp \
		../mips/pstage0.cpp \
		../mips/pstage1.cpp \
		../mips/pstage2.cpp \
		../mips/pstage3.cpp \
		../mips/pstage4.cpp \
		../mips/simple_cache.cpp \
		../mips/translate.cpp
MIPS_O		= latch.o main.o memory.o pclock.o portmanager.o processor.o pstage0.o \
		pstage1.o pstage2.o pstage3.o pstage4.o simple_cache.o translate.o 


KERNEL_H	= ../kernel/addressspace.h \
  		../kernel/kernel.h \
		../kernel/errorhandler.h \
		../kernel/thread.h 
KERNEL_C	= ../kernel/addressspace.cpp \
		../kernel/kernel.cpp \
		../kernel/errorhandler.cpp \
	        ../kernel/thread.cpp
KERNEL_O	=  addressspace.o kernel.o errorhandler.o thread.o

LIB_H		= ../lib/list.h
LIB_C		= ../lib/list.cpp
LIB_O		= list.o


ASM_H		= ../asm/structures.h
ASM_HPP		= ../asm/asm.tab.hpp
ASM_YPP		= ../asm/asm.ypp
ASM_O		= asm.tab.o lexer.o assemble.o structures.o

MACHINE		= mips
ASSEMBLER	= asm
IO		= dumbterminal

all: $(MACHINE) $(ASSEMBLER) $(IO) 

########## MIPS ( Machine ) ##########
	
$(MACHINE): $(MIPS_O) $(KERNEL_O) $(LIB_O) ISR
	      $(CC) $(CFLAGS)  $(MIPS_O) $(KERNEL_O) $(LIB_O) $(THREADLIBS) -o $(MACHINE)
main.o:	../mips/main.cpp $(MIPS_H) $(COLOR_H) ../kernel/addressspace.cpp
	$(CC) $(CFLAGS) -c ../mips/main.cpp 
addressspace.o: ../kernel/addressspace.cpp ../kernel/addressspace.h ../mips/main.h
	$(CC) $(CFLAGS) -c ../kernel/addressspace.cpp
simple_cache.o: ../mips/simple_cache.cpp $(MIPS_H) $(INSTR_H) $(COLOR_H)
	$(CC) $(CFLAGS) -c ../mips/simple_cache.cpp
memory.o: ../mips/memory.cpp $(INCLUDE_ALL)
	$(CC) $(CFLAGS) -c ../mips/memory.cpp
portmanager.o: ../mips/portmanager.cpp $(TYPES_H) $(COLOR_H)
	$(CC) $(CFLAGS) -c ../mips/portmanager.cpp
latch.o: ../mips/latch.h ../mips/latch.cpp
	$(CC) $(CFLAGS) -c ../mips/latch.cpp
processor.o: ../mips/processor.cpp $(MIPS_H) $(INCLUDE_ALL)
	$(CC) $(CFLAGS) -c ../mips/processor.cpp
pclock.o: ../mips/pclock.cpp $(MIPS_H) $(INCLUDE_ALL)
	$(CC) $(CFLAGS) -c ../mips/pclock.cpp
pstage0.o: ../mips/pstage0.cpp $(MIPS_H) $(INCLUDE_ALL)
	$(CC) $(CFLAGS) -c ../mips/pstage0.cpp
pstage1.o: ../mips/pstage1.cpp $(MIPS_H) $(INCLUDE_ALL)
	$(CC) $(CFLAGS) -c ../mips/pstage1.cpp
pstage2.o: ../mips/pstage2.cpp $(MIPS_H) $(INCLUDE_ALL)
	$(CC) $(CFLAGS) -c ../mips/pstage2.cpp
pstage3.o: ../mips/pstage3.cpp $(MIPS_H) $(INCLUDE_ALL)
	$(CC) $(CFLAGS) -c ../mips/pstage3.cpp
pstage4.o: ../mips/pstage4.cpp $(MIPS_H) $(INCLUDE_ALL)
	$(CC) $(CFLAGS) -c ../mips/pstage4.cpp
translate.o: ../mips/translate.cpp 
	$(CC) $(CFLAGS) -c ../mips/translate.cpp
kernel.o: ../kernel/kernel.cpp $(KERNEL_H) 
	$(CC) $(CFLAGS) -c ../kernel/kernel.cpp
thread.o: ../kernel/thread.cpp $(KERNEL_H)
	$(CC) $(CFLAGS) -c ../kernel/thread.cpp
errorhandler.o: ../kernel/errorhandler.cpp $(KERNEL_H)
	$(CC) $(CFLAGS) -c ../kernel/errorhandler.cpp
list.o:  ../lib/list.cpp $(LIB_H)
	$(CC) $(CFLAGS) -c ../lib/list.cpp
ISR:	../mips/vector.mips
	../test/asm ISR ../mips/vector.mips
	

########## Assembler ##########
	
$(ASSEMBLER): $(ASM_O)
	$(CC) $(CFLAGS) $(ASM_O) $(YACCLIBS) -o $(ASSEMBLER)
assemble.o: ../asm/assemble.cpp $(ASM_H) $(COLOR_H)
	$(CC) $(CFLAGS) -c ../asm/assemble.cpp
asm.tab.o: ../asm/asm.tab.hpp ../asm/asm.tab.cpp $(COLOR_H)
	$(CC) $(CFLAGS) -c ../asm/asm.tab.cpp
lexer.o: lexer.cpp $(ASM_HPP) $(COLOR_H)
	$(CC) $(CFLAGS) -c lexer.cpp
../asm/asm.tab.cpp: $(ASM_YPP) $(ASM_H) $(TYPES_H) $(INSTR_H) $(OPCODE_H)
	$(YACC) -t $(ASM_YPP) -o ../asm/asm.tab.cpp
../asm/asm.tab.hpp: $(ASM_YPP) $(ASM_H) $(TYPES_H) $(INSTR_H) $(OPCODE_H)
	$(YACC) -dt $(ASM_YPP) -o ../asm/asm.tab.hpp
lexer.cpp: ../asm/lexer.lex
	$(LEX)  -olexer.cpp ../asm/lexer.lex
structures.o: ../asm/structures.cpp $(ASM_H) $(COLOR_H)
	$(CC) $(CFLAGS) -c ../asm/structures.cpp

############## I/O #############

$(IO): dumbterminal simplescreen simplekeyboard 

dumbterminal: ../io/dumbterminal.cpp $(TYPES_H) $(COLOR_H)
	$(CC) $(CFLAGS) ../io/dumbterminal.cpp $(THREADLIBS) -o dumbterminal
simplescreen: ../io/simplescreen.cpp $(TYPES_H) $(COLOR_H)
	$(CC) $(CFLAGS) ../io/simplescreen.cpp $(THREADLIBS) -o simplescreen
simplekeyboard: ../io/simplekeyboard.cpp $(TYPES_H) $(COLOR_H)
	$(CC) $(CFLAGS) ../io/simplekeyboard.cpp $(THREADLIBS) -o simplekeyboard

############ CLEAN ##############

clean:
	$(RM) -f *.o $(MACHINE) $(ASSEMBLER) $(IO) \
	../asm/asm.tab.cpp ../asm/asm.tab.hpp ../asm/lexer.cpp lexer.cpp\
	dumbterminal simplescreen simplekeyboard 

