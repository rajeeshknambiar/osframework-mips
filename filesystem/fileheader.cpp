/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
//-----------------------------------12/04/2006-------------------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */


/*  --------The implementation routines for managing  the fileheader .---------------*/


#include "fileheader.h"
#include "disk.h"
#include "routines.h"
#include "main.h"

//--------Allocate is to initialize a fresh file header for a newly created file.
//--------and to allocate data blocks for the file out of the map of free disk blocks.



bool FileHeader::Allocate(BitMap *freeMap, int fileSize)
{

	    DEBUG (dbgFile, skyblue <<"\n[FileHeader::Allocate] Allocating the file header");
	    numBytes = fileSize;
            numSectors  = divRoundUp(fileSize, SectorSize);
	    if (freeMap->NumClear() < numSectors)
	            return false;           // not enough space

            for (int i = 0; i < numSectors; i++) 
	    {
	        dataSectors[i] = freeMap->FindAndSet();
					        // since we checked that there was enough free space,
					        // we expect this to succeed
	        ASSERT(dataSectors[i] >= 0);
	   }
	    return true;
}

//------De-allocate all the space allocated for data blocks for this file.--------------

void FileHeader::Deallocate(BitMap *freeMap)
{

	    DEBUG (dbgFile, red <<"\n[FileHeader::Deallocate] Deallocating the file header");
	    for (int i = 0; i < numSectors; i++) 
	    {
	            ASSERT(freeMap->Test((int) dataSectors[i]));  // ought to be marked!
	            freeMap->Reset((int) dataSectors[i]);
	    }
}

//-----------fetch the fileheader form disk--------------------------

void FileHeader::FetchFrom( int sectorNum )
{
	
	DEBUG (dbgFile, "\n[FileHeader::Fetch] Fetching the file header from the sector "<< sectorNum );
	Disk *disk = kernel->disk;
	//disk->disksem->P();				//waiting for interrupt..... call semaphore->V();
	disk->ReadRequest(sectorNum, (char *)this);
//	disk->disksem->V();				//waiting for interrupt..... call semaphore->V();
	
}


//------------Write the fileheader back to disk------------------

void FileHeader::WriteBack(int sectorNum )
{

	DEBUG (dbgFile ,"\n[FileHeader::WriteBack] Writing the file header back into the sector "<< sectorNum );

	Disk *disk = kernel->disk;
	//call global fn that calls disk write request.

	//disk->disksem->P();                     // wait for interrupt .... semaphore->V() to be called
	disk->WriteRequest(sectorNum, (char *)this);
//	disk->disksem->V();				//waiting for interrupt  call semaphore->V() !!
	
}

//---------find the sector which has the particular byte---------------

int FileHeader::ByteToSector(int byteNo)
{
	    return(dataSectors[byteNo / SectorSize]);
}

//-----------length of the file in bytes!!----------------
int FileHeader::FileLength()
{
	    return numBytes;
}


//---------------To get the contents of the file header--------------

void FileHeader::Print()
{

	
}


//------------Initiualzing the currently opened file ------------------
CurrentFile::CurrentFile(int sectorno)
{
	hdr = new FileHeader();
	hdr->FetchFrom( sectorno );
	seekpos = 0;               // Initially this pointer wud be 0
}


//----------Closing the opened file---------------------------

CurrentFile::~CurrentFile()
{
	delete hdr;  // deallocating the header!!
}


//---------- Go to specified postion----------------
void CurrentFile::Seek(int position)
{
	seekpos = position;
	
}

//----------Read 'numBytes' bytes from an opened file-------------------
int CurrentFile::Read(char *data, int numBytes)
{

	DEBUG (dbgFile ,"\n[CurrentFile::Read] Reading "<< numBytes <<" bytes from the current file" );
	int result = ReadPos(data, numBytes, seekpos);
	seekpos += result;
	return result;
}

//--------Write 'numBytes' bytesinto an opened file---------------------

int CurrentFile::Write(char *data, int numBytes)
{
	DEBUG (dbgFile ,"\n[CurrentFile::Write] Writing "<< numBytes <<" bytes into the current file" );
	int result = WritePos(data, numBytes, seekpos);
     	seekpos += result;
	return result;
}


//-------Read 'numbytes' bytes  from the opened file  from a specified postion-----------------

int CurrentFile::ReadPos(char *data, int numBytes, int position)
{
	int filelength = hdr ->FileLength();
	Disk *disk = kernel->disk;

	int i, firstsector, lastsector, numsectors;
	char *buffer;

	if ((numBytes <= 0)||(position < filelength)) 
		return 0;				//request not correct

	if ((position + numBytes) > filelength)
	        numBytes = filelength - position;       // max no of bytes from position till end of file


	DEBUG (dbgFile ,"\n[CurrentFile::ReadPos] Reading "<< numBytes 
			<<" bytes from the current file from the postion "<<position );
	        // find the first, last sector numbers and number of sectors to be read
	firstsector = divRoundDown(position, SectorSize);
	lastsector = divRoundDown(position + numBytes - 1, SectorSize);
	numsectors = 1 + lastsector - firstsector;

	buffer = new char[numsectors * SectorSize];
	
	for (i = firstsector; i <= lastsector; i++)
	{
		//disk->disksem->P();                     // wait for interrupt .... semaphore->V() to be called
		disk->ReadRequest(hdr->ByteToSector(i * SectorSize), &buffer[(i - firstsector) * SectorSize]);
		//disk->disksem->V();                     // wait for interrupt .... semaphore->V() to be called
		    
	}


	  memcpy( data ,&buffer[position - (firstsector * SectorSize)] ,numBytes);  //copy 'numbytes'

			
	  delete [] buffer;
	  return numBytes;
}


//---------Write 'numBytes' bytes into the opened file from the specified postion-------------
int CurrentFile::WritePos(char * data, int numBytes, int position)
{
	
	
	Disk *disk = kernel->disk;
	int filelength = hdr ->FileLength();
        int i, firstsector, lastsector, numsectors;
	bool firstaligned, lastaligned;
        char *buffer;

	if ((numBytes <= 0)||(position < filelength))
                return 0;                               //request not correct

        if ((position + numBytes) > filelength)
                numBytes = filelength - position;		// then max no of bytes possible.

	DEBUG (dbgFile ,"\n[CurrentFile::WritePos] Writing "<< numBytes 
			<<" bytes into the current file from the postion "<<position );

	// find fisrt, last sectors and no of sectors!!
        firstsector = divRoundDown(position, SectorSize);
        lastsector = divRoundDown(position + numBytes - 1, SectorSize);
	numsectors = 1 + lastsector - firstsector;

 	buffer = new char[numsectors * SectorSize];

	
	firstaligned = (position == (firstsector * SectorSize));
	lastaligned = ((position + numBytes) == ((lastsector + 1) * SectorSize));


	// If the first and last sectors are to be modified, read them!!
	if (!firstaligned)
        	ReadPos(buffer, SectorSize, firstsector * SectorSize);
	if (!lastaligned && ((firstsector != lastsector) || firstaligned))
	         ReadPos(&buffer[(lastsector - firstsector) * SectorSize],SectorSize, lastsector * SectorSize);

       for (i = firstsector; i <= lastsector; i++)
       {

//		disk->disksem->P();                     // wait for interrupt .... semaphore->V() to be called
	        disk->WriteRequest(hdr->ByteToSector(i * SectorSize), &buffer[(i - firstsector) * SectorSize]);
//		disk->disksem->V();                     // wait for interrupt .... semaphore->V() to be called
       }
              


       memcpy( &buffer[position - (firstsector * SectorSize)], data ,numBytes);  //copy 'numbytes'

       delete [] buffer;
       return numBytes;
		
			
}

int CurrentFile::Length()
{
	 return hdr->FileLength();
}


