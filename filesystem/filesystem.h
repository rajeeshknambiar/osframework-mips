/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
//----------------------------12/04/2006----------------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */


/*-----This file contains the datastructures to represent the ------------
  ----filesystem.*/

#include "copyleft.h"

#ifndef __FILESYSTEM_H
#define __FILESYSTEM_H

#include "fileheader.h"
#include "routines.h"
#include "directory.h"
#include "bitmap.h"
#include "debug.h"



class FileSystem
{
	private:
		CurrentFile *bitmapFile;              //File for bitmap
		CurrentFile *directoryFile;	      //File for directory.
	
	public:
		FileSystem(bool tocreate);
		bool Create(char *filename, int size);
		CurrentFile* Open(char *filename);
		bool Remove(char *filename);
		void List();
};


#endif //__FILESYSTEM_H

  
