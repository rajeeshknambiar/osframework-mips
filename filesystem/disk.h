/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
//---------------------11/04/2006-------------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */


/*  This file contains datastructures to  emulate the physical disk.*/
#include "copyleft.h"

#ifndef __DISK_H
#define __DISK_H

#include "routines.h"
#include "sem.h"
#include "interrupt.h"
#include "errorhandler.h"



const int SectorSize = 128;             // number of bytes per disk sector
const int SectorsPerTrack  = 32;        // number of sectors per disk track
const int NumTracks = 32;               // number of tracks per disk
const int NumSectors = (SectorsPerTrack * NumTracks);   // total number of sectors per disk

const int DiskSize = (sizeof(int) + (NumSectors * SectorSize));

/*class Sector_Data
{
	public:
		int sector;
		char *ptr ;

		Sector_Data()
		{ sector = -1 ; ptr = NULL ; }

		Sector_Data( int sec, char* p )
		{ sector = sec ; ptr = p ; }
};*/

class Disk : public Observer
{
	
	private:
		int fileNo;        //Unix file number for this disk. The disk is another file
		char *diskName; // Name of the disk.
		bool operation;    // Whether some operation is going on
		int lastrequest;   // The number of the sector which was last requested.
		void UpdateLast( int sectorNumber);

		//List<Sector_Data*> *sector_data_List;
		
	public :

		Disk();
		~Disk();
		void Notify( int intcode );

		Semaphore *disksem ;
			
		// To read from or  write into a sector of the disk
		void ReadRequest(int sectorNumber, char* data);
		void WriteRequest(int sectorNumber, char* data);
		
		
			
};
	
#endif //__DISK_H
