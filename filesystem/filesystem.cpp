/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
//--------------------12/04/2006------------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

/*-----Implementation of the filessytem routines!!-------------------*/


#define NumDirEntries	10
#define FreeMapFileSize         (NumSectors / BitsInByte)
#define DirectoryFileSize       (sizeof(DirectoryEntry) * NumDirEntries)

#define BitmapSector 0
#define DirSector 1

#include "filesystem.h"

FileSystem::FileSystem(bool tocreate)
{
	if (tocreate)
	{
		DEBUG(dbgFile , skyblue<<"\nFormatting the filesystem");
		BitMap *bmap = new BitMap(NumSectors);
		Directory *directory = new Directory(NumDirEntries);
		FileHeader *mapHeader = new FileHeader();
		FileHeader *dirHeader = new FileHeader();


		bmap->Set(BitmapSector);            // Set the bits 0 and 1 for 
		bmap->Set(DirSector);		   // bitmap and directory!!


		//Now allocate the file headers for bitmap and directory
		ASSERT(mapHeader->Allocate(bmap, FreeMapFileSize));
		ASSERT(dirHeader->Allocate(bmap, DirectoryFileSize));

		//Write the fileheaders back to disk 
		mapHeader->WriteBack(BitmapSector);
		dirHeader->WriteBack(DirSector);


		// Initialise the files for bitmap and directory
		bitmapFile = new CurrentFile(BitmapSector);
		directoryFile = new CurrentFile(DirSector);
		
		//Write back the bitmap and directory into disk
//		bmap->WriteBack(bitmapFile);
		bitmapFile->WritePos((char *)(bmap->map), (bmap->numWords) *sizeof(unsigned) , 0);
		directory->WriteBack(directoryFile);
		
		delete bmap;
		delete directory;
		delete mapHeader;
		delete dirHeader;
			
	}
	else    // we needn't create the filesystem!! just open the bitmap and directory.
	{
		bitmapFile = new CurrentFile(BitmapSector);
		directoryFile = new CurrentFile(DirSector);
	}
}

  
//----------Create the file ------------------------------------

bool FileSystem::Create(char *filename, int filesize)
{
	DEBUG(dbgFile , skyblue<<"\nCreating the filesystem");
	bool status;
	Directory *directory = new Directory(NumDirEntries);
 	BitMap *bmap;
        FileHeader *hdr;
        int sectorno;


	directory->FetchFrom(directoryFile);

	if (directory->FindSector(filename)!= -1)     // check if file is alerady present
		status = false;
	else
	{
		bmap = new BitMap( NumSectors);
		sectorno = bmap->FindAndSet();

		if (sectorno == -1) status = false; // no free block
		
		else if (!directory->Enter(filename, sectorno)) status = false; // no space in directory.!!

		else 
		{
			hdr = new FileHeader();
			if(!hdr->Allocate( bmap, filesize)) 
				status = false;  // no space in disk!!
			else
			{
				status = true; // file to be created !!
				hdr->WriteBack(sectorno);
		                directory->WriteBack(directoryFile);
		               //bmap->WriteBack(bitmapFile);
				bitmapFile->WritePos((char *)(bmap->map), (bmap->numWords) *sizeof(unsigned) , 0);
				
								
			}
			delete hdr;
		}		
		delete bmap;
			
	}
	delete directory;
	return status;
 
}

//-----------------Open a file for reading and writing---------------------------

CurrentFile* FileSystem::Open(char *filename)
{
	DEBUG(dbgFile , "\n[CurrentFile::Open] Opening the file " << filename);
	Directory *directory = new Directory(NumDirEntries);
	CurrentFile *currentFile = NULL;
	int sectorno;

    	directory->FetchFrom(directoryFile);
	sectorno = directory->FindSector(filename);
    	if (sectorno >= 0)
	        currentFile = new CurrentFile(sectorno);        // name was found in directory
    delete directory;
    return currentFile;     
}



//-----------Remove a file from the filesystem-----------------------------------

bool FileSystem :: Remove(char *filename)
{
	DEBUG(dbgFile , "\n[CurrentFile::Remove] Deleting the file "<<filename);
	Directory *directory = new Directory(NumDirEntries);
	BitMap *bmap;
	FileHeader *hdr;

	int sectorno;

	directory->FetchFrom(directoryFile);
	sectorno = directory->FindSector( filename); // find sector where file is located!!
	if (sectorno == -1) 
	{
	       delete directory;
	       return false;                     // file not found
  	}
	
	hdr = new FileHeader;
	hdr->FetchFrom(sectorno);

	bmap = new BitMap(NumSectors);

	hdr->Deallocate(bmap);             // Delete space for the header
	bmap->Reset(sectorno);		   // Delete space from datablocks 	
	directory->Remove( filename);	   // Delete filename from the directory

//	bmap->WriteBack(bitmapFile);            // writing back the bitmap
	bitmapFile->WritePos((char *)(bmap->map), (bmap->numWords) *sizeof(unsigned) , 0);
	
	
	directory->WriteBack(directoryFile);    // writing back the directory
	
	delete hdr;
        delete directory;
        delete bmap;

    	return true;

}

//------List all the files in the file system directory.----------------------

void FileSystem::List()
{
	DEBUG( dbgFile , violet << "\nListing the filesystem entries " );

	Directory *directory = new Directory(NumDirEntries);
	directory->FetchFrom(directoryFile);
	directory->List();
	delete directory;
}

