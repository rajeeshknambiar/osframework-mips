/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
//---------------------------------11/04/2006-----------------------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */


/*  The implementation of the physical disk emulation.*/

#include <cstring>
using std::strcpy;

#include "disk.h"

//--------------------Constructing the disk!!------------------
Disk::Disk()
{
	int temp = 0;
	diskName = new char(15);
	strcpy(diskName, "Disk_emulation");
	lastrequest = 0;
	
	fileNo = OpenForReadWrite(diskName, false);
	DEBUG (dbgFile, skyblue <<"\nInitializing the disk");
	if (fileNo <= 0)
	{
		fileNo = OpenForWrite(diskName);
				// need to write at end of file, so that reads will not return EOF
		Lseek(fileNo, DiskSize - sizeof(int), 0);
		Write(fileNo, (char *)&temp, sizeof(int));
	}
	operation = false;
	
	
	DEBUG (dbgFile, skyblue <<"\nCreating Semaphore disksem");
	disksem = new Semaphore("disk_sem", 0);			
}

Disk::~Disk()
{

	DEBUG (dbgFile, red << "\nDestroying the disk! ");
	Close( fileNo );
}


#include "main.h"
#include "list.cpp"

//----------------ReadRequest to read from a sector of the disk ---------------------//

void Disk::ReadRequest(int sectorNumber, char* data)
{
//	ASSERT(!operation);                            // only one request at a time
        ASSERT((sectorNumber >= 0) && (sectorNumber < NumSectors));
	
	operation = true;
	Interrupt *diskInt = new Interrupt(this, clockTick() + 5 ,DISK_READ_INT);
	kernel ->IRQ -> Insert ( diskInt );
//	kernel ->IRQ -> Insert ( new Interrupt(this, clockTick() + 5 ,DISK_READ_INT) );
//	sector_data_List->Append( new Sector_Data(sectorNumber,data) );	
//	diskInt = NULL ;
//	tempdata = NULL;
//	delete diskInt;
//	delete tempdata;
	
	disksem->P();
	DEBUG (dbgFile, skyblue <<"\n[Disk::ReadRequest] Reading from the disk sector number "<< sectorNumber);
	Lseek(fileNo, SectorSize * sectorNumber + sizeof(int), 0);
	Read(fileNo, data, SectorSize);
	UpdateLast( sectorNumber);
	//disksem->V();		// 14/11/2008. We shouldn't V() here, as the interrupt scheduled will do that
						
}
//--------------WriteRequest to write into a scetor of the disk--------------------//
void Disk::WriteRequest(int sectorNumber, char* data)
{
//	return;
//	ASSERT(!operation);                            // only one request at a time
	ASSERT((sectorNumber >= 0) && (sectorNumber < NumSectors));

	operation = true;
	Interrupt *diskInt = new Interrupt(this, clockTick() + 5 ,DISK_WRITE_INT);
	ASSERT( diskInt != NULL );
	kernel ->IRQ -> Insert ( diskInt );
//	kernel ->IRQ -> Insert ( new Interrupt(this, clockTick() + 5 ,DISK_WRITE_INT) );
//	sector_data_List->Append( new Sector_Data(sectorNumber,data) );
//	diskInt = NULL ;
//	tempdata = NULL;
//	delete diskInt;
//	delete tempdata;	
	
	DEBUG (dbgFile, skyblue <<"\n[Disk:: WriteRequest] Scheduled write on the disk sector number "<< sectorNumber);
	disksem->P();	// Comment on 04/02/2008 by Rajeesh - the V() for this P() will be done by the
			// CallBack() of diskInt interrupt. Only after diskInt is serviced, remaining
			// code will execute. So actual write will be done only asynchronously

	DEBUG (dbgFile, skyblue <<"\n[Disk:: WriteRequest] Wrote into the disk sector number "<< sectorNumber);
	Lseek(fileNo, SectorSize * sectorNumber + sizeof(int), 0);
	Write(fileNo, data, SectorSize);
	UpdateLast( sectorNumber);
	//disksem->V();  // Commented out on 04/02/2008 by Rajeesh and put in the Notify() section
   
}

//--------------------To update the last sector accessed------------------------
void Disk::UpdateLast(int newSector)
{
	DEBUG (dbgFile, skyblue <<"\n[Disk:: UpdateLast] Updating the sector which was last requested");
	lastrequest = newSector;
	//operation = false ;
}

void Disk::Notify( int intcode )
{

	disksem->V();		// Release the disksem so that actual Write happens
	
	operation = false;	// Another WriteRequest can be scheduled now
}
