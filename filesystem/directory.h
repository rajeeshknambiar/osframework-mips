/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
//-------------------------------------12/04/2006--------------------------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

/*-------------This file contains the datastructures to make and manage a--------------
  ------------directory structure, The directory is a special file that ---------------
  ------------contains the details of the files in it, like the filename , ------------
  ------------the sectornumber where we wud find the file header, a bit ---------------
  -------------indicating whether the entry is valid , etc-----------------------------*/


#include "copyleft.h"

#ifndef __DIRECTORY_H
#define __DIRECTORY_H

#include "fileheader.h"
#include <string.h>
#include "debug.h"


class DirectoryEntry
{
	public:
		string fileName;
		int sectorNo;
		bool valid;
};

class Directory
{
	private:
		DirectoryEntry *dirTable;         // The table containing the directory entries.
		int tableSize;			   // The size of the table.
	public:
		Directory(int numEntries);
		~Directory();

		void FetchFrom( CurrentFile *filename);
		void WriteBack( CurrentFile *filename);


		bool Enter(char *filename, int sectorno);
		bool Remove(char *filename );
		int FindSector(char *filename );
		
		int GetIndex(char *filename);
		void List();

		
};

		
#endif //__DISK_H

