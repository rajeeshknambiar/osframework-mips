/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
//-------------------------------12/04/2005---------------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */


/*------------The datastructures for a file header. The file header of a file is ----------
  ------------similar to the inode of unix files. It describes where in the disk----------
  ------------to find the file, its length etc.!!----------------------------------------*/



#include "copyleft.h"

#ifndef __FILEHEADER_H
#define __FILEHEADER_H

#include "bitmap.h"
#include "disk.h"
#include "debug.h"


//-----------The  file header is created as a table of pointers to the data blocks.------------
//--------As per the design , the fileheader should fit into a single data sector -----------
//--------of the disk.

class FileHeader
{

	private:
		int numBytes;				//no of bytes in the file
		int numSectors;				//no of sectors occupied
		int dataSectors[30];			//the sector numbers occupied by 
							//individual blocks of the file.
							// The size 30 is the max no of blocks.
			//{the fileheader must fit into the sector; sectorsize=128;
			// numBytes and numSectors wud take 4 bytes each. so remaining
			// 128 - 8 bytes for the datasectors. Each entry wud require 4 bytes.
			// Hence 120/4 = 30 is the maximum no of sectors possible by direct addressing!!!}

	public:
		bool Allocate(BitMap *bitmap , int filesize);
		void Deallocate(BitMap *bitmap);
		void FetchFrom(int sectorNum);
		void WriteBack(int sectorNum);
		int ByteToSector(int byteNum);

		int FileLength();
		void Print();
			
		
}; 


class CurrentFile
{
       private:
                FileHeader *hdr;
                int seekpos;
       public:
                CurrentFile(int sectorno);
               ~CurrentFile();
	       
		void Seek(int position);            

		int Read(char *into, int numBytes);
	        int Write(char *from, int numBytes);

		int ReadPos(char *into, int numBytes, int position);
	        int WritePos(char *from, int numBytes, int position);

		int Length();   

};

#endif // __FILEHEADER_H
