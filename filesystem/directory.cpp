/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
/*-------------------------------12/04/2006---------------------------------------
  -----------The implementation of the routines for the directory structure--------*/

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */


#include "directory.h"


//---------- Initializing the directory structure,the  valid bits set to 0--------
Directory::Directory(int numEntries)
{
	DEBUG(dbgFile , skyblue<<"\nInitializing the directory");
	dirTable = new DirectoryEntry[numEntries];
	tableSize = numEntries ;
	 for (int i = 0; i < tableSize; i++)
	         dirTable[i].valid = false;
	 
	
}

//----------Deallocating the directory table---------------------------------------

Directory::~Directory()
{
	DEBUG(dbgFile , red << "\nDeleting the directory");
	delete [] dirTable;
}

//----------- Read the contents of the directory from disk.----------------------

                                                                                                                             
void Directory::FetchFrom(CurrentFile *file)
{
	DEBUG(dbgFile , "\n[Directory::FetchFrom] Fetching from the directory" );
	(void) file->ReadPos((char *)dirTable, tableSize * sizeof(DirectoryEntry), 0);
}
                                                                                                                             
//--------- Write any modifications to the directory back to disk-------------------
                                                                                                                             
void Directory::WriteBack(CurrentFile *file)
{
	DEBUG(dbgFile , "\n[Directory::WriteBack] Writing back to the directory");
	    (void) file->WritePos((char *)dirTable, tableSize * sizeof(DirectoryEntry), 0);
}
                                                                                                                             
//----------Look up file name in directory, and return its location in the table of
//---------- directory entries.  Return -1 if the name isn't in the directory.
                                                                                                                             
int Directory::GetIndex(char *filename)
{
	DEBUG(dbgFile , "\n[Directory::GetIndex] Getting the index of the file" << filename );
    for (int i = 0; i < tableSize; i++)
           //if (dirTable[i].valid && !strncmp(dirTable[i].fileName.c_str(), filename, 9))//  filename max lenght = 9
           if (dirTable[i].valid && (dirTable[i].fileName == string(filename)) )//  filename max lenght = 9
	                return i;
     return -1;          // name not in directory
}

//--------------Find the sector number of the file-------------------------------

int Directory::FindSector(char *filename)
{
      DEBUG(dbgFile ,"\n[Directory::FindSector] Finding the sector where the header of the file " << filename<<" is" );
     int i = GetIndex(filename);
     if (i != -1)
          return dirTable[i].sectorNo;
     return -1;
}

//-------------Enter the 'filename' into the directory . The file header is in the sector 'sectorno'!
bool Directory::Enter(char *filename, int sectorno)
{
      DEBUG(dbgFile  ,"\n[Directory::Enter] Entering the file "<<filename <<" into the directory");
	if (GetIndex(filename) != -1)
	           return false; // The directory already consist of a file with the name
	                                                                                                        
	for (int i = 0; i < tableSize; i++)
	       if (!dirTable[i].valid) 
	       {
		     dirTable[i].valid = true;
		     //strncpy(dirTable[i].fileName, filename, 9);
		     dirTable[i].fileName = string( filename );
		     
		     dirTable[i].sectorNo = sectorno;
		     return true;
		}
	return false;       // no space.  Fix when we have extensible files.
	

}

//------------Remove the 'filename' from the directory---------------------------
bool Directory::Remove(char *filename)
{
      DEBUG(dbgFile ,"\n[Directory::Remove] Removing the file "<<filename <<" from the directory");
	int i=GetIndex(filename);
	if (i == -1)
		return false;
	else
	{
		dirTable[i].valid =false;
		return true;
	}
}
	
//----------------List the directory contents-------------------------

void Directory::List()
{
	for (int i = 0; i < tableSize; i++)
	       if (dirTable[i].valid)
			printf("%s\n", dirTable[i].fileName.c_str() );
	
}
