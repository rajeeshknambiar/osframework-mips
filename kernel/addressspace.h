/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

//----------------------- 19/01/2006 ---------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

#include "copyleft.h"

#ifndef __ADDRSPACE_H
#define __ADDRSPACE_H

/*
   This header file contains the declarations of the classes for
   the implementation of User Level Contexts. The AddressSpace class
   is the primary construct used for this purpose.
*/


#include "globalvars.h"
#include "types.h"
#include "translate.h"

class AddressSpace {

	public:
		TranslationEntry *pageTable;	// The page table used for address mapping
		u_word_32 noOfPages;		// Total no of pages the user program needed
                word_32 userRegister[32];
		AddressSpace();
		~AddressSpace();
		bool Load( const char *exeFile );	// Load an executable file into machine memory
		bool Execute();			// Execute the loaded project
		void SaveUserRegisters();		// Save the context during a context switch.
		void RestoreUserRegisters();
};
	
#endif // __ADDRSPACE_H
