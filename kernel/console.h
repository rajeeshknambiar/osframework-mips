/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
//----------------------15/04/2006-----------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */


#ifndef	__CONSOLE_H
#define __CONSOLE_H

#include "copyleft.h"
#include "portmanager.h"
#include "sem.h"
//#include "main.h"
#include "interrupt.h"
#include "errorhandler.h"

class Console : public Observer
{

	public :
		Console(PortManager *pm);
		~Console();

		int ConsoleRead(int portno, char & data );

		int ConsoleWrite( const char *data );

		PortManager *pMan;

		void Notify( int intcode );

		Semaphore *consolesem ;

		
	
};

  
#endif	// __CONSOLE_H
