/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
/*----------------27/03/2006----------------------
  errorhandler.h----------------------------------*/

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

#ifndef __ERROR_H
#define __ERROR_H

//TranslationEntry transEntry ;

void TLBMissHandler( int Iterator, int badVirtualAddress );

void ErrorHandler( ExceptionType exception ) ;

//void SysCallHandler();

//void SignalTimerInterrupt();	// Put somewhere else, or rename...?	09/04/2006

// These are the interface functions bet the kernel and cache !!

ExceptionType Kernel_Memory_Read(word_32 address ,word_32 &value , int noOfBytes);

ExceptionType Kernel_Cache_Read(word_32 address ,word_32 &value , int noOfBytes);

// Thses are  the interface between the kernel and the memory !! 


ExceptionType Kernel_Memory_Write(word_32 address ,word_32 value , int noOfBytes);

ExceptionType Kernel_Cache_Write(word_32 address ,word_32 value , int noOfBytes); 

int clockTick();

#endif //ERROR_H
