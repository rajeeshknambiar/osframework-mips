/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */


#ifndef __THREAD_H
#define __THREAD_H

#include "copyleft.h"
#include "addressspace.h"
#include "debug.h"
using std::string ;

//Status of a thread could be either of the following
enum ThreadStatus{ NEW , READY , RUNNING , WAIT };

/*
   This header file has the thread details for the operating system module 
 */

class Thread
{
	private :
		string threadName;		// Thread's name 
		ThreadStatus status;		// Status of the thread is to be stored
		int UserRegisters[REGNOS];	// To save the register values on a thread switch

	public:
		Thread(const char * name);	// Initialize the thread
		~Thread();			// Destructor

		const char * getName() { return threadName.c_str() ; };
		
//		int Createthread();		//Create the thread 
		
		void SetStatus( ThreadStatus stat);	// Set the status of the thraed
		ThreadStatus GetStatus();	// get the status of the thread
		
		void Fork();			// To fork the thread
	       
		void Yield();			// To yeild the thread for another one

		void Sleep( bool ending );	// Temporarily or Permanently sleep the thread

		void Finish();			// Finish the execution of thread

		void SaveUserState();		// Save all the registers on a thread switch
		void RestoreUserState();	// Restore all the registers on a thread switch

	        AddressSpace * addrspace;	// Address Space for the thread 

		//---- Modified on 29/01/2008 by Rajeesh ---//
		word_32 *threadStack;		// User Stack for the thread
		word_32 *stackTop;		// Pointer to Top of Stack
		bool StackAllocate();		// This method actually allocates a Stack
		//------------------------------------------//
};

#endif	// __THREAD_H
