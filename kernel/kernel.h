/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
//---------------09/03/2006---------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

/*  The kernel structure of the operating system*/

#include "copyleft.h"

//----------------------------- 26/03/2006 --------------------------------//
//-----------------------------   Rajeesh  --------------------------------//

/*
   This header file contains all the classes and declarations related to the
   Operating System Kernel. The crux of kernel is the Scheduler, which could
   be enhanced and expanded to adopt any policy for thread switching; along
   with a primitive File System manager, Memory manager, Network manager which
   basically supports the TCP/IP networking, and an I/O manager.
*/

#ifndef __KERNEL_H
#define __KERNEL_H

#include "thread.h"		// For Thread class
#include "interrupt.h"		// For Interrupt class

#include "list.h"	// For List class
#include "types.h"
#include "globalvars.h"	// For Addresses of Memory to which Registers are mapped

#include "syscalls.h"

#include "debug.h"

#include "network.h"
#include "filesystem.h"
#include "disk.h"
#include "console.h"

//#define TLBEntryAddr 	1000


class Scheduler
{
	private:
		List <Thread*> *readyQ;
		List <Thread*> *waitingQ;
	public:
		Scheduler();		// The constructor
		~Scheduler();		// Destructor

		bool AddToReadyQ ( Thread *newThread );	// Add a new process to ready queue
		bool MoveToWaitingQ ( Thread *aThread );// Move a process to waiting queue
		//--- Modified on 04/02/2008 by Rajeesh ---//
		bool RemoveFromReadyQ( Thread *aThread );	// Remove a thread from ready queue
		bool RemoveFromWaitQ( Thread *aThread );	// Remove a thread from waiting queue
		//-----------------------------------------//
		Thread* NextToRun();			// Finds Next Thread to Run from ready queue
		int RunThread( Thread *nextThread , bool ending ); // Actual scheduling - Run the thread
								   // specifying currentThread is ending or not
		//void SwitchThread(Thread *oldThread , Thread *newThread );
};
	       	


class Kernel
{
	public:
		Scheduler *scheduler;
		Thread *currentThread;
		FileSystem *fileSystem;	
		Disk *disk;
		Network *network;
		Console  *console;
		//--- 04/02/2008 - Rajeesh ---//
		AddressSpace *addrSpace;
		//----------------------------//

		SortedList <Interrupt*> *IRQ;

		
		Kernel();
		~Kernel();
		
		int Initialize();

		//--- Added by Rajeesh on 29/01/2008 ---//
		void AtExit();		// Routine to call by the destructor to safely exit
		//--------------------------------------//
		
		//word_32 ReadRegister(int reg);
		//void WriteRegister(int reg, int value);
		void TLBMissHandler(int Iterator, int badVirtualAddress );
		void SysCallHandler();
		void TimerInterruptHandler();
		void InterruptHandler( int code );

		bool NetworkTest();		// Test the network for proper working
};


#endif	// __KERNEL_H_
