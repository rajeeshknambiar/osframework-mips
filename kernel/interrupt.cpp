/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

//-------------------------- 16/04/2006 --------------------------//
//--------------------------   Rajeesh  --------------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */


#include "interrupt.h"
#include <iostream>

Interrupt :: Interrupt(Observer *obj, int when, int code)
{
	time = when;
	intCode = code;
	object = obj;
}

Interrupt :: ~Interrupt()
{
	if( object )
		object = NULL ;
}

bool Interrupt :: operator == ( Interrupt another )
{
	if( this->time == another.time &&
		this->intCode == another.intCode &&
		this->object == another.object )
		return true;
	return false;
}
		

int InterruptCompare ( Interrupt *x , Interrupt *y )
{
	if ( x->time < y->time )
		return -1; 
	else if ( x->time > y->time )
		return  1;
	else
		return 0;
}


int InterruptCompare ( Interrupt x , Interrupt y )
{
	if ( x.time < y.time )
		return -1; 
	else if ( x.time > y.time )
		return  1;
	else
		return 0;
}

