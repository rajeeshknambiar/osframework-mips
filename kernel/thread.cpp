/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

#include "debug.h"
#include "thread.h"

#include "main.h"	//	For scheduler and kernel ...

/* 
   The thread routines are expanded and given here 
   */

Thread :: Thread( const char * name)
{
	DEBUG( dbgThread , green << "\n[Thread::Thread] Creating thread - " << name );
	threadName = string(name);
        addrspace = NULL;
	status = NEW;
        for( int i ; i<REGNOS; ++i)
		UserRegisters[i] = 0;

	//--- Added on 29/01/2008 by Rajeesh ---//
	if ( ! StackAllocate() ){
		threadStack = NULL;
		stackTop    = NULL;
	}
	//--------------------------------------//
}

Thread :: ~Thread()
{
//	scheduler -> RemoveThread ( this ); 	//to remove the thread for ever
	DEBUG(dbgThread , red << "\nTerminating thread " << threadName.c_str() << " !!");
	if( addrspace )
		delete addrspace;
	if( threadStack )
		delete threadStack;	
}

//----------- Added on 29/01/2008 by Rajeesh -------------//

// We need to allocate stack to the thread and initialize it
bool Thread :: StackAllocate()
{
	threadStack = new word_32 (32); // Stack of 32 words
	stackTop    = &threadStack[31]; // Stack grows downwards in x86
	if ( threadStack ) {
		DEBUG( dbgThread , green << "\n[Thread::StackAllocate] Allocated Stack for\
			       		" << threadName.c_str() );
		return true;
	}
	DEBUG( dbgThread , red << "\n[Thread::StackAllocate] Stack Allocate failed for \
				" << threadName.c_str() );
	return false;
}

//--------------------------------------------------------//

void Thread :: SetStatus( ThreadStatus stat)
{
	status=stat;
}

ThreadStatus Thread :: GetStatus()
{
	return(status);
}

void Thread :: Fork()
{
//	status = READY ;		// In the beginning the thread has the Ready state when forked
	ASSERT ( this == kernel -> currentThread );
	
	DEBUG(dbgThread , violet << "\n[Thread :: Fork] Forking the thread " << threadName.c_str() );
	kernel -> scheduler -> AddToReadyQ( this );		// Scheduler adding of the thread
	return;
}

void Thread :: Finish()
{
	ASSERT( this == kernel->currentThread );
	DEBUG(dbgThread , red << "\n[Thread :: Finish] Finishing the thread " << threadName.c_str() );
	Sleep( true );
}

void Thread :: Sleep( bool ending )
{
	DEBUG(dbgThread , violet << "\n[Thread::Sleep] Sleeping Thread " << threadName.c_str() );
	ASSERT( this == kernel -> currentThread );
//	status = WAIT ;			// The thread is transferred to the wait state
	kernel -> scheduler -> MoveToWaitingQ ( this );	// Scheduler is made to move the thread the waiting Q
	Thread * temp;
	while( !(temp = kernel -> scheduler -> NextToRun()) )	// Here, we have to allow interrupts to run
		kernel->InterruptHandler( 0 );	// Here, 0 doesn't mean anything. We are checking if any interrupts
						// are to be served or not - Rajeesh on 04/02/2008
	//	scheduler -> AddToReadyQ ( temp );
	kernel -> scheduler -> RunThread ( temp , ending );	
	return;
}

#include "errorhandler.h"

void Thread :: SaveUserState()
{
	for( int i = 0 ; i <  80 ; i = i + 4 )
		Kernel_Memory_Read( PC_Addr + i , UserRegisters[i], 4 );
	DEBUG(dbgThread, violet << "\n[Thread::SaveUserState] Saving User state of the thread "<<threadName.c_str());
	//std::cout << "\n UserRegister[ PC_Addr ] = " << UserRegisters[0] << std::flush; 
}
	
void Thread :: RestoreUserState()
{
	for( int i = 0 ; i <  80 ; i = i + 4 )
		Kernel_Memory_Write( PC_Addr + i , UserRegisters[i], 4 );
	DEBUG(dbgThread, violet << "\n[Thread::RestoreUserState] Restoring User state of the thread "<<threadName.c_str());
}

void Thread :: Yield()
{
	ASSERT ( this == kernel ->currentThread );
	Thread *temp;
	if( (temp = kernel -> scheduler -> NextToRun()) )
	{
		kernel -> scheduler -> AddToReadyQ ( this );
		
		kernel -> scheduler -> RunThread ( temp , false );
	}
	DEBUG(dbgThread , violet "\n[Thread :: Yield] Yeilding the thread " << threadName.c_str() );

}
