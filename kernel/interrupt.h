/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
//------------------------------- 16/04/2006 ------------------------------//
//-------------------------------   Rajeesh  ------------------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

#ifndef __INTERRUPT_H
#define __INTERRUPT_H

#include "copyleft.h"

#define	DISK_READ_INT		0
#define DISK_WRITE_INT		1
#define CONSOLE_READ_INT	2
#define CONSOLE_WRITE_INT	3
#define	TIMER_INT		4

class Observer
{
	public:
		virtual void Notify( int intcode ) = 0 ;
		Observer() {};
		virtual ~Observer() {};
};


class Interrupt
{

	public:
		int time;
		int intCode;
		Observer *object;

		Interrupt(Observer *obj, int when, int code);
		~Interrupt();

		bool operator == ( Interrupt another );
};


int InterruptCompare( Interrupt *x , Interrupt *y ) ;
int InterruptCompare( Interrupt x , Interrupt y ) ;

#endif	// __INTERRUPT_H
