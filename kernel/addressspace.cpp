/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

//------------------------ 19/01/2006 --------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

/* 
   The AddressSpace class implementation comes out of mist
   in the following few lines of code. The Page table setting up and loading
   the OS understandable executable code.
*/

//#include "globalvars.h"			// for TotalPhysPages, redundant
#include "addressspace.h"
#include "types.h"
#include "color.h"

#include "errorhandler.h"

#include "debug.h"				// 04/02/2008 - Rajeesh
						// this file is using cout directly !!

AddressSpace :: AddressSpace()
{
	DEBUG( dbgAddr, skyblue << "\nCreating new AddressSpace and pageTable"<<reset);
	pageTable = new TranslationEntry[TotalPhysPages];

	for( register u_word_32 i = 0; i < TotalPhysPages ; i ++ )
	{
		pageTable[i].virtualPage  = i;
		pageTable[i].physicalPage = i;
		pageTable[i].valid = true;
		pageTable[i].use   = false;
		pageTable[i].dirty = false;
		pageTable[i].readOnly = false;
	}
	
	noOfPages = 0 ;
}


AddressSpace :: ~AddressSpace()
{
	DEBUG( dbgAddr, red << "\nDestroying AddressSpace and pageTable"<< reset);
	if ( pageTable != NULL )
		delete []pageTable;
	noOfPages = 0 ;
}


#include "exeheader.h"			// for exeHeader
#include "instruction.h"	// for OneRecord
#include <fstream>
#include <iostream>
using namespace std;

#include "main.h" 		//extern MainMemory *mem;

#include "globalvars.h"	// For PageSize

//#define	SYSTEM_START_ADDRESS	1024	// ---------

bool AddressSpace :: Load( const char *exeFile )
{
	ifstream progfile;
       	progfile.open ( exeFile, ios::in | ios::binary );
	if( ! progfile )
	{
		cout << "Error : exectable file cannot be opened !!\n";
		return false;
	}

	exeHeader header;
	if( ! progfile.read( (char*) &header, sizeof(exeHeader) ) )
	{
		cout << "Error : Header of executable file could not be found !!\n";
		return false;
	}

	//u_word_32 size = header.totalSize + StackSize ;
	u_word_32 size = header.totalSize;

	noOfPages = static_cast<u_word_32> ( size/PageSize );
	
	if ( ((float)size/PageSize) - (int)(size/PageSize) > 0 )
		noOfPages ++;


	//u_word_32 virtAddress = header.virtualAddress;
	u_word_32 virtAddress = SYSTEM_START_ADDRESS - 4 ;	// '-4' to avoid first record, which is START

	//cout << "File size : " << size << ", virtual address : " << virtAddress << "\n";

	//u_word_32 physAddress;

	OneRecord rec;
	
	if( ! progfile.read( (char*) &rec, sizeof(OneRecord) ) )
	{
		cout << red << "\n[AddressSpace :: Load] Error !!" << reset << flush;
		return false;
	}                                                       	
	
	while( progfile.read( (char*) &rec, sizeof(OneRecord) ) )
	{
//		mem->Write( virtAddress + rec.address - 4  , rec.inst.iV , 4);
		Kernel_Memory_Write (virtAddress + rec.address , rec.inst.iV , 4) ;
	}
	progfile.close();
	return true;

}

void AddressSpace::SaveUserRegisters()
{
	
//	    for (int i = 0; i < 32 ; i++)
//		 userRegister[i] = proc -> ReadRegister(i);
}


void AddressSpace::RestoreUserRegisters()
{
//	for(int i=0; i<32;i++)
//		proc -> WriteRegister( i, userRegister[i]);
}
