/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
//----------------08/03/2006------------------------//
/* This file conssists of the error handling routins*/

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */


#include "copyleft.h"
#include "addressspace.h"
//#include "globalvars.h"
#include "syscalls.h"
#include "translate.h"
#include "main.h"
#include "color.h"

using std::cout;
using std::flush;


void TLBMissHandler( int Iterator, int badVirtualAddress )
{
	/* 23/01/2008
	 * Rajeesh
	 *   We need an alternative mechanism for not referring to either CP0 or kernel here
	 */

	// Write back the entry being replaced...
	if( CP0->TLB[Iterator].virtualPage != -1 && CP0->TLB[Iterator].physicalPage != -1 &&
			CP0->TLB[Iterator].valid == true )	// Write back TLB Entry
	{
		// modified on 04/02/2008 by Rajeesh
		// addrSpace was global, which we changed to member of Kernel
		kernel->addrSpace->pageTable[CP0->TLB[Iterator].virtualPage].use = CP0->TLB[Iterator].use;
		kernel->addrSpace->pageTable[CP0->TLB[Iterator].virtualPage].dirty = CP0->TLB[Iterator].dirty;
	}
	kernel->TLBMissHandler( Iterator, badVirtualAddress );
}


//----------------------- 15/04/2006 -------------------------//
//-----------------------   Rajeesh  -------------------------//

void ErrorHandler( ExceptionType exception )
{
	switch ( exception )
	{
		case SYSCALL :

			kernel->SysCallHandler();
			break;

		case RESERVED_INSTRUCTION :
			
			//delete kernel;		// To be refined by transfering control to kernel
			proc -> Terminate();
			break;

		case TIMERINTERRUPT :

			//kernel->TimerInterruptHandler();
			kernel->InterruptHandler( TIMER_INT );
			break;

		case TRAP :
			
			proc -> Terminate();	// To be refined by transfering control to kernel
			break;

		case OVERFLOW :
			
			delete kernel;		// To be refined by transfering control to kernel
			proc -> Terminate();
			break;

		case DIVIDE_ERROR :

			 delete kernel;          // To be refined by transfering control to kernel
			 proc -> Terminate();
			 break;

		case ALIGNMENTERROR :

			delete kernel;		// To be refined by transfering control to kernel
			proc -> Terminate();
			break;

		case BADADDRESS :
			delete kernel;		// To be refined by transfering control to kernel
			proc -> Terminate();
			break;

		case NOEXCEPTION:		// No need to handle NOEXCEPTION and TLBMISS here;
		case TLBMISS:			// just to mute gcc on -Wall
			break;
			
	}
}


/*void SysCallHandler()
{
	kernel->SysCallHandler();
}*/


//--------------------------------------------------------------------------------//
//-------------------   30-02-06   -----------------------------------------------//

ExceptionType Kernel_Cache_Write( word_32 address , word_32 value , int noOfBytes )
{
	// CAUTION :	Supports writing only 4 bytes , no 1,2 bytes support

	return proc -> WriteMem ( address,value ,noOfBytes ) ;

	// This function in turn calls SimpleCache::Write rather than Memory::Write
}

ExceptionType Kernel_Cache_Read( word_32 address , word_32 &result , int noOfBytes )
{
	// Supports reading 4 bytes only
	return proc -> ReadMem ( address, result, noOfBytes ) ;
	
	// This function in turn calls SimpleCache::Read rather than Memory::Read
}

ExceptionType Kernel_Memory_Write( word_32 address, word_32 value, int noOfBytes )
{
	return mem -> Write ( address, value, noOfBytes);

	// Calls the MainMemory::Write, direct interaction with memory - no Cache in between
}

ExceptionType Kernel_Memory_Read( word_32 address , word_32 &result , int noOfBytes )
{
	return mem -> Read ( address, result, noOfBytes) ;
	// Reading directly from MainMemory, without using Cache
}
//--------------------------------------------------------------------------------//


//-------------------------------- 09/04/2006 ------------------------------------//
//--------------------------------   Rajeesh  ------------------------------------//

/*void SignalTimerInterrupt ()
{
	//	What to do...?
	//sem_wait( cout_mutex );
	//cout << red << "\n\tTimer Interrupt Occured !! \n" << reset ;
	//sem_post( cout_mutex ); 

	//proc -> SetException( false );

	kernel -> TimerInterruptHandler();
	
	return;

}*/

//--------------------------------------------------------------------------------//

extern int clock_count ;	// In globalvars.h

int clockTick()
{
	return ( proc -> clock_count );
}
