/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
/*--------------------------15/04/2006--------------------------*/

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

#include "console.h"
#include "debug.h"
#include "main.h"

Console::Console(PortManager *pm)
{
	DEBUG(dbgNet, skyblue <<"\nInitializing the Console Input");
	pMan = pm;	
	consolesem = new Semaphore( "console_sem" , 1);
}

Console::~Console()
{
	DEBUG(dbgNet, skyblue <<"\nDeleting the Console Input");
	if( pMan )
		pMan = NULL ;
	
}

int Console::ConsoleRead(int portNo,char & data)
{

	
	Interrupt * consoleInt = new Interrupt(this, clockTick() + 5, CONSOLE_READ_INT);
	kernel -> IRQ -> Insert( consoleInt );
	//delete consoleInt;
	
	DEBUG(dbgNet, skyblue <<"\nReading from  the Console Input");
	consolesem->P();
	if(pMan->Read( portNo, reinterpret_cast<word_32 &>(data) ) < 0 )
		return -1;
	consolesem->V();
	return 0;
		
	
}


int Console::ConsoleWrite( const char *data )
{

	Interrupt * consoleInt = new Interrupt(this, clockTick() + 5, CONSOLE_WRITE_INT);
	kernel -> IRQ -> Insert( consoleInt );
//	delete consoleInt;

	DEBUG(dbgNet, skyblue <<"\nWriting into the Console Output");
	int i = 0 ;
	do
	{

		consolesem->P();
		if (pMan->Write( 2 , static_cast<word_32>(data[i]) ) < 0)
			return -1;
		consolesem->V();
	} while( data[i++] != '\0' );
	
	return 0;
}

void Console :: Notify( int intcode )
{
	consolesem ->V();
}
