/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
/*--------------09/03/2006----------------------
  The routines for the kernel!!*/

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */


#include "copyleft.h"
#include "kernel.h"
//#include "types.h"

//------------------------------ 20/03/2006 -------------------------------//
//------------------------------   Rajeesh  -------------------------------//

#include "list.cpp"	// Include - otherwise 'Undefined references to List::' occur...
#include "main.h"
#include "errorhandler.h"

#include "debug.h"


//-------------------- Scheduler ----------------------

Scheduler :: Scheduler ()
{
	DEBUG( dbgThread , skyblue << "\nScheduler Initialized" );
	readyQ = new List <Thread*>;
	waitingQ = new List <Thread*>;
}


Scheduler :: ~Scheduler ()
{
	DEBUG( dbgThread , red << "\nDestroying Scheduler..." );
	if ( readyQ )
		delete readyQ;
	if ( waitingQ )
		delete waitingQ;	
}

bool Scheduler :: AddToReadyQ ( Thread *newThread )
{
	DEBUG( dbgThread , green << "\n[Scheduler::AddToReadyQ] Adding thread " << newThread->getName() 
	    			<< " to Ready Queue" );
	newThread->SetStatus( READY );
	return readyQ -> Append( newThread );
}

bool Scheduler :: MoveToWaitingQ ( Thread *aThread  )
{
	DEBUG( dbgThread , violet << "\n[Scheduler::MoveToWaitingQ] Moving thread " << aThread->getName() 
				<< " to Waiting Queue" );
	aThread->SetStatus ( WAIT );
	return waitingQ -> Append( aThread );
}

//--- Modified on 02/04/2008 by Rajeesh ---//
bool Scheduler :: RemoveFromReadyQ( Thread *aThread )
{
	DEBUG( dbgThread , violet << "\n[Scheduler::RemoveFromReadyQ] Removing thread " << aThread->getName()
	    			<< " from Ready Queue" );
	if( readyQ -> IsEmpty() )
		return false;
	else
		return readyQ -> Remove( aThread );
}

bool Scheduler :: RemoveFromWaitQ( Thread *aThread )
{
	 DEBUG( dbgThread , violet << "\n[Scheduler::RemoveFromWaitingQ] Removing thread " << aThread->getName()
			 	<< " from Waiting Queue" );
	 if( waitingQ -> IsEmpty() )
		 return false;
	 else
		 return waitingQ -> Remove( aThread );
}


Thread* Scheduler :: NextToRun ()
{
	if( readyQ -> IsEmpty() )
		return NULL;
	else
		return readyQ -> RemoveFront();
	DEBUG( dbgThread , violet << "\n[Scheduler::NextToRun] Finding next thread to run from ReadyQueue" );
}

int Scheduler :: RunThread (Thread *nextThread , bool ending )	
{
	DEBUG(dbgThread, green << "\n[Scheduler::RunThread] Next Thread let to run !!");
	
	Thread *oldThread = kernel -> currentThread ;
	if( oldThread == nextThread )	// Can this cause problem somewhere...?
		return 0;
	if( oldThread -> addrspace != NULL )
		oldThread -> SaveUserState();

	nextThread -> RestoreUserState();	// Set all the registers including PC, EPC etc.
	kernel -> currentThread = nextThread ;	// Set the currentThread
	nextThread -> SetStatus ( RUNNING );	// Set status to RUNNING...

	//SwitchThread( oldThread , nextThread );
	if( ending == true )		// ie, if oldThread is ending
		delete oldThread ;
//	else
//		AddToReadyQ ( oldThread );
	
	return 0;
}

// ---------------------------------- 06-04-06 ----------------------------------

/*
void Scheduler :: SwitchThread( Thread *oldThread , Thread *newThread ) //to store the register contents to the stach space 
{
	//---------------- 09/04/2006 -------------------//
	//	Just save all the registers of oldThread and load the
	//	register values of newThread...
//	SaveRegisters();	// Should save all the registers includeing that of CP0
//--------------	Rajeesh - 29/01/2008 	-------------//
//	Uncomment this Method, and make a MIPS routine (similar to TLBWrite) to execute here
//	by changing the PCReg (ofcourse through Kernel_Memory_Write in PCRegAddress)
//-----------------------------------------------------------//
//	LoadRegisters();	// Load register values from newThread ...
	return;
}
*/
//-------------------------------------------------------------------------------


//---------------------------------- Kernel -------------------------------------


Kernel :: Kernel ()
{
	scheduler = NULL ;
	currentThread = NULL ;
	network = NULL ;
	fileSystem = NULL;
	disk = NULL;
	console = NULL;

	IRQ = NULL;
}

//-------- Modified on 29/01/2008 by Rajeesh ----------//

Kernel :: ~Kernel ()
{
	AtExit();
}

void Kernel :: AtExit()
{
	if( network )
		delete network;
	if( fileSystem )
		delete fileSystem;
	if( disk )
		delete disk;
	if( console )
		delete console;
	if( IRQ )
		delete IRQ;
	if( scheduler )
		delete scheduler;
	if( thread )
		delete thread;
}

//-----------------------------------------//

//	Initializing all the components of kernel and loading the first user program

#include "color.h"
using std::flush;

int Kernel :: Initialize ()
{
	
	addrSpace = new AddressSpace();
	DEBUG(dbgAddr, skyblue << "\nAddress Space Initialized !!");

	network = new Network();	// Network driver module of kernel
	DEBUG(dbgNet, skyblue << "\nNetwork Driver Initialized !!");
	//NetworkTest();
        
	scheduler = new Scheduler();	// Create the Scheduler for Kernel
	
	IRQ = new SortedList <Interrupt*>(InterruptCompare);	// Create an Interrupt Request Queue
	
        thread = new Thread("mainthread");      // Create a new thread as mainthread
	DEBUG(dbgThread, skyblue << "\nMain Thread Initialized");
	
        if( addrSpace == NULL )
	{
	          cout << "Could not create address space for boot program.Quitting..\n";
	               return -2;
	}
	if( ! addrSpace->Load ( "a.out" ) )     
	{                                      
              cout << red << "\nError, could not load the \"a.out\" program..."
                      << "\nTerminating... \n" << reset << flush;
            return -2;
	}
	
	thread->addrspace = addrSpace ;       //Assign the new address space to the threads address space
	//scheduler->AddToReadyQ ( thread );     //Add the thread to the ready queue
	currentThread = thread ;

	console = new Console(pMan);	// Create a Console to read/write

	disk = new Disk();		// Create Disk Driver
	
	fileSystem = new FileSystem(1);	// Create File System
					// Rajeesh on 04/02/2008 - Very Important. This calls Disk::WriteRequest
					// which in turn makes currentThread->Sleep() which puts current thread
					// in waitingQ. Since no other thread is in the readyQ, this will loop
					// forever. We need a mechanism to generate the interrupt it scheduled, which
					// will do a semaphore->V() and removing currentThread from waitingQ and put
					// it in readyQ. And, our machine hasn't started executing yet. So how to do that?
					// Nachos does this by doing InterruptIdle() in Thread::Sleep which will advance
					// the clock and allow the interrupt to be generated and served.
					// Gotcha! One thing we can do is, these interrupts are software interrupts, not
					// h/w interrupts and hence not machine generated. So we can serve them independent
					// of the machine. (hint : if machine hasn't started executing, serve them. If not
					// allow the machine to do it (don't know how feasible it is though...)
		
	if(  scheduler->RunThread ( thread , false )  == -1)//Run the thread
	{
		cout << red << "\nError, could not execute the \"a.out\" program..."
			<< "\nTerminating... \n" << reset << flush;
		return -2 ;
	}
	return 0 ;
}


/*word_32 Kernel :: ReadRegister(int reg)
{
	return 0;	// Do TODO
	
}

void Kernel :: WriteRegister(int reg, int value)
{
	return;	
}

*/


void Kernel :: InterruptHandler( int code )
{
	//Interrupt interrupt;
	//	For other interrupts that are in IRQ
	//DEBUG( dbgThread , violet << "\nServing Interrupts in IRQ" );
	//--- Added on 04/02/2008 by Rajeesh ---//
	//We have a problem with FileSystem() constructor in Kernel::Initialization.
	// It calls Disk::WriteRequest which /schedules Write for later time by creating
	// an interrupt for it. But, our Processor hasn't started executing yet, so clockTick()
	// will return 0. So, here we do an ugly hack to prevent this scenario. Later on, we can
	// think of a better solution by refining this or something...
	int now = clockTick();	// now = 0 means, Processor hasn't started executing yet,
       				// so serve any existing Interrupts immediately
	//-------------------------------------//
	
	while( ! IRQ->IsEmpty() && IRQ->Front() && (IRQ->Front()->time <= now || now == 0) ) // now == 0 added on 04/02/2008
	{
		DEBUG( dbgThread , violet << "\n[Kernel::InterruptHandler] Serving Interrupts in IRQ" );
		Interrupt *interrupt = IRQ -> RemoveFront(); 	// Remove the interrupt to be scheduled now
		interrupt->object -> Notify( interrupt->intCode );  // Notify the observer
		delete interrupt;		  	// Remove the interrupt from queue
	}
	//	For Timer interrupt
	if( code == TIMER_INT )
	{
		Thread *nextThread = scheduler->NextToRun() ;
		if( nextThread != NULL )
			scheduler->RunThread( nextThread , false );
		else
			scheduler->RunThread ( currentThread , false );
		//return;
	}
	
	return;

}

void Kernel :: TLBMissHandler( int Iterator, int badVirtualAddress )
{

	/* 23/01/2008
	 * Rajeesh
	 *   Need to change this. Here, we need to change the PC value of CP0
	 *   by means of writing to corresponding memory address. RestoreUserRegisters
	 *   machine routine will then set the actual PC register to this value then.
	 *   Moreover, we don't want badVirtualAddress to be passed in like this,
	 *   we can _read_ it from the corresponding memory address for BVADDR.
	 */

	/*
	sem_wait( cout_mutex );
	cout << red << "\n [Kernel::TLBMissHandler] Got called..." << reset;
	sem_post( cout_mutex );*/
	DEBUG(dbgThread ,violet << "\n [Kernel::TLBMissHandler] Got called...");
	
	TranslationEntry transEntry ;
	//static int Iterator;
	unsigned int virtualPageNo;
	
//	cout <<"\n The very bad virtual address is "<<badVirtualAddress<<"\n";

	//DEBUG('+',"\n The very bad virtual address is "<<badVirtualAddress);	
	
	virtualPageNo = badVirtualAddress / PageSize;
	transEntry.physicalPage = -1;
	transEntry.virtualPage = -1;
	if( virtualPageNo > addrSpace->noOfPages )
	{
	//	cout << "Referencing Memory out of bound...!!! ";
		DEBUG( dbgThread , red << "[Kernel::TLBMissHandler] Referencing Memory out of bound...!");
		return ;
		// exit
	}  
	
	// i think we shud do these steps using TLB write...!!
	//afer finding out the entry for the requested page in the pagetable of the address space of the 
	//particular process!!

//	cout << "\n[ TLBHandler ] :: VPN " << virtualPageNo << flush;

	//DEBUG(dbgMachine,"\n[ TLBHandler ] :: VPN " << virtualPageNo );
// 	
	//transEntry = new TranslationEntry();
	transEntry.physicalPage = currentThread->addrspace->pageTable[virtualPageNo].physicalPage;
	transEntry.virtualPage = currentThread->addrspace->pageTable[virtualPageNo].virtualPage;
	//cout <<" THE CALCULATED PAGE NOs "<<virtualPageNo << "  "<< transEntry.physicalPage <<"  "<<transEntry.virtualPage<<"\n";
	transEntry.readOnly = currentThread->addrspace->pageTable[virtualPageNo].readOnly;
	transEntry.dirty = false;
	transEntry.valid = true;
	transEntry.use = false;
//now we must update the entry register of the CP0 to transEntry
// Then the TLB entry, indexed by Iterator is updated to the values in Entry register.
//	Entry = transEntry;
//	CP0->TLB[Iterator]=transEntry; // to be done elsewhere!!
//	Iterator = (Iterator + 1) % TLBSIZE;
	//	Important - Don't Change the following order !!!

	u_word_32 entrylo0 = 0;
	entrylo0 = transEntry.valid; 	entrylo0 = entrylo0 << 8;
	entrylo0 |= transEntry.use;	entrylo0 = entrylo0 << 8;
	entrylo0 |= transEntry.dirty; 	entrylo0 = entrylo0 << 8;
	entrylo0 |= transEntry.readOnly;

	//cout << "\n\t entrylo0 = " << entrylo0;	
	
	Kernel_Cache_Write( ENTRYHI_Addr , transEntry.virtualPage , 4);		// Important to use Cache_Write, since 
	Kernel_Cache_Write( ENTRYLO1_Addr  , transEntry.physicalPage , 4);	// LW prior to TLBW instruction in ISR
	Kernel_Cache_Write( ENTRYLO0_Addr , entrylo0, 4);			// reads from Cache rather than Memory...
	//Kernel_Memory_Write( TLBEntryAddr + 8 , transEntry.readOnly, 1);
	//Kernel_Memory_Write( TLBEntryAddr + 9 , transEntry.dirty , 1);
	//Kernel_Memory_Write( TLBEntryAddr + 10 ,transEntry.use , 1);
	//Kernel_Memory_Write( TLBEntryAddr + 11 , transEntry.valid , 1);
	
}

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

//#include "debug.h"


void Kernel :: SysCallHandler ( )
{
	int code;
	Kernel_Memory_Read( (word_32)A0_Addr, code, 4);
	switch(code)
	{
		case SYSCALL_HALT:
			/*
			sem_wait( cout_mutex );
			cout << red  << "\n\n\n\t\tHalting the Execution...\n\n"<< reset;
			sem_post( cout_mutex );*/
			DEBUG( '+' , red  << "\n\n\t[Kernel::SysCallHandler] Halting the Execution...\n");


			proc->Terminate();	// Do it somehow other than this..!!
			
			AtExit();	// Destroy the Kernel
			//------- Rajeesh - 29/01/2008 --------//
			// OK, *DO NOT* call proc->Terminate() here. Alternatively? Make an MIPS
			// routine execute by changing the PCReg which executes instruction HALT?
			// ------------------------------------//

			break;

		case SYSCALL_OPEN:
			{
				
				word_32 address;
				Kernel_Memory_Read( (word_32)A1_Addr, address, 4);
				cout << gray <<  "\nThe value at A1       "<< address << reset ;
			
				address += SYSTEM_START_ADDRESS;	// To get the absolute memory location
			
				string fileName;
				int i = 0 /*, j = 0*/ ;				
				do
			  	 {

					Kernel_Cache_Read(  address, reinterpret_cast<word_32&>(fileName[i++]), 4);
					//j += 4;
					address += 4 ;
					
					/*sem_wait(cout_mutex);
					cout << green << fileName[i-1] << reset ;
					sem_post(cout_mutex); */
					//cout << red << "\n"<< i <<"th" << fileName <<reset ;
					
			  	 }while(fileName[i-1] != 0 );

				//cout << "\n\t filename : " << fileName  ;

				DEBUG( dbgThread , green << "\n\tFilename to open: " << fileName.c_str() );

				int fileId = open( fileName.c_str(), O_RDWR | O_APPEND | O_CREAT, 0700);
						// The real Linux API to open/create a file
			
				Kernel_Memory_Write( (word_32)V0_Addr, fileId, 4);	
						// Write fileId back to register v0 - in fact to mapped memory location

				/*mem -> Read( (word_32)PC_Addr, val, 4);	// NOT REQUIRED !!
				val += 4;
				mem -> Write( (word_32)PC_Addr, val, 4);*/
			
				if( fileId != -1 )
				{
				//	cout << green << "\nFile opened!! File Name : "<< fileName 
				//		<< " with id : " << fileId << reset ;
					DEBUG(dbgThread , green << "\nFile opened : "<< fileName.c_str()
							<< " with id : " << fileId) ;
					console->ConsoleWrite("\nFile Opened !!\n");
				}
				else
				{
					//	cout << red << "\nFile couldnot be created" << reset;
					DEBUG( dbgThread , red << "\nError !! File could not be created" );
				}
				
			}
			
			break;
			
		case SYSCALL_CLOSE:
			//close( fileId );
			//close( arg );
			break;

		case SYSCALL_WRITE:
			break;
		case SYSCALL_READ:
			break;			

		default:
			/*
			sem_wait( cout_mutex );
			cout << red << "\nError in the system call code" << reset;
			sem_post( cout_mutex );*/
			DEBUG( dbgThread , red << "\n[Kernel::SysCallHandler] Error !! Unknown system call...");
			
			break;
	}
	return;
}


void Kernel :: TimerInterruptHandler ()
{
	//scheduler->RunThread ( currentThread , false );
	Thread *nextThread = scheduler->NextToRun() ;
	if( nextThread != NULL )
		scheduler->RunThread( nextThread , false );
	else
		scheduler->RunThread ( currentThread , false );
	return;
}


//--------------------------- 12/04/2006 ---------------------------------//
//---------------------------   Rajeesh  ---------------------------------//

bool Kernel :: NetworkTest()
{
	PacketHeader inPktHdr, outPktHdr;

	string data = "Hi there !" , ack = "Got the Msg", retData , toMachine = "192.168.3.100" ;
	
	inPktHdr = network->Receive( (char*) retData.c_str() );
//	cout << green << "\nGot mail from " << inPktHdr.from.sin_addr.s_addr << reset ;

	DEBUG( dbgNet , green << "\nGot mail from " << inPktHdr.from.sin_addr.s_addr);
	
//	network->Send( data.c_str() , toMachine.c_str() );
//	cout << green << "\nSent mail to " << toMachine.c_str() << reset ;


	return true;
}
	
