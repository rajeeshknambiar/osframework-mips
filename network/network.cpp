/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna.
 *  Any part of this file could be used, reproduced, modified or distributed adhering to the
 *  terms and conditions in "copyleft.h"
 */


//------------------------------- 11/04/2006 ------------------------------------//
//-------------------------------   Rajeesh  ------------------------------------//

#include "network.h"
#include "routines.h"

#include <cstring>
using std::memcpy;

PacketHeader :: PacketHeader()
{
	to.sin_family = AF_INET ;	// AF_INET family to have TCP/IP communication
	to.sin_addr.s_addr = htonl( INADDR_ANY );
	to.sin_port = htons( PORT_NUMBER );	// Default port
	
	from.sin_family = AF_INET ;
	from.sin_addr.s_addr = htonl( INADDR_ANY );
	from.sin_port = htons( PORT_NUMBER );

	length	= 0 ;
}

//--------------------------------------

//#include "list.cpp"		// For List - notice that 'list.cpp' instead of 'list.h'

Mail :: Mail( PacketHeader hdr , char *msgData )
{
	pktHdr = hdr ;
	data = new char[ MaxPacketSize ];
	memcpy( data , msgData , hdr.length );
}

Mail :: ~Mail()
{
	delete data;
}

#include "synch.cpp"		// For SynchList

MailBox :: MailBox()
{
	msgList = new SynchList< Mail* > ;		// To be changed to SynchList - TODO
}

MailBox :: ~MailBox()
{
	delete msgList ;
}

void MailBox :: PutMail( PacketHeader hdr , char *msg )
{
	Mail *mail = new Mail( hdr , msg );
	msgList -> Append( mail );
	
	DEBUG( dbgNet, green << "\n[MailBox::PutMail] Putting new mail in Mail Box" << reset );
}

void MailBox :: GetMail( PacketHeader *hdr , char *msg )
{
	DEBUG( dbgNet, "\n[MailBox::GetMail] Waiting for mail in Mail Box" );
	
	Mail *mail = msgList -> RemoveFront();
	*hdr = mail->pktHdr ;
	memcpy( msg , mail->data , mail->pktHdr.length );
	DEBUG( dbgNet, "\n[MailBox::GetMail] Got new mail in Mail Box" );

	delete mail;
}
	
//-----------------------------------------------


Network :: Network ()
{
	mailBox = new MailBox();
		
	ASSERT( gethostname( (char*)hostName.c_str() , 60 ) == 0 );	// Get the hostname of this machine
	DEBUG( dbgNet, "\nSetting Host name : " << hostName.c_str() ) ;
	//memcpy( hostName.c_str() , (void*)pktHdr.from.sin_addr.s_addr , hostName.length() ); 
									// copy the hostname to pktHdr.from address
	//socket = -1 ;	// Or OpenSocket...?
	socket = OpenSocket();						// Open a socket
	packetAvailable = false ;
	sendingBusy     = false ;

}
		

Network :: Network ( char *hname )
{
	mailBox = new MailBox();
	
	hostName = string( hname );
	DEBUG( dbgNet, "\nSetting Host name : " << hostName.c_str() ) ;
	
	//socket = -1 ;
	socket = OpenSocket();
	packetAvailable = false ;
	sendingBusy	= false ;
}

Network :: ~Network ()
{
	delete mailBox ;
	CloseSocket( socket ) ;
}


//#include "main.h"

#include "routines.h"		// for SendToSocket and ReadFromSocket
#include "color.h"
#include <netdb.h>			// for 'gethostbyname' and 'struct hostent'

bool Network :: Send ( const char *data , const char *toName )
{
	
	struct hostent *myself , *destination ;
	// 'myself' refers to this machine, which is trying to send packets to 'destination' machine
	// Setting the 'from' part of the Packet Header being sent out 
	myself = gethostbyname( hostName.c_str() );	// Get my IP Address details
	memcpy( (void*)&pktHdr.from.sin_addr.s_addr , myself->h_addr, myself->h_length );
	// Setting the destination address
	ASSERT( (destination = gethostbyname(toName)) != NULL ) ;  // Get IP Address of foreign machine
	memcpy( (void*)&pktHdr.to.sin_addr.s_addr , destination->h_addr, destination->h_length );	
	pktHdr.length = sizeof(pktHdr) + strlen(data) + 1 ;
	
	ASSERT( (sendingBusy == false) && (pktHdr.length > 0) && (pktHdr.length <= MaxPacketSize) );
	DEBUG(dbgNet, blue<<"Sending packet to address " << pktHdr.to.sin_addr.s_addr 
			<< ", length " << pktHdr.length << reset );

	char *buffer = new char[ MaxPacketSize ];
	ASSERT( buffer != NULL );
	
	*(PacketHeader*) buffer = pktHdr ;				// First prepend the header
	memcpy( buffer+sizeof(PacketHeader) , data , pktHdr.length );	// Copy from data to buffer
	sendingBusy = true ;
	SendToSocket( socket, buffer, MaxPacketSize , pktHdr.to );
	sendingBusy = false ;
		
	delete []buffer;
	return true;
}


PacketHeader Network :: Receive ( char *data )
{
	if( ! PollSocket ( socket ) )		// Nothing to do if no data is arrived
	{
		pktHdr.length = 0;
		return pktHdr ;
	}

	//struct hostent *myself , *source ;	// IP address of this machine and Source machine
	// 'myself' is this machine which is listening and accepting connections, from 'source' machine
	
	int addr_len = sizeof(NetworkAddress);
	static int newsock = OpenSocket();	// New socket for an incoming connection
		
	if( bind( newsock, (struct sockaddr*) &pktHdr.to, addr_len ) < 0 )
		DEBUG( dbgNet, red << "\n[Network::Receive] Error ! Could not bind to socket !!" << reset ) ;
	if( listen( newsock , 5 ) < 0 )
		DEBUG( dbgNet, red << "\n[Network::Receive] Error ! Could not listen to socket !!" << reset ) ;
	DEBUG( dbgNet, green << "\n[Network::Receive] Listening to port " << PORT_NUMBER << reset ) ;

	if( (socket = accept( newsock, (struct sockaddr*) &pktHdr.from , (socklen_t*)&addr_len )) < 0 )
		DEBUG( dbgNet, red << "\n[Network::Receive] Error ! Could not accept connections !!" << reset ) ;
	
	
	char *buffer = new char[ MaxPacketSize ];
	ReadFromSocket ( socket, buffer, MaxPacketSize, pktHdr.from );	// Read Packet from Socket

	pktHdr = *(PacketHeader*) buffer ;	// Set the pktHdr
	ASSERT( pktHdr.length <= MaxPacketSize );

	memcpy( data , buffer+sizeof(PacketHeader) , pktHdr.length );	// Copy from buffer to data
	
	delete []buffer;
	DEBUG(dbgNet, "Network received packet from " << pktHdr.from.sin_addr.s_addr
			<< ", length " << pktHdr.length << reset );
	cout << "\n\tMessage is : " << green << data << "\n" << reset;
	
	mailBox->PutMail( pktHdr , data );
		
	return pktHdr;
}

//------------------------------------------
