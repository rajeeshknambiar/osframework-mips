/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

//----------------------- 11/04/2006 -------------------------//
//-----------------------   Rajeesh  -------------------------//

#ifndef __NETWORK_H
#define __NETWORK_H

#include "copyleft.h"

#include "debug.h"		// For DEBUG

#include "list.h"		// For List

#include <iostream>			// For string
using std::string;

#include <netinet/in.h>			// For sockaddr_in

#define	PORT_NUMBER	6565

#define MaxPacketSize	64

typedef sockaddr_in NetworkAddress ;


class PacketHeader
{
	public:
		NetworkAddress to ;	// IP address of Destination machine acceptiong connections
		NetworkAddress from;	// IP address of Source machine trying to connect to another
		unsigned length ;	// Length of the Packet

		PacketHeader();
		~PacketHeader() {};
};


class Mail
{
	public:
		PacketHeader pktHdr;
		char *data;

		Mail( PacketHeader hdr , char *msgData );
		~Mail();
};

#include "synch.h"

class MailBox
{
	private:
		SynchList<Mail*> *msgList ;	// To be changed to SynchList !!! TODO
	public:
		MailBox();
		~MailBox();
		
		void GetMail( PacketHeader *hdr , char *msg );
		void PutMail( PacketHeader hdr , char *msg );
};
		

class Network
{
	private:
		MailBox *mailBox ;
		PacketHeader pktHdr ;		// A Packet Header
		string hostName ;		// Host name/IP address of this machine
		int socket;			// The socket through which sending/receiving is done
		bool packetAvailable;		// Did a packet arrive or not
		bool sendingBusy;		// Network is currently busy sending a packet
	public:
		Network();			// The default constructor - 'hostName' will be set automatically
		Network(char *hname);		// Set the 'hostName' as specified by 'hname'
		~Network();
		
		bool Send ( const char *data , const char *toName );	// Send a Packet to network
		PacketHeader Receive ( char *data );		// Receive a Packet from network

};


#endif	// __NETWORK_H
