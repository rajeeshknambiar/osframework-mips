/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */


#ifndef __DEBUG_H
#define __DEBUG_H

#include "copyleft.h"

#include <iostream>
#include <assert.h>
#include "color.h"

// The assert and debug conditions are given here ..

#include <semaphore.h>
extern sem_t *cout_mutex ;

#define ASSERT(condition) assert(condition)

const char dbgAll ='+';
const char dbgThread = 't';	// for the Thread 
const char dbgInt = 'i';	// for the interrupts 
const char dbgMachine = 'm';	// for the machine part
const char dbgAddr = 'a';	// for the addressSpace
const char dbgNet = 'n';	// for the network 
const char dbgFile = 'f';	// for the filesystem


class Debug{

private :
	char *enabledFlags;

public:	
	Debug( const char *flagList );

	bool IsEnabled( char flag );

};

extern Debug *debug;

#define DEBUG(flag,expr) \
    if (debug->IsEnabled(flag)) {  \
            sem_wait(cout_mutex); std::cout << expr << reset; sem_post(cout_mutex); \
       }

#endif	// __DEBUG_H
