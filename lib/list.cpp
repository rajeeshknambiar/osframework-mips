/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

//--------------------- 25/01/2006 ---------------------//
//---------------------   Rajeesh  ---------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

#ifndef __LIST_CPP
#define __LIST_CPP

#include "list.h"
#include "debug.h"

template <class Type>
List<Type> :: List()
{
	head = NULL ;
	tail = NULL ;
	numEntries = 0 ;
}

template <class Type>
List <Type> :: ~List()
{
	while( ! IsEmpty() )
		delete RemoveFront();
}


template<class Type>
bool List <Type> :: Append( Type item )
{
	ListElement<Type> *newitem = new ListElement<Type>( item );
	
	if ( IsEmpty() )	// list is empty
	{
		head = newitem;
		tail = newitem;
	}
	else			// Put it at the end
	{
		tail->next = newitem;
		tail = newitem;
	}
	numEntries ++;
	return true;
}

template <class Type>
bool List <Type> :: Prepend( Type item )
{
	ListElement<Type> *newitem = new ListElement<Type>( item );
 	
	if ( IsEmpty() )        // list is empty
	{
		head = newitem;
		tail = newitem;
	}
	else			// Put it at the beginning
	{
		newitem->next = head;
		head = newitem;
	}
	numEntries ++;
	return true;
}
		

template <class Type>
bool List <Type> :: Remove( Type item )
{
	Type removeditem;
	ASSERT( IsInList( item ) );
	if( item == head->item )
	{
		removeditem = RemoveFront();	// The first one is 'item'; to be deleted
		ASSERT ( item == removeditem );	// Otherwise error !!
			

			return true;
	}
	else
	{
		ListElement<Type> *prev, *ptr;
		for( prev= head, ptr=head->next;  ptr != NULL;  prev=ptr, ptr=ptr->next )
		{
			if( item == ptr->item )		// Found 'item'
			{
				prev->next = ptr->next;
				if ( prev->next == NULL )
					{ tail = prev; }
				delete ptr;
				numEntries --;		// Decrement number of entries
				return true;
			}	
		}
		if( ptr == NULL )
			return false;
	}
	ASSERT ( ! IsInList( item )); 	// The 'item' must not be present in the list after deletion !!
		
	return true;
}

template <class Type>
bool List <Type> :: IsInList( Type item )
{
	ListElement<Type> *ptr;
	for( ptr = head; ptr != NULL; ptr = ptr->next )
		if ( ptr->item == item )
			return true;
	return false;
}

template <class Type>
Type List <Type> :: RemoveFront()
{
	ListElement<Type> *element = head;
	Type retitem;
	if( IsEmpty() )
		return retitem;
	
	retitem = head->item;
	
	if( head == tail )
	{
		head = NULL;
		tail = NULL;
	}
	else
	{	head = element->next;
	}
	numEntries -- ;
	delete element;
	return retitem;
}

template <class Type>
void List <Type> :: Apply ( void(*fn)(Type) )
{
	ListElement<Type> *ptr ;
	
	for( ptr = head;  ptr != NULL;  ptr = ptr->next )
		(*fn)( ptr->item );

	return;
}

template <class Type>
void List <Type> :: Reverse ()
{
	if ( IsEmpty() || (head==tail) )		// If no elements or just one elements, nothing to do...
		return;
	
	ListElement<Type>  *ptr=head->next , *nextptr= NULL;
	
	tail = head;
	do
	{						// Traverse the list from 'head', reversing 'next' links...
		nextptr	  = ptr->next;
		ptr->next = head;
		head	  = ptr;
		ptr	  = nextptr;
	} while( nextptr != NULL );
	
	//ptr = head;
	//head = tail;
	//tail = head;
	
	return;
}

template <class T>
bool SortedList <T> :: Insert( T item)
{
     ListElement<T> *element = new ListElement<T>(item);
     ASSERT( element != NULL );
     ListElement<T> *ptr;                // keep track
     //ASSERT(!IsInList(item));
     if (this->IsEmpty()) 
     {                      // if list is empty, put at front
	     this->head = element;
	     this->tail = element;
     } 
     else if ( Compare(item, this->head->item) < 0 ) 
     {  // item goes at front
	     element->next = this->head;
	     this->head = element;
     } 
     else 
     {            // look for first elt in list bigger than item
     	for (ptr = this->head; ptr->next != NULL; ptr = ptr->next) 
     	{
     		 if ( Compare(item, ptr->next->item) < 0 ) 
		 {
			 element->next = ptr->next;
			 ptr->next = element;
			 this->numEntries ++ ;
			 return true;
		 }
	}
     	this->tail->next = element;             // item goes at end of list
     	this->tail = element;
    }
    this->numEntries ++;
    ASSERT(this->IsInList(item));
    return true;
}


#endif	// __LIST_CPP	
