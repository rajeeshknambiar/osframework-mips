/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

//--------------------- 22/01/2006 --------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

/*
   This header file contains definitions for implementing a Synchronized List(SynchList).
   The synchronization is made possible by using semaphores.
 */


#ifndef	__SYNCH_H
#define __SYNCH_H

#include "copyleft.h"

//------------------------        12/04/06         -----------------------------//

#include "sem.h"		// For Semaphore

template < class T>
class SynchList
{
	private :
		List <T> *list;
		Semaphore *sem_synch;

	public:
		SynchList();
		~SynchList();

		void Append ( T item);

		T RemoveFront();

		void Apply( void (*f)(T));
};


#endif	// __SYNCH_H

