/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
//-----------------------------11/04/2006------------------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

/*
	Routines necessary for system dependant interface are implemented here.
	The Operating System calls the routines defined here, instead of the 
	linux library routines directly.*/

#include "routines.h"
#include "debug.h"

#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"
#include "sys/file.h"
#include "sys/types.h"
#include "sys/select.h"
#include <sys/stat.h>
#include <fcntl.h>



//--------------Routines for file system ------------------------------------------------



// ----- For creating the file with 'name' for the first time or truncates if it already exists----//

int OpenForWrite(char *name)
{
	    int fd = open(name, O_RDWR|O_CREAT|O_TRUNC, 0666);

	        ASSERT(fd >= 0);
		    return fd; // return file descriptor
}

//------------For opening the file for reading or writing into the file-------------------------------------//

int OpenForReadWrite(char *name, bool error)
{
	    int fd = open(name, O_RDWR, 0);

	        ASSERT(!error || fd >= 0);
		    return fd; // returns the file descriptor or shows error if file doesn't exist
}



//----------- Read characters from an open file.  Abort if read fails.-------------//

void Read(int fd, char *buffer, int nBytes)
{
	    int retVal = read(fd, buffer, nBytes);
	        ASSERT(retVal == nBytes);
}



//------------ Read characters from an open file, returning as many as are available.---------- //

int ReadMax(int fd, char *buffer, int nBytes)
{
	    return read(fd, buffer, nBytes);
}

//-----------  Write characters to an open file.  Abort if write fails.---------//

void Write(int fd, char *buffer, int nBytes)
{
	    int retVal = write(fd, buffer, nBytes);
	        ASSERT(retVal == nBytes);
}


//--------- Lseek to  change the location within an open file.  Abort on error.----------//

void Lseek(int fd, int offset, int whence)
{
	    int retVal = lseek(fd, offset, whence);
	        ASSERT(retVal >= 0);
}

//---------Ftell to get the current location within an open file---------------------------//
int Ftell( int fd)
{
	    return lseek(fd,0,SEEK_CUR); 
  //	    return ftell( fd );
}	    

//----------- Close a file.  Abort on error.----------------------------------------------//

int Close(int fd)
{
	    int retVal = close(fd);
	        ASSERT(retVal >= 0);
		    return retVal;
}

//------------Unlink to  Delete a file.---------------------------------------------------//

bool Unlink(char *name)
{
	    return unlink(name);
}

bool PollFile(int fd)
{
	fd_set rfd,wfd,xfd;
	int retVal;
	    struct timeval pollTime;
	    FD_ZERO(&rfd);
	    FD_ZERO(&wfd);
	    FD_ZERO(&xfd);
	    FD_SET(fd,&rfd);
	
	    pollTime.tv_sec = 0;
	    pollTime.tv_usec = 0;
	    retVal = select(32, &rfd, &wfd, &xfd, &pollTime);
	    
	    ASSERT((retVal == 0) || (retVal == 1));
            if (retVal == 0)
			        return false;
	   return true; 
}

void WhenUserAbort( void (*function) (int) )
{
	signal( SIGINT , function ) ;   // Give signal interrupt - SIGINT to 'function' on a "^C"
}


//------------------------------- 11/04/2006 -------------------------------------//
//-------------------------------   Rajeesh  -------------------------------------//

int OpenSocket()
{
	int sock = socket ( AF_INET, SOCK_STREAM, 0 );	// Returns -1 on error
	ASSERT( sock > -1 );
	return sock;
}

int CloseSocket( int socketId )
{
	shutdown( socketId , SHUT_RDWR );	// Shutdown the full duplex transimssion/reception
	int retVal = close( socketId );		// Returns -1 on error
	ASSERT( retVal > -1 );
	return retVal;
}


ssize_t ReadFromSocket( int socketId , char *buffer , int packetSize , NetworkAddress &from )
{
	int retVal , size = sizeof( from );
	retVal = recvfrom(socketId,buffer,packetSize,0,(struct sockaddr*) &from,(socklen_t*) & size);
	
	if (retVal != packetSize)	// Miss match in the no of data sent and received
	{
		DEBUG( dbgNet, "\nError in ReadFromSocket... missmatch in data length" );
	}
	ASSERT(retVal == packetSize);

	return retVal;
}


ssize_t SendToSocket( int socketId, char *buffer, int packetSize, NetworkAddress to )
{
	int retVal , retryCount;
	if( connect( socketId, (struct sockaddr*) &to, sizeof(to) ) < 0 )
	{
		DEBUG( dbgNet, "\nError in SendToSocket... could not connect to Destination machine" );
		return -1;
	}
	
	for(retryCount=0;retryCount < 10;retryCount++)
	{
		retVal = sendto( socketId, buffer, packetSize, 0,(struct sockaddr*) &to, sizeof(to) );
		if (retVal == packetSize)
		{
			DEBUG( dbgNet, "Mail sent successfully.\n");
			return retVal ; 
		}
		//	Assert that if not retruned by this, error occured 
		ASSERT(retVal < 0);
	}
	return -1;
}
bool PollSocket(int sockID)
{
	    return PollFile(sockID);    // on UNIX, socket ID's are just file ID's
}

