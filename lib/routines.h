/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
//------------------------11/04/2006----------------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */


/* This file contains the prototypes for the routines for system dependent interface */

#ifndef __ROUTINES_H
#define __ROUTINE_H

#include "copyleft.h"

#include <stdio.h>

#include <sys/types.h>
#include <netinet/in.h>


// Divide and either round up or down
#define divRoundDown(n,s)  ((n) / (s))
#define divRoundUp(n,s)    (((n) / (s)) + ((((n) % (s)) > 0) ? 1 : 0))

typedef	sockaddr_in NetworkAddress ;

// File operations: open/read/write/lseek/close, and check for error
// For simulating the disk and the console devices.
extern int OpenForWrite(char *name);
extern int OpenForReadWrite(char *name, bool crashOnError);
extern void Read(int fd, char *buffer, int nBytes);
extern int ReadPartial(int fd, char *buffer, int nBytes);
extern void Write(int fd, char *buffer, int nBytes);
extern void Lseek(int fd, int offset, int whence);
extern int Ftell(int fd);
extern int Close(int fd);
extern bool Unlink(char *name);

extern bool PollFile(int fd);
// For use by the Network
extern int OpenSocket();
extern int CloseSocket(int socketId);
extern ssize_t ReadFromSocket(int socketId, char *buffer, int packetSize, NetworkAddress &from);
extern ssize_t SendToSocket(int socketId, char *buffer, int packetSize, NetworkAddress to);
extern bool PollSocket(int socketno);

#include <signal.h>

void WhenUserAbort( void (*function) (int) ) ;


#endif	// __ROUTINES_H
