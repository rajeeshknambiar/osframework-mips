/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */


#ifndef __SYNCH_CPP
#define __SYNCH_CPP

#include "synch.h"
#include "list.cpp"

template <class T>
SynchList<T> :: SynchList ()
{
	list = new List <T>;
	sem_synch = new Semaphore("synch_list_semaphore", 1);
}

template <class T>
SynchList<T> :: ~SynchList()
{
	delete list;
	delete sem_synch;
}

template <class T>
void SynchList <T> :: Append (T item )
{
	sem_synch -> P();
	list -> Append ( item );
	sem_synch -> V();
}

template <class T>
T SynchList<T> :: RemoveFront()
{
	T item;
	sem_synch -> P();
	while(list -> IsEmpty() );
	item = list -> RemoveFront();
	sem_synch -> V();
	return item;
}	

template <class T>
void SynchList<T> :: Apply(void (*func)(T))
{
	sem_synch -> P();
	list -> Apply((func));
	sem_synch -> V();
}

#endif	// __SYNCH_CPP
