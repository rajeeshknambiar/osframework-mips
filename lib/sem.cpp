/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

//-----------------   12-04-06  -----------------------------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

//The semaphore implementations of the operaing system goes here ...

#ifndef __SEM_CPP
#define __SEM_CPP

#include "sem.h"
#include "main.h"
Semaphore :: Semaphore( const char *temp,int initial_value )
{
	DEBUG( dbgThread, skyblue << "\nSemaphore " << temp << " initialized" );
	name = string( temp);
	value = initial_value ;
	queue = new List <Thread *>;
}

Semaphore :: ~Semaphore()
{
	DEBUG( dbgThread , red << "\nDestroying Semaphore " << name ) ;
	delete queue;
}


void Semaphore :: P()
{
	if ( value <= 0)
	{
		DEBUG(dbgThread,"\n[Semaphore::P] Semaphore " << name << " not available");
		queue -> Append( kernel -> currentThread );
		kernel -> currentThread -> Sleep(false);
	}
	else
	{
		DEBUG(dbgThread,"\n[Semaphore::P] Semaphore " << name << " decrementing");
		value --;
	}	
}	

void Semaphore :: V()
{
	if( !( queue -> IsEmpty()))
	{
		DEBUG(dbgThread , "\n[Semaphore::V] Semaphore "<<name<<" removes thread from the queue");
		//--- Modified on 02/04/2008 by Rajeesh ---//
		Thread *temp = queue -> RemoveFront();
		kernel -> scheduler -> RemoveFromWaitQ( temp );
		//-----------------------------------------//
		kernel -> scheduler -> AddToReadyQ( temp );
	}
	else
	{
		DEBUG(dbgThread,"\n[Semaphore::V] Semaphore "<<name<<" incrementing");
		value ++;
	}

}

#endif	// __SEM_CPP
