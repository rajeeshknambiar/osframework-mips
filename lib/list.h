/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

//--------------------- 22/01/2006 --------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */


#include "copyleft.h"

#ifndef __LIST_H
#define __LIST_H

/*
   This header file contains a variety of useful datastructures
   that will make the implementation of kernel datastructures
   simple and efficient. The list datastructure is a generic
   class which can hold any type of data(a class object or a
   datatype) and can perform various operations. This list class
   is used to implement all the process queues(ready,waiting etc),
   pending interrupts and the like.
*/

#include <iostream>		// For NULL

template <class Type>
class ListElement {
	public:
		Type item;
		ListElement *next;

		ListElement( Type newitem ) { item = newitem; next = NULL; } ;
};

template<class Type> class ListIterator;	// Declaration of Iteraror, Defined below...

template <class Type>
class List {					// Singly linked list.
	protected:
		ListElement<Type> *head;	// First element of the list, NULL if list is empty.
		ListElement<Type> *tail;	// Last element of the list.
		unsigned int numEntries;	// Number of elements in the list.

		friend class ListIterator<Type>;	// To travel through the list;
			
	public:	
		List();				// Initialise list.
		
		virtual ~List();		// Destroy the list, virtual destructor because of the
						// possibility of complex List types derived from this.
		
		virtual bool Append ( Type item ); // Place the item at the beginning of list.
		
		virtual bool Prepend( Type item ); // Put the item at the end of the list.
		

		Type Front () { return head->item; };  // Return first item, without removing it from list.
		
		Type RemoveFront();		// Remove the first item from list and return it.
		
		bool Remove( Type item );	// Remove the specified item, return success/failure.

		bool IsInList( Type item );	// Is the item in the list ?
		
		unsigned int NumEntries() { return numEntries; }; // Returns number of elements in list.

		bool IsEmpty()
		{ if (numEntries == 0)
			return true;
		  else
			return false;
	       	}; // Tell whether list is empty.
		

		void Apply( void(*fn)(Type) );	// Apply the specified function to all elements in list.

		virtual void Reverse();		// Reverse the whole list.
		
};


//------------------------- 23/01/2006 -------------------------// Rajeesh

template <class Type>
class SortedList : public List<Type> {

	protected:
		// Appending and Prepending does not make any sense in a sorted list!
		bool Append (Type item) { return Insert(item); };

		bool Prepend(Type item) { return Insert(item); };

		int  (*Compare)(Type x, Type y);	// Comparison function for sorting the list.

	public:
		SortedList( int (*compare)(Type x, Type y) ) : List<Type>() // Calling base class
			{ Compare = compare; }		// constructor, and setting up Compare function.

		~SortedList() {};
		
		bool Insert( Type item );		// Insert the item sorted
};


template <class Type>
class ListIterator{
	private:
		ListElement<Type> *current;
		ListElement<Type> *next;
	public:
		ListIterator ( List<Type> *list )
	       	{ 
			current = list->first; 
			if( current == NULL )
				next = NULL;
			else
				next = current->next;
		};
		
		Type Item() { return current->item; } ;

		void Next() 
		{
			current = next;
			if( next != NULL )
				next = next->next;
		} ;

		bool IsDone() { return current == NULL ; } ;

};	// Class Iterator, which is used to traverse through List.




#endif	// __LIST_H
