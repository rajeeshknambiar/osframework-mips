/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
//---------------------------12/04/2006--------------------------------

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */


/* ---------Implementation of the routines to manage the bitmap .*/


#include "bitmap.h"

//----------To  Initialize a bitmap with "numItems" bits, so that every bit is clear.--------


BitMap::BitMap(int numItems)
{
	    int i;

	        ASSERT(numItems > 0);

	        numBits = numItems;
	        numWords = divRoundUp(numBits, BitsInWord);
	        map = new unsigned int[numWords];
	       for (i = 0; i < numWords; i++) 
	       {
	            map[i] = 0;             // initialize map to keep Purify happy
	       }
	       for (i = 0; i < numBits; i++) 
	       {
	            Reset(i);
	       }
}


//-----------------To  De-allocate a bitmap.----------------------------------------------

BitMap::~BitMap()
{
	    delete map;
}

//-------------To set the "nth" bit in a bitmap.  "which" is the number of the bit to be set.

void BitMap::Set(int which)
{
	    ASSERT(which >= 0 && which < numBits);

            map[which / BitsInWord] |= 1 << (which % BitsInWord);

 	    ASSERT(Test(which));
}



//------------ To clear the "nth" bit in a bitmap.  "which" is the number of the bit to be cleared.

void BitMap::Reset(int which)
{
	    ASSERT(which >= 0 && which < numBits);

	        map[which / BitsInWord] &= ~(1 << (which % BitsInWord));

		    ASSERT(!Test(which));
}


//-----------To return TRUE if the "nth" bit is set.   "which" is the number of the bit to be tested.

bool BitMap::Test(int which) const
{
	    ASSERT(which >= 0 && which < numBits);

            if (map[which / BitsInWord] & (1 << (which % BitsInWord))) 
	    {
	        return true;
	    } 
	    else 
	    {
		 return false;
	    }
}

//------------To  find and allocate a bit     If no bits are clear, return -1.

int BitMap::FindAndSet()
{
	    for (int i = 0; i < numBits; i++) 
	    {
           	if (!Test(i)) 
		{
		        Set(i);
		        return i;
		}
	    }
	    return -1;
}


//----------To find  how many bits are unallocated-----------------------------------

int BitMap::NumClear() const
{
	    int count = 0;
            for (int i = 0; i < numBits; i++) 
	    {
	        if (!Test(i)) 
		{
		     count++;
		}
	    }
	    return count;
}

//-----------To print the contents of the bitmap, for debugging.------------------------


void BitMap::Print() const
{
    DEBUG(dbgFile, "\n[Bitmap::Print] Bitmap set");
    for (int i = 0; i < numBits; i++) 
    {
       if (Test(i)) 
       {
	       DEBUG(dbgFile, i << ", ");
	}
    }
    
}

//----------------------------------------------------------------------
// Bitmap::SelfTest
//      Test whether this module is working.
//----------------------------------------------------------------------

void
BitMap::SelfTest()
{
        int i;

        ASSERT(numBits >= BitsInWord);      // bitmap must be big enough

        ASSERT(NumClear() == numBits);      // bitmap must be empty
        ASSERT(FindAndSet() == 0);
        Set(31);
        ASSERT(Test(0) && Test(31));
	ASSERT(FindAndSet() == 1);
	Reset(0);
	Reset(1);
	Reset(31);
	for (i = 0; i < numBits; i++) 
	{
	    Set(i);
	}
	ASSERT(FindAndSet() == -1);         // bitmap should be full!
	for (i = 0; i < numBits; i++) 
	{
	     Reset(i);
	}
}
    
