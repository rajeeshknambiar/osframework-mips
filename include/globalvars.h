/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

//------------------------ 19/01/2006 ---------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

#ifndef __GLOBAL_H
#define __GLOBAL_H

#include "copyleft.h"

/*
   The purpose of this header file is to define and collect all the
   global variables and like in to a single entity.
*/


#define	PageSize	1024			// Bytes - 1 KB

#define	MemorySize	4 * 1024 * PageSize		// 4 MB

#define	TotalPhysPages	(MemorySize) / (PageSize)

#define StackSize	2048

#define REGNOS	50

//-------------------------------04/04/2006-------------------------------------//
//-------To save the register values into particular memory locations-----------//
//-------Dedicating certain memory locations for the registers to be stored-----//
#define PC_Addr		900
#define NPC_Addr	904
#define INDEX_Addr	908
#define ENTRYHI_Addr	912	
#define ENTRYLO1_Addr	916
#define ENTRYLO0_Addr	920
#define CONTEXT_Addr	924
#define BADVADDR_Addr	928
#define COUNT_Addr	932
#define CAUSE_Addr	936	
#define EPC_Addr	940
#define AT_Addr		944
#define V0_Addr		948
#define V1_Addr		952
#define A0_Addr		956
#define A1_Addr		960
#define A2_Addr		964
#define A3_Addr		968
#define SP_Addr		972
#define RETADDR_Addr	976

//--------------------------------------------------------------------------------//


#endif // __GLOBAL_H
