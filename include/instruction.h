/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/


# ifndef __INSTRUCTION_H
# define __INSTRUCTION_H

#include "types.h"
 
class Instruction
{
public :
	u_word_32 op : 6;
	u_word_32 rest : 26;
};

/* different types of instructions being modelled
 * as derived types of Instruction will cause inconsistencies
 * in instruction sizes.  So we have to explicitly cast and 
 * convert between them.
 */

class RFormat
{
public : 
	u_word_32 op : 6;
	u_word_32 rs : 5;
	u_word_32 rt : 5;
	u_word_32 rd : 5;
	u_word_32 shamt : 5;
	u_word_32 funct : 6;
};

class IFormat
{
public :
	u_word_32 op : 6;
	u_word_32 rs : 5;
	u_word_32 rt : 5;
	word_32 imm : 16;	// Signed
};

class JFormat
{ /* This is structurally same as Instruction */
public :
	u_word_32 op : 6;
	u_word_32 tAddr : 26;
};

union Inst
{ // This can be optimally be used everywhere.
	word_32 iV;		// Signed
	Instruction noF;
	IFormat iF;
	RFormat rF;
	JFormat jF;
};

struct OneRecord
{ // This structure is used in the assembler and in the
  // Bootloading code in memory.cpp
	unsigned int address;
	Inst inst;
};

# endif
