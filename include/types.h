/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

 
# ifndef __TYPES_H
# define __TYPES_H

typedef int word_32;
typedef unsigned int u_word_32;
typedef long long word_64;

enum ResultStage { NORESULT, RESULT_AT_ID, RESULT_AT_EX, RESULT_AT_MEM };

enum RegisterFetchTarget { ALU_A, ALU_B, IDRES_FT };
enum RegisterWriteSource { IDRES, ALU, ALU_HI, LOAD };

enum PCUpdateType { PC_ABSOLUTE, PC_RELATIVE };

enum PCWritingStage { NOT_WRITTEN, PC_STAGE0, PC_STAGE1, PC_STAGE2 };

# endif
