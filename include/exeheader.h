/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

//--------------------- 19/01/2006 ---------------------//

/*  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of this file could be used,
 *  modified or distributed by strictly adhering to the terms and conditions in "copyleft.h"
 */


#ifndef __EXEHEADER_H
#define __EXEHEADER_H

#include "copyleft.h"

/*
   The header structure which contains metadata of the executable
   file is defined in the exeHeader. The header contains information
   about size and address of various components of an executable
   file recognised and executed by the VyajOS.
*/

#include "types.h"


typedef struct exeHeader{
	u_word_32 totalSize;
	u_word_32 virtualAddress;
}exeHeader;

// The header has been changed for the cross compiler purposes !!!
// change if needed . 8-3-06 modified
/*
typedef struct segment {
	  int virtualAddr;              // location of segment in virt addr space 
	  int inFileAddr;               // location of segment in this file 
	  int size;                     // size of segment 
} Segment;

typedef struct noffHeader {
	   int noffMagic;               // should be NOFFMAGIC 
	   Segment code;                // executable code segment 
	   Segment initData;            // initialized data segment 
#ifdef RDATA
	   Segment readonlyData;        // read only data 
#endif
	   Segment uninitData;          // uninitialized data segment --
						                                       //  should be zero'ed before use
											                                 
} NoffHeader;
*/
//-------------------------------------------------------------

#endif	// __EXEHEADER_H
