/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

 
# ifndef __OPCODES_H
# define __OPCODES_H

/*Opcode definitions go here
 */

# define	OP_ZERO		0
// Further demuxed using the 'funct' field
// as ...

# define	FUNCT_ADD	0x20
# define	FUNCT_AND	0x24
# define	FUNCT_DIV	0x1a
# define	FUNCT_MULT	0x18
# define	FUNCT_NOR	0x27
# define	FUNCT_OR	0x25
# define	FUNCT_SLL	0
# define	FUNCT_SLLV	4
# define	FUNCT_SRA	3
# define	FUNCT_SRAV	7
# define	FUNCT_SRL	2
# define	FUNCT_SRLV	6
# define	FUNCT_SUB	0x22
# define	FUNCT_XOR	0x26
# define	FUNCT_SLT	0x2a
# define	FUNCT_JALR	9
# define	FUNCT_JR	0x8
# define	FUNCT_MFHI	0x10
# define	FUNCT_MFLO	0x12
# define	FUNCT_MTHI	0x11
# define	FUNCT_MTLO	0x13
# define	FUNCT_SYSCALL	0xc
# define	FUNCT_RDIN	0x3f		// Addition to MIPS
# define	FUNCT_RDOUT	0x3e		// Addition to MIPS



# define	OP_ADDI		8
# define	OP_ANDI		0xc
# define	OP_ORI		0xd
# define	OP_XORI		0xe
# define	OP_LUI		0xf
# define	OP_SLTI		0xa
# define	OP_BEQ		4

# define	OP_ONE		1		
// This need further demux through 'rt' field
// to identify the instruction as BGEZ or BLTZ

# define	OP_BGTZ		7
# define	OP_BLEZ		6
# define	OP_BNE		5
# define	OP_J		2
# define	OP_JAL		3
# define	OP_LW		0x23
# define	OP_SW		0x2b

//----------------------------------------------------------------------------//
//---------- MODIFICATION FOR BGEZ ------------------------------------------//
#define 	OP_BGEZ		1
#define 	OP_BLTZ		0

//----------------------------------------------------------------------------//


# define	OP_DIN		0x3f		// Addition to MIPS
# define	OP_DOUT		0x3e		// Addition to MIPS


//------------------------ 12/02/2006 --------------------------//
//------------------------   Rajeesh  --------------------------//

/*
   The following instructions/opcodes are addition to the bare CPU.
   They implement privileged and special instructions, particularly
   dealing with the functions of System Control Coprocessor - CP0,
   like TLB access/modify instructions, Trap instructions etc.
   Ref : MIPS32 Architecture for Programmers, Volume I:Introduction
         to MIPS32 Archtecture 
*/



// The following instructions are Special Instructions

// These Trap instructions come under OP_ZERO 
#define		FUNCT_TEQ	0x34	// Trap if Equal
#define		FUNCT_TGE	0x30	// Trap if Greater Than
#define		FUNCT_TLT	0x32	// Trap if Less Than
#define		FUNCT_TNE	0x36	// Trap if Not Equal

// The following instructions come under OP_ONE
#define		OP_TEQI		0xc	// Trap if Equal Immmediate
#define		OP_TGEI		8	// Trap if Greater Than Imm
#define		OP_TLTI		0xa	// Trap if Less Than Imm
#define		OP_TNEI		0xe	// Trap if Not Equal Imm

// The following are Privileged Instructions

#define		OP_COP0		16	// To demux COP0 opcodes

#define		FUNCT_ERET		0x18	// Exception Return
#define		FUNCT_TLBP		8	// TLB Probe
#define		FUNCT_TLBR		1	// Read TLB entry
#define		FUNCT_TLBW		2	// Write TLB entry
#define		FUNCT_MFC0		0	// Move From CP0
#define		FUNCT_MTC0		4	// Move To   CP0

//--------------------------------------------------------//

# endif

