/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

/********************************************************************************************************
*  													*
*			--------- COPYLEFT 2005-2006 NIT Calicut --------				*
*													*
*			 Copyleft © Rajeesh, Dinesh, Thamanna, 2005-2006  				*
*			National Institute of Technology, Calicut, India.				*
*													*
*	This software is the result of the collected effort of the following persons as their Project	*
*	work in patrial fulfillment for the award of degree of Master of Compter Applications at	*
*	National Institute of Technology, Calicut, India; in the year 2005-2006.			*
*													*
*	Rajeesh K V											*
*	Y3M008												*
*	2003 MCA											*
*													*
*	Thamanna Q R											*
*	Y3M010												*
*	2003 MCA											*
*													*
*	Dinesh V											*
*	Y3M011												*
*	2003 MCA											*
*													*
*	under the guidance of										*
*													*
*	Dr. Priya Chandran										*
*	Asst. Professor											*
*	Computer Science and Engineering Dept.								*
*	National Institute of Technology								*
*	Calicut, India.											*
*													*
*													*
*	This software could be freely used, modified and distributed subject to the following		*
*	terms and conditions.										*
*													*
*	1) The terms 'file', 'software', 'package', 'parts of software', 'parts of			*
*	   file' are used interchangeably and will be referred to as the 'software' hereafter.		*
*													*
*	2) This software and associated documentation can be used, modified and redistributed 		*
*	   for academic purpose, strictly free of cost.							*
*													*
*	3) Permission to use, modify and distribte this software is granted provided the 		*
*	   authorship of the software is recognized and duly respected.					*
*													*
*	4) This software comes with absolutely no warranty, either implicit or explicit.		*
*													*
*	5) The rights of the original author of the MIPS machine simulator has been			*
*	   recognized and preserved as per norms.							*
*													*
*	6) Any part of this package can be redistributed or used only in conjunction			*
*	   with the complete software and shall not be used, modified or distributed 			*
*	   seperately.											*
*													*
*													*
********************************************************************************************************/

//--- Added on 07/02/2008 by Rajeesh ---//
// To display the Copyleft message in every executables - do a 'strings system|grep Copyright'

# ifdef __COPYLEFT

const char *copyleft = "Copyright (c) 2005-2008 Rajeesh, Dinesh, Thamanna - NIT Calicut ";

# endif	// __COPYLEFT


