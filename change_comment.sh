#!/bin/bash
# Rajeesh
# 01/11/2008
# The OS program had custom copyright comment inserted by Varghese all over.
# Change it to standard free software copyright statement
# Run this script from the top directory

#for file in `find . -name '*.cpp' -or -name '*.h'`
for file in `find . -type f | grep -vE ".sbak$|.sh$"`	# exclude .sh to avoid endless loop
do
	# If line number 2  contains 'miniproject', we got the beginning
	# If line number 49 contains 'varghese', we got line 50 as end.
	# If both conditions are true, delete lines 2 to 50 using sed, and reset flags
	# Then put the copyright text at the beginning
	awk '(FNR==2 && /miniproject/) {beg=1}; (FNR==49 && /varghese/) {end=1};
	  END { if (beg==1 && end==1) {beg=0;end=0;cmd="sed -i.sbak 2,50d "FILENAME;system(cmd)} }' $file
	#if [ -f $file.sbak ]; then
	echo "Processing $file ..."
	cp $file $file.sbak 
	cat copyright.txt $file.sbak > $file
	#fi
done
#For Makefiles and .mips files, comment must start with #, not /*
find . -name "Makefile.*" -or -name "*.mips" | xargs sed -i.sbak 's/^[/ ]\*/# */g'
# Remove the backup files
find . -name "*.sbak" -delete
