/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

 
#include <iostream>
using std::cerr;
using std::cout;
using std::flush;

#include <fstream>
using std::ofstream;
using std::ios;

#include "structures.h"

#include "color.h"

ofstream ofile;

extern FILE *yyin;	// from lexer.lex
extern int yyparse ( );	// from asm.yy
extern Symtab symtab;	// from asm.yy
extern int errorcount;	// from asm.yy

int lineno;
bool pass1 = true;

int main(int argc, char **argv)
{
	if ( argc < 3 )
	{
		cerr << red << "\nusage : asm <output_file> <input_file>"
			<< reset << "\n\n";
		return -1;
	}
	
	yyin = fopen( argv[2], "r" );
	if ( yyin == NULL )
	{
		cerr << red << "\nInput file not found." << reset << "\n\n";
		return -2;
	}
	
	ofile.open ( argv[1], ios::out | ios::trunc | ios::binary );
	if ( !ofile )
	{
		cerr << red << "\nCould not open output file for writing." 
			<< reset << "\n\n";
		return -3;
	}
	
	cout << flush << "\nStarting pass 1 ... \n";
	lineno = 1;
	pass1 = true;
	yyparse ();
	fclose ( yyin );
	if ( errorcount == 0 )
	{
		cout << flush << "\nStarting pass 2 ... \n";
		yyin = fopen( argv[2], "r" );
		pass1 = false;
		lineno = 1;
		yyparse ();
		fclose ( yyin );
	}
	
	cout << flush << "\nDisplaying the symbol table ... \n";
	symtab.Display ( );
	
	ofile.close ();
	
	return 0;
}
