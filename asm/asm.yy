
%{
/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

	#include <fstream>
	using std::ofstream;
	
	#include <iostream>
	using std::cerr;
	#include <iomanip>
	using std::setw;
	
	#include <cstring>
	using std::strlen;
	
	#include "instruction.h"
	#include "opcodes.h"
	#include "color.h"
	#include "structures.h"
	
	extern int lineno;	// from assemble.cpp
	extern char * yytext;	// from lexer.lex
	extern bool pass1;	// from assemble.cpp
	extern ofstream ofile;	// from assemble.cpp
	extern int yylex ();	// from lexer.lex
	
	int yyerror ( char * );
	int address = 0;
	int filesize = 0;
	int errorcount = 0;
	
	Symtab symtab;
	
	OneRecord rec;
	
	const int size = sizeof ( OneRecord );
	
%}

%token ADD ADDI
%token AND ANDI NOR OR ORI
%token DIV MULT
%token SLL SLLV SRA SRAV SRL SRLV
%token SUB
%token XOR XORI
%token LUI
%token SLT SLTI
%token BEQ BGEZ BGTZ BLEZ BLTZ BNE
%token J JAL JALR JR
%token LW SW
%token MFHI MFLO MTHI MTLO
%token SYSCALL NOP
%token DIN DOUT RDIN RDOUT

%token DW START BEG END  

%token ERET
%token TEQ TEQI TGE TGEI TLT TLTI TNE TNEI
%token TLBP TLBR TLBW
%token MFC0 MTC0

%token REGISTER IDENTIFIER INTCONSTANT STRING

%start program

%union
{
	char id[100];
	int  value;
}

%type <value> INTCONSTANT REGISTER
%type <id>    IDENTIFIER STRING

%%

program: newlines begin_stmt newlines start_stmt instructions newlines end_stmt newlines
	| error
	;

begin_stmt: BEG INTCONSTANT '\n'
		{
			//address = $2;		//---------- 17/01/2006 ---------//
			address = 0;		//--- 19/03/2006 -- This is where userprogram starts
			rec.address = 0;
			rec.inst.iV = 0;
			
			//---------------- 19/01/2006 ------------------//
			/* This is the place where we need to put the header
			   in the object file. */
			if( ! pass1 )
			{
				ofile.write( reinterpret_cast<char*>(& filesize), sizeof(u_word_32) );
				ofile.write( reinterpret_cast<char*>(& rec.address), sizeof(rec.address) );
			}
			/* Need to modify Load_MIPS_program to run correctly */
			//----------------------------------------------//
				
		}
	;

start_stmt: START INTCONSTANT '\n'
		{
			if ( ! pass1 )
			{
				rec.address = 0;
				rec.inst.iV = $2;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
				address += 4;
			}
		}

/********************** 08/02/2006 - Rajeesh *******************/
	|  START IDENTIFIER '\n'
		{
			if( !pass1 )
			{
				unsigned int targAddr;
				if( ! symtab.LookUp ( $2, targAddr ) )
				{
                                        cerr << red << "\nLine:" << setw(4) << lineno
                                                << ", pass:" << setw(2) << (( pass1 )? 1 : 2)
                                                << ", Undefined Identifier : "
                                                << $2 << reset << "\n";
                                        errorcount ++;
                                }
				rec.address = 0;
				rec.inst.iV = targAddr;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
				address += 4;
			}
		}
/*************************************************************/
	;

end_stmt: END			//--------------- 19/01/2006 --------------//
	{
		filesize = address ;	// We need to find the size of the program,
					// including data segment to construct the
					// object file header in the second pass.
		std::cout << "filesie = " << filesize << "\n";
	}
	;			//-----------------------------------------//

newlines:
	| newlines '\n'
	;
	
instructions: newlines label_one_instruction
	| instructions newlines label_one_instruction
	;

label_one_instruction: IDENTIFIER ':' {
			if ( pass1) symtab.Insert ( $1, address );
		}
	| IDENTIFIER {
		if (pass1) symtab.Insert ( $1, address );
		}
	| one_instruction '\n'
	| error
	;

one_instruction: DW INTCONSTANT
		{
			if ( ! pass1 )
			{
				rec.address = address;
				rec.inst.iV = $2;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| DW '[' INTCONSTANT ']' 
		{
			address += 4 * $3;
		}
	| DW STRING
		{
			int length = strlen ( $2 );
			for ( int i = 0; i < length; i++ )
			{
				if ( ! pass1 )
				{
					rec.address = address;
					rec.inst.iV = static_cast<word_32>($2[i]);
					ofile.write ( reinterpret_cast<char*>(&rec), size );
				}
				address += 4;
			}
		}
	| ADD REGISTER ',' REGISTER ',' REGISTER
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = $4;
				rec.inst.rF.rt = $6;
				rec.inst.rF.rd = $2;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_ADD;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| ADDI REGISTER ',' REGISTER ',' INTCONSTANT
		{
			if ( ! pass1 )
			{
				rec.inst.iF.op = OP_ADDI;
				rec.inst.iF.rs = $4;
				rec.inst.iF.rt = $2;
				rec.inst.iF.imm = $6;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| ADDI REGISTER ',' REGISTER ',' IDENTIFIER
		{
			if ( ! pass1 )
			{
				unsigned int targAddr;
				if ( !symtab.LookUp ( $6, targAddr) ) // If no such entry
				{
					cerr << red << "\nLine:" << setw(4) << lineno 
						<< ", pass:" << setw(2)	<< (( pass1 )? 1 : 2) 
						<< ", Undefined Identifier : " 
						<< $4 << reset << "\n";
					errorcount ++;
				}
				rec.inst.iF.op = OP_ADDI;
				rec.inst.iF.rs = $4;
				rec.inst.iF.rt = $2;
				rec.inst.iF.imm = targAddr;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| AND REGISTER ',' REGISTER ',' REGISTER
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = $4;
				rec.inst.rF.rt = $6;
				rec.inst.rF.rd = $2;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_AND;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| ANDI REGISTER ',' REGISTER ',' INTCONSTANT
		{
			if ( !pass1 )
			{
				rec.inst.iF.op = OP_ANDI;
				rec.inst.iF.rt = $2;
				rec.inst.iF.rs = $4;
				rec.inst.iF.imm = $6;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| DIV REGISTER ',' REGISTER
		{
			if ( !pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = $2;
				rec.inst.rF.rt = $4;
				rec.inst.rF.rd = 0;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_DIV;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| MULT REGISTER ',' REGISTER
		{
			if ( !pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = $2;
				rec.inst.rF.rt = $4;
				rec.inst.rF.rd = 0;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_MULT;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| NOR REGISTER ',' REGISTER ',' REGISTER
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = $4;
				rec.inst.rF.rt = $6;
				rec.inst.rF.rd = $2;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_NOR;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| OR REGISTER ',' REGISTER ',' REGISTER
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = $4;
				rec.inst.rF.rt = $6;
				rec.inst.rF.rd = $2;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_OR;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| ORI REGISTER ',' REGISTER ',' INTCONSTANT
		{
			if ( !pass1 )
			{
				rec.inst.iF.op = OP_ORI;
				rec.inst.iF.rt = $2;
				rec.inst.iF.rs = $4;
				rec.inst.iF.imm = $6;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| SLL REGISTER ',' REGISTER ',' INTCONSTANT
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = 0;
				rec.inst.rF.rd = $2;
				rec.inst.rF.rt = $4;
				rec.inst.rF.shamt = $6;
				rec.inst.rF.funct = FUNCT_SLL;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| SLLV REGISTER ',' REGISTER ',' REGISTER
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = $6;
				rec.inst.rF.rd = $2;
				rec.inst.rF.rt = $4;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_SLLV;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| SRA REGISTER ',' REGISTER ',' INTCONSTANT
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = 0;
				rec.inst.rF.rd = $2;
				rec.inst.rF.rt = $4;
				rec.inst.rF.shamt = $6;
				rec.inst.rF.funct = FUNCT_SRA;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| SRAV REGISTER ',' REGISTER ',' REGISTER
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = $6;
				rec.inst.rF.rd = $2;
				rec.inst.rF.rt = $4;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_SRAV;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| SRL REGISTER ',' REGISTER ',' INTCONSTANT
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = 0;
				rec.inst.rF.rd = $2;
				rec.inst.rF.rt = $4;
				rec.inst.rF.shamt = $6;
				rec.inst.rF.funct = FUNCT_SRL;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| SRLV REGISTER ',' REGISTER ',' REGISTER
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = $6;
				rec.inst.rF.rd = $2;
				rec.inst.rF.rt = $4;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_SRLV;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| SUB REGISTER ',' REGISTER ',' REGISTER
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = $4;
				rec.inst.rF.rd = $2;
				rec.inst.rF.rt = $6;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_SUB;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| XOR REGISTER ',' REGISTER ',' REGISTER
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = $4;
				rec.inst.rF.rd = $2;
				rec.inst.rF.rt = $6;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_XOR;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| XORI REGISTER ',' REGISTER ',' INTCONSTANT
		{
			if ( ! pass1 )
			{
				rec.inst.iF.op = OP_XORI;
				rec.inst.iF.rt = $2;
				rec.inst.iF.rs = $4;
				rec.inst.iF.imm = $6;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| LUI REGISTER ',' INTCONSTANT
		{
			if ( ! pass1 )
			{
				rec.inst.iF.op = OP_LUI;
				rec.inst.iF.rt = $2;
				rec.inst.iF.rs = 0;
				rec.inst.iF.imm = ($4 >> 16);
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| LUI REGISTER ',' IDENTIFIER
		{
			if ( ! pass1 )
			{
				unsigned int targAddr;
				if ( !symtab.LookUp ( $4, targAddr) ) // If no such entry
				{
					cerr << red << "\nLine:" << setw(4) << lineno 
						<< ", pass:" << setw(2)	<< (( pass1 )? 1 : 2) 
						<< ", Undefined Identifier : " 
						<< $4 << reset << "\n";
					errorcount ++;
				}
				rec.inst.iF.op = OP_LUI;
				rec.inst.iF.rt = $2;
				rec.inst.iF.rs = 0;
				rec.inst.iF.imm = (targAddr >> 16);
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| SLT REGISTER ',' REGISTER ',' REGISTER
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = $4;
				rec.inst.rF.rt = $6;
				rec.inst.rF.rd = $2;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_SLT;
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| SLTI REGISTER ',' REGISTER ',' INTCONSTANT
		{
			if ( ! pass1 )
			{
				rec.inst.iF.op = OP_SLTI;
				rec.inst.iF.rs = $4;
				rec.inst.iF.rt = $2;
				rec.inst.iF.imm = $6;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| BEQ REGISTER ',' REGISTER ',' IDENTIFIER
		{
			if ( ! pass1 )
			{
				unsigned int targAddr;
				if ( !symtab.LookUp ( $6, targAddr) ) // If no such entry
				{
					cerr << red << "\nLine:" << setw(4) << lineno 
						<< ", pass:" << setw(2)	<< (( pass1 )? 1 : 2) 
						<< ", Undefined Identifier : " 
						<< $6 << reset << "\n";
					errorcount ++;
				}
				
				rec.inst.iF.op = OP_BEQ;
				rec.inst.iF.rs = $2;
				rec.inst.iF.rt = $4;
				rec.inst.iF.imm = (targAddr - address  ) / 4; //--------23/03/2006 .
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| BGEZ REGISTER ',' IDENTIFIER
		{
			if ( ! pass1 )
			{
				unsigned int targAddr;
				if ( !symtab.LookUp ( $4, targAddr) ) // If no such entry
				{
					cerr << red << "\nLine:" << setw(4) << lineno 
						<< ", pass:" << setw(2)	<< (( pass1 )? 1 : 2) 
						<< ", Undefined Identifier : " 
						<< $4 << reset << "\n";
					errorcount ++;
				}
				
				rec.inst.iF.op = OP_ONE;
				rec.inst.iF.rs = $2;
				rec.inst.iF.rt = OP_BGEZ;	// Distinguishing key
				rec.inst.iF.imm = (targAddr - address ) / 4;//--------note the point!!
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| BGTZ REGISTER ',' IDENTIFIER
		{
			if ( ! pass1 )
			{
				unsigned int targAddr;
				if ( !symtab.LookUp ( $4, targAddr) ) // If no such entry
				{
					cerr << red << "\nLine:" << setw(4) << lineno 
						<< ", pass:" << setw(2)	<< (( pass1 )? 1 : 2) 
						<< ", Undefined Identifier : " 
						<< $4 << reset << "\n";
					errorcount ++;
				}
				
				rec.inst.iF.op = OP_BGTZ;
				rec.inst.iF.rs = $2;
				rec.inst.iF.rt = 0;
				rec.inst.iF.imm = (targAddr - address ) / 4;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| BLEZ REGISTER ',' IDENTIFIER
		{
			if ( ! pass1 )
			{
				unsigned int targAddr;
				if ( !symtab.LookUp ( $4, targAddr) ) // If no such entry
				{
					cerr << red << "\nLine:" << setw(4) << lineno 
						<< ", pass:" << setw(2)	<< (( pass1 )? 1 : 2) 
						<< ", Undefined Identifier : " 
						<< $4 << reset << "\n";
					errorcount ++;
				}
				
				rec.inst.iF.op = OP_BLEZ;
				rec.inst.iF.rs = $2;
				rec.inst.iF.rt = 0;
				rec.inst.iF.imm = (targAddr - address ) / 4;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| BLTZ REGISTER ',' IDENTIFIER
		{
			if ( ! pass1 )
			{
				unsigned int targAddr;
				if ( !symtab.LookUp ( $4, targAddr) ) // If no such entry
				{
					cerr << red << "\nLine:" << setw(4) << lineno 
						<< ", pass:" << setw(2)	<< (( pass1 )? 1 : 2) 
						<< ", Undefined Identifier : " 
						<< $4 << reset << "\n";
					errorcount ++;
				}
				
				rec.inst.iF.op = OP_ONE;
				rec.inst.iF.rs = $2;
				rec.inst.iF.rt = OP_BLTZ;	// Distinguishing key
				rec.inst.iF.imm = (targAddr - address ) / 4;
				
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| BNE REGISTER ',' REGISTER ',' IDENTIFIER
		{
			if ( ! pass1 )
			{
				unsigned int targAddr;
				if ( !symtab.LookUp ( $6, targAddr) ) // If no such entry
				{
					cerr << red << "\nLine:" << setw(4) << lineno 
						<< ", pass:" << setw(2)	<< (( pass1 )? 1 : 2) 
						<< ", Undefined Identifier : " 
						<< $6 << reset << "\n";
					errorcount ++;
				}
				
				rec.inst.iF.op = OP_BNE;
				rec.inst.iF.rs = $2;
				rec.inst.iF.rt = $4;
				rec.inst.iF.imm = (targAddr - address ) / 4;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| J INTCONSTANT
		{
			if ( ! pass1 )
			{
				rec.inst.jF.op = OP_J;
				rec.inst.jF.tAddr = $2 / 4;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| J IDENTIFIER
		{
			if ( ! pass1 )
			{
				unsigned int targAddr;
				if ( !symtab.LookUp ( $2, targAddr) ) // If no such entry
				{
					cerr << red << "\nLine:" << setw(4) << lineno 
						<< ", pass:" << setw(2)	<< (( pass1 )? 1 : 2) 
						<< ", Undefined Identifier : " 
						<< $2 << reset << "\n";
					errorcount ++;
				}
				
				rec.inst.jF.op = OP_J;
				rec.inst.jF.tAddr = targAddr / 4;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| JAL INTCONSTANT
		{
			if ( ! pass1 )
			{
				rec.inst.jF.op = OP_JAL;
				rec.inst.jF.tAddr = $2 / 4;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| JAL IDENTIFIER
		{
			if ( ! pass1 )
			{
				unsigned int targAddr;
				if ( !symtab.LookUp ( $2, targAddr) ) // If no such entry
				{
					cerr << red << "\nLine:" << setw(4) << lineno 
						<< ", pass:" << setw(2)	<< (( pass1 )? 1 : 2) 
						<< ", Undefined Identifier : " 
						<< $2 << reset << "\n";
					errorcount ++;
				}
				
				rec.inst.jF.op = OP_JAL;
				rec.inst.jF.tAddr = targAddr / 4;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| JALR	REGISTER ',' REGISTER
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = $2;
				rec.inst.rF.rt = 0;
				rec.inst.rF.rd = $4;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_JALR;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| JR REGISTER
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = $2;
				rec.inst.rF.rt = 0;
				rec.inst.rF.rd = 0;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_JR;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| LW REGISTER ',' INTCONSTANT '(' REGISTER ')'
		{
			if ( ! pass1 )
			{
				rec.inst.iF.op = OP_LW;
				rec.inst.iF.rs = $6;
				rec.inst.iF.rt = $2;
				rec.inst.iF.imm = $4;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| SW REGISTER ',' INTCONSTANT '(' REGISTER ')'
		{
			if ( ! pass1 )
			{
				// Note that here rt contains the source and rs the
				// destination... to conform to the same format as LW
				rec.inst.iF.op = OP_SW;
				rec.inst.iF.rs = $6;
				rec.inst.iF.rt = $2;
				rec.inst.iF.imm = $4;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| MFHI REGISTER
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = 0;
				rec.inst.rF.rt = 0;
				rec.inst.rF.rd = $2;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_MFHI;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| MFLO REGISTER
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = 0;
				rec.inst.rF.rt = 0;
				rec.inst.rF.rd = $2;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_MFLO;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| MTHI REGISTER
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = $2;
				rec.inst.rF.rt = 0;
				rec.inst.rF.rd = 0;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_MTHI;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| MTLO REGISTER
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = $2;
				rec.inst.rF.rt = 0;
				rec.inst.rF.rd = 0;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_MTLO;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| SYSCALL
		{
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = 0;
				rec.inst.rF.rt = 0;
				rec.inst.rF.rd = 0;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_SYSCALL;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| NOP
		{
			if ( ! pass1 )
			{
				rec.inst.iV = 0;	// The entire thing is zero
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| DIN	REGISTER ',' INTCONSTANT
		{
			if ( ! pass1 )
			{
				rec.inst.iF.op = OP_DIN;
				rec.inst.iF.rs = 0;
				rec.inst.iF.rt = $2;
				rec.inst.iF.imm = $4;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| DOUT REGISTER ',' INTCONSTANT
		{
			if ( ! pass1 )
			{
				rec.inst.iF.op = OP_DOUT;
				rec.inst.iF.rs = $2;
				rec.inst.iF.rt = 0;
				rec.inst.iF.imm = $4;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| RDIN REGISTER ',' REGISTER 
		{	// reg2 port, reg1 data destination
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = 0;
				rec.inst.rF.rt = $4;
				rec.inst.rF.rd = $2;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_RDIN;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}
	| RDOUT REGISTER ',' REGISTER
		{
			// reg1 data source, reg2 port
			if ( ! pass1 )
			{
				rec.inst.rF.op = OP_ZERO;
				rec.inst.rF.rs = $2;
				rec.inst.rF.rt = $4;
				rec.inst.rF.rd = 0;
				rec.inst.rF.shamt = 0;
				rec.inst.rF.funct = FUNCT_RDOUT;
			
				rec.address = address;
				ofile.write ( reinterpret_cast<char*>(&rec), size );
			}
			address += 4;
		}

//------------------------ 13/02/2006 ------------------------- //
//------------------------   Rajeesh  ------------------------- //

/*
	Here goes the Parser code for the additional instructions. The following instructions
	are to be added :

	1.  ERET
	2.  TEQ rs,rt
	3.  TEQI rs,Imm
	4.  TGE rs,rt
	5.  TGEI rs,Imm
	6.  TLT rs,rt
	7.  TLTI rs,Imm
	8.  TNE rs,rt
	9.  TNEI rs,Imm
	10. TLBP
	11. TLBR
	12. TLBW

	Refer MIPS32 Instruction Set from page 121 onwards for the implementation.
*/

//--------------------------------------------------------------- //
// ----------- writing the various parser operations for the above given instructions -------------------- //


	| ERET 
		{
			// retrun from exception
			if( ! pass1)
			{
			rec.inst.rF.op=OP_COP0;
			rec.inst.rF.rs=0;
			rec.inst.rF.rd=0;
			rec.inst.rF.rt=0;
			rec.inst.rF.shamt=0;
			rec.inst.rF.funct=FUNCT_ERET;

			rec.address=address;
			ofile.write( reinterpret_cast<char*>(&rec),size);
			}
		 address +=4;
		}

	| TEQ REGISTER ',' REGISTER 
		{
			// trap if rs and at are equal
			if( ! pass1)
			{
			rec.inst.rF.op=OP_ZERO;
			rec.inst.rF.rs = $2;
			rec.inst.rF.rt = $4;
			rec.inst.rF.rd = 0;
			rec.inst.rF.shamt = 0;
			rec.inst.rF.funct = FUNCT_TEQ;

			rec.address = address;
			ofile.write(reinterpret_cast<char*>(&rec),size);
			}
		address+=4;
		}
		
	| TEQI REGISTER ',' INTCONSTANT
		{
			// trap if rs is equal to the immediate constant
			if( ! pass1 )
			{
			rec.inst.iF.op = OP_ONE;
			rec.inst.iF.rs = $2;
			rec.inst.iF.imm = $4;
  			rec.inst.rF.rt = OP_TEQI;

			rec.address = address ;
			ofile.write( reinterpret_cast<char *> (&rec) ,size);
			}
		address +=4;
		}
	| TGE REGISTER ',' REGISTER
		{
			// trap if rs greater than or equal to rt 	
			if( ! pass1 )
			{ 
			rec.inst.rF.op = OP_ZERO;
			rec.inst.rF.rs = $2;
			rec.inst.rF.rt = $4;
			rec.inst.rF.rd = 0;
			rec.inst.rF.shamt = 0;
			rec.inst.rF.funct = FUNCT_TGE;

			rec.address = address;
			ofile.write ( reinterpret_cast<char *>(&rec),size);	
			}
		address +=4;
		}
	| TGEI REGISTER ',' INTCONSTANT
		{
			// trap if rs is greater than or equal to immediate value
			if( ! pass1)
			{
			rec.inst.iF.op = OP_ONE ;
			rec.inst.iF.rs = $2;
			rec.inst.iF.imm = $4;
			rec.inst.rF.rt = OP_TGEI;

			rec.address = address ;
			ofile.write( reinterpret_cast<char *>(&rec),size);
			}
		address +=4;
		}
   	| TLT REGISTER ',' REGISTER
		{
			// trap if rs if less than rt
			if ( !pass1)
			{
			rec.inst.rF.op = OP_ZERO;
			rec.inst.rF.rs = $2 ;
			rec.inst.rF.rt = $4 ;
			rec.inst.rF.rd = 0  ;
			rec.inst.rF.shamt = 0;
			rec.inst.rF.funct = FUNCT_TLT;

			rec.address = address;
			ofile.write(reinterpret_cast<char*>(&rec), size);	
			}
		address +=4;
		}
	| TLTI REGISTER ',' INTCONSTANT
		{
			// trap if rs less than immediate
			if( !pass1 )
			{
			rec.inst.iF.op = OP_ONE;
			rec.inst.iF.rs = $2;
			rec.inst.iF.imm = $4;
			rec.inst.iF.rt = OP_TLTI;

			rec.address = address;
			ofile.write(reinterpret_cast<char*>(&rec),size);
			}
		address += 4;
		}
	
	| TNE REGISTER ',' REGISTER
		{ 
			// trap if rs not equal to rt
			if ( ! pass1 )
			{
			rec.inst.rF.op = OP_ZERO;
			rec.inst.rF.rs = $2;
			rec.inst.rF.rt = $4;
			rec.inst.rF.rd = 0;
			rec.inst.rF.shamt = 0;
			rec.inst.rF.funct = FUNCT_TNE;

			rec.address = address;
			ofile.write(reinterpret_cast<char*>(&rec),size);
			}
		address += 4;
		}

	| TNEI REGISTER ',' INTCONSTANT
		{
			// Raise Trap Exception if rs != Imm value
			if( ! pass1 )
			{
			rec.inst.iF.op  = OP_ONE;
			rec.inst.iF.rs  = $2;
			rec.inst.iF.imm = $4;
			rec.inst.iF.rt  = OP_TNEI;
			
			rec.address = address;
			ofile.write(reinterpret_cast<char*>(&rec),size);
			}
		address += 4;
		}

	| TLBP
		{
			// TLB probe for finding the entry matching EntryHi and EntryLo regs of CP0
			if ( !pass1)
			{
			rec.inst.rF.op = OP_COP0;
			rec.inst.rF.rs = 0;
			rec.inst.rF.rt = 0;
			rec.inst.rF.rd = 0;
			rec.inst.rF.shamt = 0;
			rec.inst.rF.funct = FUNCT_TLBP;
	
			rec.address = address;
			ofile.write(reinterpret_cast<char *>(&rec),size);
			}
		address +=4;
		}
        | TLBR
                {
                        // Read the indexed entry from the TLB - using Index register of CP0
                        if ( !pass1)
                        {
                        rec.inst.rF.op = OP_COP0;
                        rec.inst.rF.rs = 0;
                        rec.inst.rF.rt = 0;
			rec.inst.rF.rd = 0;
			rec.inst.rF.shamt = 0;
                        rec.inst.rF.funct = FUNCT_TLBR;

                        rec.address = address;
                        ofile.write(reinterpret_cast<char *>(&rec),size);
                        }
                address +=4;
                }
		
	
        | TLBW
                {
                        // Writing to the Indexed TLB entry - using Index register of CP0
                        if ( !pass1)
                        {
                        rec.inst.rF.op = OP_COP0;
                        rec.inst.rF.rs = 0;
                        rec.inst.rF.rt = 0;
			rec.inst.rF.rd = 0;
			rec.inst.rF.shamt = 0;
                        rec.inst.rF.funct = FUNCT_TLBW;

                        rec.address = address;
                        ofile.write(reinterpret_cast<char *>(&rec),size);
                        }
                address +=4;
                }

//--------------------------------------------------------------------------------------------------------//

//----------------------------------- 21/03/2006 ---------------------------------------------------------//
//-----------------------------------   Rajeesh  ---------------------------------------------------------//

/*	Special Instructions MFC0(Move From Coprocessor) and MTC0(Move To Coprocessor) are implemented
	below. The format is : " [MFC0/MTCO] rt, rd " , where 'rt' is a general purpose register and
	'rd' is a CP0 register. But, CP0 registers are given values greater than 32, so they can't be
	represented in 'rd' since it is only 5 bits wide. So, what we plan to do is, if $4 (ie, rd) value
	is greater than or equal to 32, then the unused 'rs' field is set to 1, otherwise 0. So on decoding
	the instruction, if 'rs' is set to 1, add 32 to the value in 'rd'.
*/

	| MFC0 REGISTER ',' REGISTER
		{
			// MFCO rt, rd	- rd will be a CP0 register, set rs=1 if rd >= 32
			if( ! pass1 )
			{
			rec.inst.rF.op = OP_COP0;
			if ( $4 >= 32 )
				rec.inst.rF.rs = 1;
			else
				rec.inst.rF.rs = 0;
			rec.inst.rF.rt = $2;
			rec.inst.rF.rd = $4;
			rec.inst.rF.shamt = 0;
			rec.inst.rF.funct = FUNCT_MFC0;

			rec.address = address;
			ofile.write(reinterpret_cast<char *>(&rec),size);
			}
		address +=4;
		}


        | MTC0 REGISTER ',' REGISTER
                {
                        // MTCO rt, rd  - rd will be a CP0 register, set rs=1 if rd >= 32
                        if( ! pass1 )
                        {
                        rec.inst.rF.op = OP_COP0;
                        if ( $4 >= 32 )
                                rec.inst.rF.rs = 1;
                        else
                                rec.inst.rF.rs = 0;
                        rec.inst.rF.rt = $2;
                        rec.inst.rF.rd = $4;
                        rec.inst.rF.shamt = 0;
                        rec.inst.rF.funct = FUNCT_MTC0;

                        rec.address = address;
                        ofile.write(reinterpret_cast<char *>(&rec),size);
                        }
                address +=4;
                }

//-----------------------------------------------------------------------------------------//


	;

%%

int yyerror(char *str)
{
	cerr << red << "\nLine:" << setw(4) << lineno << ", pass:" << setw(2)
		<< (( pass1 )? 1 : 2) << ", parser encountered error : " 
		<< str << ", in the vicinity of token : " << yytext << reset << "\n";
	errorcount ++;
	return 0;
}
