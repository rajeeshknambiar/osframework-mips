
%{
/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/
 
	#include "asm.tab.hpp"
	
	#include <iostream>
	using std::cout;
	using std::cerr;
	#include <iomanip>
	using std::setw;
	
	#include "color.h"
	
	extern int lineno;	// from main.cpp
	extern int errorcount;	// from asm.ypp
	extern bool pass1;	// from main.cpp
	
	char stringbuffer[100];
	int stringindex;
%}

%x string

%%

"begin"		cout << " " << yytext ; return BEG;
"start"		cout << " " << yytext ; return START;
"end"		cout << " " << yytext ; return END;

"add"		cout << " " << yytext ; return ADD;
"addi"		cout << " " << yytext ; return ADDI;

"and"		cout << " " << yytext ; return AND;
"andi"		cout << " " << yytext ; return ANDI;
"nor"		cout << " " << yytext ; return NOR;
"or"		cout << " " << yytext ; return OR;
"ori"		cout << " " << yytext ; return ORI;

"div"		cout << " " << yytext ; return DIV;
"mult"		cout << " " << yytext ; return MULT;

"sll"		cout << " " << yytext ; return SLL;
"sllv"		cout << " " << yytext ; return SLLV;
"sra"		cout << " " << yytext ; return SRA;
"srav"		cout << " " << yytext ; return SRAV;
"srl"		cout << " " << yytext ; return SRL;
"srlv"		cout << " " << yytext ; return SRLV;

"sub"		cout << " " << yytext ; return SUB;

"xor"		cout << " " << yytext ; return XOR;
"xori"		cout << " " << yytext ; return XORI;

"lui"		cout << " " << yytext ; return LUI;

"slt"		cout << " " << yytext ; return SLT;
"slti"		cout << " " << yytext ; return SLTI;

"beq"		cout << " " << yytext ; return BEQ;
"bgez"		cout << " " << yytext ; return BGEZ;
"bgtz"		cout << " " << yytext ; return BGTZ;
"blez"		cout << " " << yytext ; return BLEZ;
"bltz"		cout << " " << yytext ; return BLTZ;
"bne"		cout << " " << yytext ; return BNE;

"j"		cout << " " << yytext ; return J;
"jal"		cout << " " << yytext ; return JAL;
"jalr"		cout << " " << yytext ; return JALR;
"jr"		cout << " " << yytext ; return JR;

"lw"		cout << " " << yytext ; return LW;
"sw"		cout << " " << yytext ; return SW;

"mfhi"		cout << " " << yytext ; return MFHI;
"mflo"		cout << " " << yytext ; return MFLO;
"mthi"		cout << " " << yytext ; return MTHI;
"mtlo"		cout << " " << yytext ; return MTLO;

"syscall"	cout << " " << yytext ; return SYSCALL;
"nop"		cout << " " << yytext ; return NOP;

"din"		cout << " " << yytext ; return DIN;
"dout"		cout << " " << yytext ; return DOUT;
"rdin"		cout << " " << yytext ; return RDIN;
"rdout"		cout << " " << yytext ; return RDOUT;

"dw"		cout << " " << yytext ; return DW;

	/*Exception Return */

"eret"		cout << " " << yytext ; return ERET;

	/* Instructions for TRAP */

"teq"		cout << " " << yytext ; return TEQ;
"teqi"		cout << " " << yytext ; return TEQI;
"tge"		cout << " " << yytext ; return TGE;
"tgei"		cout << " " << yytext ; return TGEI;
"tlt"		cout << " " << yytext ; return TLT;
"tlti"		cout << " " << yytext ; return TLTI;
"tne"		cout << " " << yytext ; return TNE;

	/*Instructions for TLB probe, read and write */

"tlbp"		cout << " " << yytext ; return TLBP;
"tlbr"		cout << " " << yytext ; return TLBR;
"tlbw"		cout << " " << yytext ; return TLBW;

        /*Instructions to move from and to the Coprocessor*/

"mfc0"		cout << " " << yytext; return MFC0;
"mtc0"		cout << " " << yytext; return MTC0;


	/* Registers */
	
"$zero"	|
"$0"	yylval.value = 0; cout << " " << yytext ; return REGISTER;

"$at"	|
"$1"	yylval.value = 1; cout << " " << yytext ; return REGISTER;

"$v0"	|
"$2"	yylval.value = 2; cout << " " << yytext ; return REGISTER;

"$v1"	|
"$3"	yylval.value = 3; cout << " " << yytext ; return REGISTER;

"$a0"	|
"$4"	yylval.value = 4; cout << " " << yytext ; return REGISTER;

"$a1"	|
"$5"	yylval.value = 5; cout << " " << yytext ; return REGISTER;

"$a2"	|
"$6"	yylval.value = 6; cout << " " << yytext ; return REGISTER;

"$a3"	|
"$7"	yylval.value = 7; cout << " " << yytext ; return REGISTER;

"$t0"	|
"$8"	yylval.value = 8; cout << " " << yytext ; return REGISTER;

"$t1"	|
"$9"	yylval.value = 9; cout << " " << yytext ; return REGISTER;

"$t2"	|
"$10"	yylval.value = 10; cout << " " << yytext ; return REGISTER;

"$t3"	|
"$11"	yylval.value = 11; cout << " " << yytext ; return REGISTER;

"$t4"	|
"$12"	yylval.value = 12; cout << " " << yytext ; return REGISTER;

"$t5"	|
"$13"	yylval.value = 13; cout << " " << yytext ; return REGISTER;

"$t6"	|
"$14"	yylval.value = 14; cout << " " << yytext ; return REGISTER;

"$t7"	|
"$15"	yylval.value = 15; cout << " " << yytext ; return REGISTER;

"$s0"	|
"$16"	yylval.value = 16; cout << " " << yytext ; return REGISTER;

"$s1"	|
"$17"	yylval.value = 17; cout << " " << yytext ; return REGISTER;

"$s2"	|
"$18"	yylval.value = 18; cout << " " << yytext ; return REGISTER;

"$s3"	|
"$19"	yylval.value = 19; cout << " " << yytext ; return REGISTER;

"$s4"	|
"$20"	yylval.value = 20; cout << " " << yytext ; return REGISTER;

"$s5"	|
"$21"	yylval.value = 21; cout << " " << yytext ; return REGISTER;

"$s6"	|
"$22"	yylval.value = 22; cout << " " << yytext ; return REGISTER;

"$s7"	|
"$23"	yylval.value = 23; cout << " " << yytext ; return REGISTER;

"$t8"	|
"$24"	yylval.value = 24; cout << " " << yytext ; return REGISTER;

"$t9"	|
"$25"	yylval.value = 25; cout << " " << yytext ; return REGISTER;

"$k0"	|
"$26"	yylval.value = 26; cout << " " << yytext ; return REGISTER;

"$k1"	|
"$27"	yylval.value = 27; cout << " " << yytext ; return REGISTER;

"$gp"	|
"$28"	yylval.value = 28; cout << " " << yytext ; return REGISTER;

"$sp"	|
"$29"	yylval.value = 29; cout << " " << yytext ; return REGISTER;

"$fp"	|
"$30"	yylval.value = 30; cout << " " << yytext ; return REGISTER;

"$ra"	|
"$31"	yylval.value = 31; cout << " " << yytext ; return REGISTER;
	
	/*----------Added on 20/03/2006---------------------*/

"$hi"	|
"$32"	yylval.value = 32; cout << " " << yytext ; return REGISTER;
	
"$lo"	|
"$33"	yylval.value = 33; cout << " " << yytext ; return REGISTER;

"$pc"	|
"$34"	yylval.value = 34; cout << " " << yytext ; return REGISTER;

"$npc"	|
"$35"	yylval.value = 35; cout << " " << yytext ; return REGISTER;

"$ind"	|
"$36"	yylval.value = 36; cout << " " << yytext ; return REGISTER;

"$ehi"	|
"$37"	yylval.value = 37; cout << " " << yytext; return REGISTER;

"$elo1"  |
"$38"   yylval.value = 38; cout << " " << yytext; return REGISTER;

"$elo0"  |
"$39"   yylval.value = 39; cout << " " << yytext; return REGISTER;

"$ctx"	|
"$40"	yylval.value = 40; cout << " " << yytext ; return REGISTER;

"$pm"	|
"$41"	yylval.value = 41; cout << " " << yytext ; return REGISTER;

"$bv"	|
"$42"	yylval.value = 42; cout << " " << yytext ; return REGISTER;

"$cnt"  |
"$43"   yylval.value = 43; cout << " " << yytext ; return REGISTER;

"$cmp"  |
"$44"   yylval.value = 44; cout << " " << yytext ; return REGISTER;

"$cs"   |
"$45"   yylval.value = 45; cout << " " << yytext ; return REGISTER;

"$epc"   |
"$46"   yylval.value = 46; cout << " " << yytext ; return REGISTER;
	/*	Rajeesh.  Status Register (SR). Added on 25/01/2008	*/
"$sr"	|
"$47"   yylval.value = 47; cout << " " << yytext ; return REGISTER;



		/* Identifiers */
([a-zA-Z_])([a-zA-Z_0-9])*	{
	strcpy(yylval.id,yytext); cout << " " << yytext ; return IDENTIFIER;
	}

	/* Integer constants */
([0-9])*	yylval.value = atoi(yytext); cout << " " << yytext ; return INTCONSTANT;
\-([0-9])*	yylval.value = atoi(yytext); cout << " " << yytext ; return INTCONSTANT;
\'[0-9a-zA-Z\-]\'	{
		yylval.value = static_cast<int>(yytext[1]);
		cout << " " << yytext ; return INTCONSTANT;
	}
\'\\n\'		{
		yylval.value = static_cast<int>('\n');
		cout << " " << yytext ; return INTCONSTANT;
	}
\'\\r\'		{
		yylval.value = static_cast<int>('\r');
		cout << " " << yytext ; return INTCONSTANT;
	}
\'\\t\'		{
		yylval.value = static_cast<int>('\t');
		cout << " " << yytext ; return INTCONSTANT;
	}
\'\\\"\'		{
		yylval.value = static_cast<int>('\"');
		cout << " " << yytext ; return INTCONSTANT;
	}
\'\\\'\'		{
		yylval.value = static_cast<int>('\'');
		cout << " " << yytext ; return INTCONSTANT;
	}

	/* Punctuators */
\:	|
\[	|
\]	|
\(	|
\)	|
\,		cout << " " << yytext ; return yytext[0];

\n	lineno ++; cout << " " << yytext ; return yytext[0];

([\t ])+	//For the moment we are ignoring white spaces;

	/* Single line comment */
\#[^\n]*	cout << " " << yytext ; lineno ++; /* Ignore comments. */


\"	stringindex = 0; BEGIN(string);
<string>\"	{ 
		// saw the closing quote, all done
		BEGIN(INITIAL);
		stringbuffer[stringindex] = '\0';
		cout << " \"" << stringbuffer << "\"";
		strcpy ( yylval.id, stringbuffer );
		return STRING;
	}
<string>\n	{
		cerr << red << "\nLine:" << setw(4) << lineno << ", pass:" << setw(2)
			<< (( pass1 )? 1 : 2)
			<< ", Unterminated string constant" << reset << "\n";
		errorcount ++;
		BEGIN(INITIAL);
	}
<string>\\n	stringbuffer[stringindex++] = '\n';
<string>\\r	stringbuffer[stringindex++] = '\r';
<string>\\t	stringbuffer[stringindex++] = '\t';
<string>\\\\	stringbuffer[stringindex++] = '\\';
<string>\\\"	stringbuffer[stringindex++] = '\"';
<string>\\'	stringbuffer[stringindex++] = '\'';
<string>[^\\\n\"]	{
		char * yptr = yytext;
		while ( *yptr )
			stringbuffer[stringindex++] = *yptr++;
	}

	

	/* Anything not matched so far results in an error */
.	{
		cerr << red << "\nLine:" << setw(4) << lineno << ", pass:" << setw(2)
			<< (( pass1 )? 1 : 2)
			<< ", Invalid lexicon : " << yytext << reset << "\n";
		errorcount ++;
	}
