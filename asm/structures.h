/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

 
# ifndef __STRUCTURES_H
# define __STRUCTURES_H

class SymtabEntry
{
public:
	char id[100];
	unsigned int address;
	SymtabEntry * next;
	
	SymtabEntry ( );
};

class Symtab
{
private:
	SymtabEntry * first;

public:
	Symtab ( );
	~Symtab ( );
	
	bool Insert ( char * id, unsigned int address );
	
	bool LookUp ( char * id );
	
	bool LookUp ( char * id, unsigned int & address );
	
	void Display ( );
};

# endif
