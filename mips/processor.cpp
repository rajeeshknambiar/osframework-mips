/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

 

//#include "processor.h"

#include "main.h"
#include <iostream>
using std::cout;
using std::flush;
#include <cstdlib>
using std::exit;

#include <fcntl.h>
#include <signal.h>

#include "color.h"



/**********************************************************************************
**********************Constructor and Destructor functions************************/

Processor :: Processor ( MainMemory * m, Cache * dc, Cache * ic, PortManager * pm )
{
	//DEBUG(dbgMachine, skyblue << "\nThe Processor Initialized" << reset);
	mem = m;
	dataCache = dc;
	instrCache = ic;
	pman = pm;
	requestProgramTermination = false;
	blockUpdate = false;

//-------------------- 20/02/2006 ----------------------//
//--------------------   Rajeesh  ----------------------//
	
	IsException = false;
	//InterruptEnabled = true;	// 06/02/2008
	
//------------------------------------------------------//	
	
	for ( int i = 0; i < 32 ; i++ )
		reg[i] = 0;
	Hi = Lo = 0;
	
	for ( int i = 0; i < 5; i++ )
	{
		inLatch[i].Initialise ( );
		outLatch[i].Initialise ( );
		flushStage[i] = false;
	}
	PCreg = SYSTEM_START_ADDRESS;
	
	continueCount = 0;
	for ( int i = 0; i < BREAKPOINTARRAYSIZE; i++ )
		breakPointArray[i] = -1;
}

Processor :: ~Processor ( )
{
	AtExit ( );
}

void Processor :: AtExit ( )
{
	// Kill all the threads before closing semaphores.
	for ( int i = 0; i < 5; i++ )
		pthread_kill ( stagethread[i], SIGKILL );
	
	// Close the stage semaphores
	for ( int i = 0; i < 5; i++ )
	{
		int stagesem_value;
		if ( sem_getvalue ( stagesem[i], &stagesem_value ) == -1 )
			cout << red << "\nError polling the value of stagesem["
				<< i << "]" << reset << flush;
		else
			cout << gray << "\nThe value of stagesem[" << i << "] at closing = "
				<< stagesem_value << reset << flush;
			
		sem_close ( stagesem[i] );
	}
	
	int clocksem_value;
	if ( sem_getvalue ( clocksem, &clocksem_value ) == -1 )
		cout << red << "\nError polling the value of clocksem"
			<< reset << flush;
	else
		cout << gray << "\nThe value of clocksem at closing = "
			<< clocksem_value << reset << flush;
	sem_close ( clocksem );
	
	int pc_mutex_value;
	if ( sem_getvalue ( pc_mutex, &pc_mutex_value ) == -1 )
		cout << red << "\nError polling the value of pc_mutex"
			<< reset << flush;
	else
		cout << gray << "\nThe value of pc_mutex at closing = "
			<< pc_mutex_value << reset << flush;
	sem_close ( pc_mutex );
	
	sem_unlink ( "clocksem" );
	sem_unlink ( "stagesem1" );
	sem_unlink ( "stagesem2" );
	sem_unlink ( "stagesem3" );
	sem_unlink ( "stagesem4" );
	sem_unlink ( "stagesem5" );
	sem_unlink ( "pcmutex" );
}

void Processor :: Terminate ( )
{
	DEBUG(dbgMachine, red << "\n[Processor::Terminate] Terminating the Processor Execution!!");
	requestProgramTermination = true;
}




/*********************************************************************************
*******************Threading initialisation**************************************/

void* stage0 ( void * pobj ) ;
void* stage1 ( void * pobj ) ;
void* stage2 ( void * pobj ) ;
void* stage3 ( void * pobj ) ;
void* stage4 ( void * pobj ) ;
	
void Processor :: Execute ( )
{

	DEBUG(dbgMachine, green <<"\n[Processor::Execute] Processor Starting execution !!");
	clocksem = sem_open ( "clocksem", O_CREAT | O_EXCL, O_RDWR, 5 );
	if ( clocksem == NULL )
	{
		sem_unlink ( "clocksem" );
		clocksem = sem_open ( "clocksem", O_CREAT, O_RDWR, 5 );
	}
	
	pc_mutex = sem_open ( "pcmutex", O_CREAT | O_EXCL, O_RDWR, 1 );
	if ( pc_mutex == NULL )
	{
		sem_unlink ( "pcmutex" );
		pc_mutex = sem_open ( "pcmutex", O_CREAT, O_RDWR, 1 );
	}
	
	stagesem[0] = sem_open ( "stagesem1", O_CREAT | O_EXCL, O_RDWR, 0 );
	if ( stagesem[0] == NULL )
	{
		sem_unlink ( "stagesem1" );
		stagesem[0] = sem_open ( "stagesem1", O_CREAT, O_RDWR, 0 );
	}
	
	stagesem[1] = sem_open ( "stagesem2", O_CREAT | O_EXCL, O_RDWR, 0 );
	if ( stagesem[1] == NULL )
	{
		sem_unlink ( "stagesem2" );
		stagesem[1] = sem_open ( "stagesem2", O_CREAT, O_RDWR, 0 );
	}
	
	stagesem[2] = sem_open ( "stagesem3", O_CREAT | O_EXCL, O_RDWR, 0 );
	if ( stagesem[2] == NULL )
	{
		sem_unlink ( "stagesem3" );
		stagesem[2] = sem_open ( "stagesem3", O_CREAT, O_RDWR, 0 );
	}
	
	stagesem[3] = sem_open ( "stagesem4", O_CREAT | O_EXCL, O_RDWR, 0 );
	if ( stagesem[3] == NULL )
	{
		sem_unlink ( "stagesem4" );
		stagesem[3] = sem_open ( "stagesem4", O_CREAT, O_RDWR, 0 );
	}
	
	stagesem[4] = sem_open ( "stagesem5", O_CREAT | O_EXCL, O_RDWR, 0 );
	if ( stagesem[4] == NULL )
	{
		sem_unlink ( "stagesem5" );
		stagesem[4] = sem_open ( "stagesem5", O_CREAT, O_RDWR, 0 );
	}
	
	if ( clocksem == NULL || pc_mutex == NULL || stagesem[0] == NULL ||
		 stagesem[1] == NULL || stagesem[2] == NULL ||
		 stagesem[3] == NULL || stagesem[4] == NULL)
	{
		cout << red << "\nError, Some of the semaphores couldn't be opened." 
			<< "\nTerminating...\n" << red << flush;
		exit (0);
	}
	
	if( pthread_create ( &stagethread[0], NULL, &stage0, this ) )
		cout << "Error creating thread.\n";
	if( pthread_create ( &stagethread[1], NULL, &stage1, this ) )
		cout << "Error creating thread.\n";
	if( pthread_create ( &stagethread[2], NULL, &stage2, this ) )
		cout << "Error creating thread.\n";
	if( pthread_create ( &stagethread[3], NULL, &stage3, this ) )
		cout << "Error creating thread.\n";
	if( pthread_create ( &stagethread[4], NULL, &stage4, this ) )
		cout << "Error creating thread.\n";
	
	ExecutionThread ( );	// This thread now becomes the clockmanager...
}

int Processor::clock_count = 0 ;		// In globalvars.h

void Processor :: ExecutionThread ( )
{
	//int clock_count = 0;
	do
	{
		for ( int i = 0; i < 5; i++ )
			sem_wait ( clocksem );
		
		Clock( clock_count ++ );		// In globalvars.h
		// Note that clock count is incremented

//------------------------- 16/03/2006 ----------------------------//
//-------------------------   Rajeesh  ----------------------------//
//	Check here for any exception, if so, do not signal the stagesems.
	
		while( IsException );

		// Execution gets paused here until IsException is set to false at somewhere
		// When processing is completed, just IsException could be set to false //TODO
	// Once the exception processing is complete, signal the stagesems !!
//----------------------------------------------------------------//

		for ( int i = 0; i < 5; i++ )
		sem_post ( stagesem[i] );
	
	} while ( true );
}

/*******************************************************************************
******************Thread Entry Points******************************************/
// These are friend functions which call a corresponding Stage function in the 
// processor... This approach is necessary because we cannot take the address of
// a class member function to provide it to pthread_create API

void* stage0 ( void * pobj )
{
	Processor * p = reinterpret_cast<Processor*> (pobj);
	
	do
	{
		sem_wait ( p -> stagesem[0] );
		
		p -> Stage0 ( );
		
		sem_post ( p -> clocksem );
		
	} while ( true );
	
	return NULL;
}

void* stage1 ( void * pobj )
{
	Processor * p = reinterpret_cast<Processor*> (pobj);
	
	do
	{
		sem_wait ( p -> stagesem[1] );
		
		p -> Stage1 ( );
		
		sem_post ( p -> clocksem );
		
	} while ( true );
	
	return NULL;
}

void* stage2 ( void * pobj )
{
	Processor * p = reinterpret_cast<Processor*> (pobj);
	
	do
	{
		sem_wait ( p -> stagesem[2] );
		
		p -> Stage2 ( );
		
		sem_post ( p -> clocksem );
		
	} while ( true);
	
	return NULL;
}

void* stage3 ( void * pobj )
{
	Processor * p = reinterpret_cast<Processor*> (pobj);
	
	do
	{
		sem_wait ( p -> stagesem[3] );
		
		p -> Stage3 ( );
		
		sem_post ( p -> clocksem );
		
	} while ( true );
	
	return NULL;
}

void* stage4 ( void * pobj )
{
	Processor * p = reinterpret_cast<Processor*> (pobj);
	
	do
	{
		sem_wait ( p -> stagesem[4] );
		
		p -> Stage4 ( );
		
		sem_post ( p -> clocksem );
		
	} while ( true );
	
	return NULL;
}

void Processor :: SetException(bool TorF)
{
	IsException = TorF;
}

//--- Commented out and reimplemented in CP0 on 06/02/2008 by Rajeesh ---//
/*
bool Processor :: SetInterruptStatus( bool TorF )
{
	bool oldStatus = InterruptEnabled;
	InterruptEnabled = TorF;
	return oldStatus;
}

bool Processor :: GetInterruptStatus()
{
	return InterruptEnabled;
}
 */
//-----------------------------------------------------------------------//


//---------------------------- 20/02/2006 --------------------------------//
//----------------------------   Rajeesh  --------------------------------//

/*
*  Here goes the implementation of methods of SystemControlCoprocessor, CP0.
*  The major functions are SignalException and SwitchMode.
*/


word_32 SystemControlCoprocessor :: Index = 0 ;		// -- 03/04/2006 --
						// As Index is a static varibale, it has to be initialised like this

SystemControlCoprocessor :: SystemControlCoprocessor()
{
	//DEBUG(dbgMachine, skyblue << "\nThe System Control Coprocessor Initialized" << reset);
	
	EPC 	  = 0 ;
	Context   = 0 ;
	Cause 	  = 0 ;
	Count	  = 10;	// Timer Interrupt on every 10 clocks...
	BadVAddr  = 0 ;
	Index     = 0 ;
	EntryHi   = 0 ;
	EntryLo1  = 0 ;
	EntryLo0  = 0 ;
	StatusReg = 0 ;	// Interrupt Disabled by default

	Mode	 = UserMode;	// Initially in UserMode

	TLB 	 = new TranslationEntry[TLBSIZE];	// A 48 entry TLB
}

SystemControlCoprocessor :: ~SystemControlCoprocessor()
{
	delete []TLB;
}

//--- Reimplemented from Processor on 06/02/2008 by Rajeesh ---//

bool SystemControlCoprocessor :: SetInterruptStatus( bool TorF )
{
	bool oldStatus = StatusReg & 0xFFFFFF;	// Get the last bit
	if (TorF == true )
		StatusReg |= 0x1;	// Set the last bit
	else
		StatusReg &= 0xFFFFFE;  // doing an AND with 1110
	return oldStatus;
}

bool SystemControlCoprocessor :: GetInterruptStatus()
{
	// Do and AND with 1111
	return ( StatusReg & 0xFFFFFF );
}
//-----------------------------------------------------------------------//


#include "errorhandler.h" 	// for TLBMissHandler TODO - Put its declaration in some '.h' file
					// and include that '.h' file here !!

Status mode ;		// To save the Mode on a context switch

void SystemControlCoprocessor :: SignalException( ExceptionType Type )
{       
//	FlushStage();		// Flushing out the instruction in pipeline...	 Don't Do the Stage4
//	SaveRegisterValues (); 	// To save register values for OS use !!
	//	Do the Interrupt Handling here....

	// Switch the execution mode to KernelMode here..., will be reset later by ERET instruction in Stage2	
	/*if( Mode == false )
		mode = UserMode;
	else
		mode = KernelMode ; */

/*	23/01/2008
 *	Rajeesh		- TODO
 *	  Here, we didn't check whether the Interrupts are disabled or not.
 *	  If Interrupts are disabled, we shouldn't allow them, huh?
 *	  Need to look into detail. Whether InerruptEnabled bit is set where
 *	  this method is called, etc.
 */

	//--- Added on 06/02/2008 by Rajeesh ---//
	SetInterruptStatus (false );
	//--------------------------------------//
	mode = Mode;
	SwitchMode ( KernelMode );
	
	//DEBUG(dbgMachine,"\nSignal Exception in System control Coprocessor !!");
	
	switch ( Type )
	{

		// The control must be transferred to the OS routine from here 

		case TLBMISS:
				SaveRegisterValues();

				FlushStage();
				
				//cout << red << "\n[SignalException] TLBMISS Occured...!!!" << flush;
				
				DEBUG(dbgMachine,"\n[CP0::SignalException] TLBMISS Occured...");
				
				// Write the code of operating system routine to handle TLB Miss and
				// call it from here.
				// The OS routine must find the missing entry by looking at the correct
				// PageTable entry of current process and put it in TLB
				
				// I need to set Index register also here !!
				Index = (Index+1) % TLBSIZE ;	// FIFO ... TODO - to be manipulated by OS !!
				//proc->blockUpdate = false;	
				//Entry = TLBMissHandler( Index, BadVAddr );	// This method isn't correct, rewrite !!
				TLBMissHandler( Index, BadVAddr );	// This method isn't correct, rewrite !!
				/*	
				if ( Entry.physicalPage == -1 )	// -- 24/03/2006
				{
					sem_wait( cout_mutex );
					cout << red << "\nMemory protection violation !! Terminating..." << reset;
					sem_post( cout_mutex );
					proc -> Terminate();	// Quitting....
								// Don't actually quit, return to the OS handler...
				}
				*/
				// else - execute following steps...

				StartingAddress = 4 ;	// -- 24/03/2006 -- We are going to execute ISR, which uses
							// LW, which adds StartingAddress to Immediate value,
							// but, ISR's StartingAddress is 4. After the ERET, set this
							// back to SYSTEM_START_ADDRESS ...

				proc->PCreg = TLBWAddress;// For TLBW instruction !! 19/03/2006 -- Better use MACROs...
				
				//SwitchMode ( mode ); // To be done at ERET !!
				
				proc->SetException ( false ); 		// Resumes stalled execution here !!!

				break;
		
		case SYSCALL:
				
				FlushStage();
				/*
				sem_wait( cout_mutex );
				cout << violet << "\nSYSCALL Exception Occured...!!" << reset;
				sem_post( cout_mutex );*/

				DEBUG(dbgMachine,violet << "\n[CP0::SignalException] SYSCALL Exception Occured...");
				DEBUG(dbgMachine,violet << "\n[CP0::SignalException] The SYSCALL code is " << proc->reg[4]);

				// The operating system call goes here ..
				// Passing the argument register value to handler...
				SaveRegisterValues (); 	// To save register values for OS use !!

				ErrorHandler( SYSCALL );
				
				RestoreRegisterValues();

				//proc -> UpdatePC_Stage2( EPC , PC_ABSOLUTE );	// Restore PC to EPC 19/03/2006
				proc -> PCreg = EPC ;		// Restore PCreg to EPC

				//cout << violet << "The value is " << proc->reg[2]<< reset ;
				
				//char ch;
				//std::cin >> ch;
				proc->SetException ( false );
				SwitchMode ( mode );	// To be done at ERET - Do not switch to UserMode here - we
				//are just about to start the syscall

				break;

		case TIMERINTERRUPT:

				/*if( mode == KernelMode )
				{
					Count = 10 ;
					proc->SetException ( false );
					proc->SetInterruptStatus( true );
					break;
				}*/
				//proc->SetException ( true );
				
				//EPC = proc -> outLatch[1].PC ;// We need to restart execution from Stage3
				
				//FlushStage();
				/*
				sem_wait( cout_mutex );
				cout << red << "\n\tTIMER Interrupt Occured...!!\n" << reset;
				sem_post( cout_mutex );*/
				DEBUG(dbgMachine,red << "\n\tTIMER Interrupt Occured...!!\n" );

				SaveRegisterValues();

				//SignalTimerInterrupt();
				ErrorHandler( TIMERINTERRUPT );
				
				RestoreRegisterValues();
				//Count = 10 ;		// Restore the Count register value
				//proc -> PCreg = EPC ;
				proc->SetException ( false );
				SwitchMode( mode );
				//proc->SetInterruptStatus ( true );
				
				break;

		case RESERVED_INSTRUCTION:
				
				DEBUG( dbgMachine, red << "\n[ Stage1 ] Invalid use of Privileged Instruction!!" );
				
				FlushStage();

				ErrorHandler( RESERVED_INSTRUCTION );
				
				//proc->SetException ( false );	// The ErrorHandler currently calls proc->Terminate
				//SwitchMode( mode );		// which will destrou proc, CP0 and all, comment out for now
				
				break;

				
		case TRAP:
				
				//The operating system call goes here ..

				ErrorHandler( TRAP );
				
				//SwitchMode( mode );	// To be done ar ERET
				break;
		case OVERFLOW:
				/*
				sem_wait( cout_mutex );
				cout << red << "\n\tArithmetic Overflow... Exiting...!!\n" << reset;
				sem_post( cout_mutex );*/
				DEBUG(dbgMachine,red << "\n\tArithmetic Overflow... Exiting...!!\n");

				ErrorHandler( OVERFLOW );
				
				proc->SetException ( false );
				SwitchMode( mode );
				
				//proc->Terminate();	// Quit Execution
				break;	

		case DIVIDE_ERROR :
				 DEBUG(dbgMachine,red << "\n\tDivide by Zero... Exiting...!!\n");
				 
				 ErrorHandler( DIVIDE_ERROR );

				 proc->SetException ( false );
				 SwitchMode( mode );

				 break;


		case BADADDRESS:
				/*
				sem_wait( cout_mutex );
				cout << red << "\n\tBad Address Error... Exiting...!!\n" << reset;
				sem_post( cout_mutex );*/
				DEBUG(dbgMachine,red << "\n\tBad Address Error... Exiting...!!\n");
				
				ErrorHandler( BADADDRESS );

				proc->SetException ( false );
				SwitchMode( mode );
				
				//proc->Terminate();      // Quit Execution
				break;
				
		case ALIGNMENTERROR:
				/*
				sem_wait( cout_mutex );
				cout << red << "\n\tAddress Alignment Error... Exiting...!!\n" << reset;
				sem_post( cout_mutex );*/
				DEBUG(dbgMachine,red << "\n\tAddress Alignment Error... Exiting...!!\n");

				ErrorHandler( ALIGNMENTERROR );

				proc->SetException ( false );
				SwitchMode( mode );
				//proc->Terminate();      // Quit Execution

				break;
		case NOEXCEPTION:

				//	Nothing to do... Perfect !!! Just to avoid warning by g++
				SwitchMode( mode );
				break;
	}

//	BadVAddr = 0;
//	EPC = 0;
	proc->blockUpdate = false;	// Caused a huge bug !! This will be set by FlushStage() -- 22/03/06 - Rajeesh
	
}

void SystemControlCoprocessor :: WriteTLBIndex()
	{
	/*	sem_wait( cout_mutex );
		cout << red << "\nWriting TLB entry : " << EntryHi << ","
			<< EntryLo1 << "," << EntryLo0 <<" to " << Index;
		sem_post( cout_mutex );
	*/
		DEBUG(dbgMachine,red <<"\n[CP0::WriteTLBIndex] Writing TLB entry !!!");	
		TLB[ Index ].virtualPage = EntryHi ;
		TLB[ Index ].physicalPage = EntryLo1;
		TLB[ Index ].valid = (EntryLo0) & 0xFF000000;	// Last 8 bits (1st Byte)
		TLB[ Index ].use = (EntryLo0) & 0xFF0000; 	// Second Byte
		TLB[ Index ].dirty = (EntryLo0) & 0xFF00; 	// 3rd Byte
		TLB[ Index ].readOnly = (EntryLo0) & 0xFF;	// 4th Byte	
	}	//		For use by TLBW instruction

void SystemControlCoprocessor :: ReadTLBIndex()
	{
		DEBUG(dbgMachine,red <<"\n[CP0::ReadTLBIndex] Reading TLB entry !!!");
		
	       	EntryHi = TLB[ Index ].virtualPage ; 
		EntryLo1 = TLB[ Index ].physicalPage ;
		EntryLo0 = 0 ;		// Put the bytes in reverse order and shift successively
		EntryLo0 = TLB[ Index ].readOnly ; 
		EntryLo0 = EntryLo0 << 8 ;	// Put MSByte in last byte and shift
		EntryLo0 = EntryLo0 | TLB[ Index ].dirty ; 
		EntryLo0 = EntryLo0 << 8 ; // Put next MSByte
		EntryLo0 = EntryLo0 | TLB[ Index ].use;
	        EntryLo0 = EntryLo0 << 8 ;	// ,,
		EntryLo0 = EntryLo0 | TLB[ Index ].valid ; 	// Put LSByte in last byte
	}
		
void SystemControlCoprocessor :: SwitchMode( Status mode )
{

	DEBUG(dbgMachine,red <<"\n[CP0::SwitchMode] Switching to Mode " << (int)mode);
	Mode = mode ;
	if( mode == KernelMode )
	{
		SetInterruptStatus ( false ) ;
		DEBUG( dbgMachine, red <<"\n[CP0::SwitchMode] Interrupt is disabled by modeswitch") ;
	}
	else
	{
		SetInterruptStatus ( true );
		DEBUG( dbgMachine, green<<"\n[CP0::SwitchMode] Interrupt is enabled by modeswitch") ;
	}
}

void SystemControlCoprocessor :: FlushStage()
{

/*	sem_wait ( cout_mutex );
	cout << red << "\nFlushing first 4 stages in pipeline..." << reset;
	sem_post ( cout_mutex );
*/
	DEBUG(dbgMachine,red <<"\n[CP0::FlushStage] Flushing first 4 stages in pipeline...");
	
	for ( int i=0 ; i<4 ;++i )	// Except Stage4...
		{
//			proc -> flushStage[i] = true;
		proc -> inLatch[i].Initialise();
		//proc -> inLatch[i].finished = true;
		proc -> outLatch[i].Initialise();
		//proc -> outLatch[i].finished = true;	
		}                                       	
}

void SystemControlCoprocessor :: SetCurrentRegisters( u_word_32 epc, u_word_32 bvaddr)
{

	EPC = epc ;	 	// Setting the EPC register to correct value when returning from interrupt handler
	BadVAddr = bvaddr;	// The address which caused an exception - particularly TLB exception
}

//-------------- 09/03/2006 ------------
//	procedure to save the register values for the OS use !!!
// 	written below

#include "globalvars.h"


void SystemControlCoprocessor :: SaveRegisterValues ()
{
/*	int i;	
	for(  i=0; i<32 ;++i)
		RegisterArray[i] = proc -> reg[i];
	
	RegisterArray [i++] = proc -> Hi;
	RegisterArray [i++] = proc -> Lo;
	RegisterArray [i++] = proc -> PCreg;
	RegisterArray [i++] = proc -> NPCreg ;
	RegisterArray [i++] = Index;
	RegisterArray [i++] = EntryHi;
	RegisterArray [i++] = EntryLo1;
	RegisterArray [i++] = EntryLo0;
	RegisterArray [i++] = Context ;
	RegisterArray [i++] = PageMask ;
        RegisterArray [i++] = BadVAddr;
	RegisterArray [i++] = Count ;
	RegisterArray [i++] = Compare ;
	RegisterArray [i++] = Cause ;
	RegisterArray [i++] = EPC;*/

	DEBUG(dbgMachine,red <<"\n[CP0::SaveRegisterValues] Saving register values");
	
	mem -> Write ( (word_32)PC_Addr, proc->PCreg, 4);
	mem -> Write ( (word_32)NPC_Addr, proc->NPCreg, 4);
	mem -> Write ( (word_32)INDEX_Addr, Index, 4);
	mem -> Write ( (word_32)ENTRYHI_Addr,EntryHi , 4);
	mem -> Write ( (word_32)ENTRYLO1_Addr, EntryLo1, 4);
	mem -> Write ( (word_32)ENTRYLO0_Addr, EntryLo0, 4);
	mem -> Write ( (word_32)CONTEXT_Addr, Context, 4);
	mem -> Write ( (word_32)BADVADDR_Addr, BadVAddr, 4);
	mem -> Write ( (word_32)COUNT_Addr, Count, 4);
	mem -> Write ( (word_32)CAUSE_Addr, Cause, 4);
	mem -> Write ( (word_32)EPC_Addr, EPC, 4);
	mem -> Write ( (word_32)AT_Addr, proc->reg[1], 4);
	mem -> Write ( (word_32)V0_Addr, proc->reg[2], 4);
	mem -> Write ( (word_32)V1_Addr, proc->reg[3], 4);
	mem -> Write ( (word_32)A0_Addr, proc->reg[4], 4);
	mem -> Write ( (word_32)A1_Addr, proc->reg[5], 4);
	mem -> Write ( (word_32)A2_Addr, proc->reg[6], 4);
	mem -> Write ( (word_32)A3_Addr, proc->reg[7], 4);
	mem -> Write ( (word_32)SP_Addr, proc->reg[29], 4);
	mem -> Write ( (word_32)RETADDR_Addr, proc->reg[31], 4);
}	

//procedure to restore the register values!!


void SystemControlCoprocessor :: RestoreRegisterValues ()
{
/*	int i = 0 ;
	for( i=0; i<32 ; i++)
	       	proc -> reg[i]= RegisterArray [i++];
	
	proc -> Hi 	= RegisterArray [i++];
	proc -> Lo 	= RegisterArray [i++];
	proc -> PCreg	= RegisterArray [i++];
	proc -> NPCreg 	= RegisterArray [i++];
	Index 		= RegisterArray [i++];
	EntryHi		= RegisterArray [i++];
	EntryLo1	= RegisterArray [i++];
	EntryLo0        = RegisterArray [i++];
	Context 	= RegisterArray [i++];
	PageMask 	= RegisterArray [i++];
        BadVAddr 	= RegisterArray [i++];
	Count 		= RegisterArray [i++];
	Compare 	= RegisterArray [i++];
	Cause 		= RegisterArray [i++]; 
	EPC 		= RegisterArray [i++]; */

	DEBUG(dbgMachine,red <<"\n[CP0::RestoreRegisterValues] Restoring register values");
	
	mem -> Read ( (word_32)PC_Addr,(word_32&)proc->PCreg, 4);
	mem -> Read ( (word_32)NPC_Addr, (word_32&)proc->NPCreg, 4);
	mem -> Read ( (word_32)INDEX_Addr, Index, 4);
	mem -> Read ( (word_32)ENTRYHI_Addr,EntryHi , 4);
	mem -> Read ( (word_32)ENTRYLO1_Addr, EntryLo1, 4);
	mem -> Read ( (word_32)ENTRYLO0_Addr, EntryLo0, 4);
	mem -> Read ( (word_32)CONTEXT_Addr, Context, 4);
	mem -> Read ( (word_32)BADVADDR_Addr, BadVAddr, 4);
	mem -> Read ( (word_32)COUNT_Addr, Count, 4);
	mem -> Read ( (word_32)CAUSE_Addr, Cause, 4);
	mem -> Read ( (word_32)EPC_Addr, EPC, 4);
	mem -> Read ( (word_32)AT_Addr, proc->reg[1], 4);
	mem -> Read ( (word_32)V0_Addr, proc->reg[2], 4);
	mem -> Read ( (word_32)V1_Addr, proc->reg[3], 4);
	mem -> Read ( (word_32)A0_Addr, proc->reg[4], 4);
	mem -> Read ( (word_32)A1_Addr, proc->reg[5], 4);
	mem -> Read ( (word_32)A2_Addr, proc->reg[6], 4);
	mem -> Read ( (word_32)A3_Addr, proc->reg[7], 4);
	mem -> Read ( (word_32)SP_Addr, proc->reg[29], 4);
	mem -> Read ( (word_32)RETADDR_Addr, proc->reg[31], 4);
}

// For the use of TLBP instruction....
// The Index register is loaded with the address(index) of the TLB entry whose
// contents match the contents of the Entry refgister.

void SystemControlCoprocessor :: ProbeTLBEntry()
{
	DEBUG(dbgMachine,red <<"\n[CP0::ProbeTLBEntry] Probing TLB Entry !!");
	
	for( int i=0; i < TLBSIZE; i++ )
	{
		if( (TLB[i].virtualPage == EntryHi) && 	(TLB[i].physicalPage == EntryLo1) )
		{
			Index = i ;
			return;
		}
	}
	return;		// No Entry matches the specified Entry !!! //TODO
	
}

//-------------------------------------------------------------------------//
