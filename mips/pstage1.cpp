/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

 
//#include "processor.h"

#include <pthread.h>
#include <iostream>
using std::cout;
using std::flush;

#include "color.h"
#include <semaphore.h>
extern sem_t * cout_mutex;	// Defined in main.cpp



#include "main.h"		// For CP0	and  StartingAddress



void Processor :: Stage1 ( )
{
	// Copy inLatch into outLatch... NowForth work with outLatch
	LatchCopy ( outLatch[1], inLatch[1] );
	
	// Fisrt check for NOP
	if ( outLatch[1].inst.iV == 0 )
	{
		// Do Nothing 
		outLatch[1].finished = true;
		
/*		sem_wait ( cout_mutex );
		cout << "\n[ Stage1 ] NOP" << flush;
		sem_post ( cout_mutex );
*/
		DEBUG(dbgMachine,"\n[ Stage1 ] NOP");
	}
	else // else perform the switch code
	switch ( outLatch[1].inst.noF.op )
	{
	case OP_ZERO:
		switch ( outLatch[1].inst.rF.funct )
		{
		case FUNCT_ADD:
			outLatch[1].resultStage = RESULT_AT_EX;
			
			if ( RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs )
					&& RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = outLatch[1].inst.rF.rd;
			
/*			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] ADD r" << outLatch[1].inst.rF.rs << " + r"
				<< outLatch[1].inst.rF.rt << " -> r"
				<< outLatch[1].inst.rF.rd << flush;
			sem_post ( cout_mutex );
*/
			DEBUG(dbgMachine,"\n[ Stage1 ] ADD r"<< outLatch[1].inst.rF.rs<< " + r"
					<< outLatch[1].inst.rF.rt << " -> r"<< outLatch[1].inst.rF.rd );
			break;
			
		case FUNCT_AND:
			outLatch[1].resultStage = RESULT_AT_EX;
			
			if ( RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs )
					&& RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = outLatch[1].inst.rF.rd;
			
/*			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] AND r" << outLatch[1].inst.rF.rs << " & r"
				<< outLatch[1].inst.rF.rt << " -> r"
				<< outLatch[1].inst.rF.rd << flush;
			sem_post ( cout_mutex );
*/
			DEBUG(dbgMachine,"\n[ Stage1 ] AND r" << outLatch[1].inst.rF.rs << " & r"
			                                << outLatch[1].inst.rF.rt << " -> r"
		                                << outLatch[1].inst.rF.rd );
			break;
			
		case FUNCT_DIV:
			outLatch[1].resultStage = RESULT_AT_EX;
			
			if ( RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs )
					&& RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = REG_LO;
			outLatch[1].targReg2 = REG_HI;
/*			
			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] DIV r" << outLatch[1].inst.rF.rs << " / r"
				<< outLatch[1].inst.rF.rt << " -> Hi-Lo" << flush;
			sem_post ( cout_mutex );
*/
			DEBUG(dbgMachine,"\n[ Stage1 ] DIV r" << outLatch[1].inst.rF.rs << " / r"
			                                << outLatch[1].inst.rF.rt << " -> Hi-Lo" );
			break;
			
		case FUNCT_MULT:
			outLatch[1].resultStage = RESULT_AT_EX;
			
			if ( RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs )
					&& RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = REG_LO;
			outLatch[1].targReg2 = REG_HI;
			
/*			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] MULT r" << outLatch[1].inst.rF.rs << " * r"
				<< outLatch[1].inst.rF.rt << " -> Hi-Lo" << flush;
			sem_post ( cout_mutex );
*/
			DEBUG(dbgMachine,"\n[ Stage1 ] MULT r" << outLatch[1].inst.rF.rs << " * r"
		                                << outLatch[1].inst.rF.rt << " -> Hi-Lo" );
			break;
			
		case FUNCT_NOR:
			outLatch[1].resultStage = RESULT_AT_EX;
			
			if ( RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs )
					&& RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = outLatch[1].inst.rF.rd;
			
/*			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] NOR r" << outLatch[1].inst.rF.rs << " nor r"
				<< outLatch[1].inst.rF.rt << " -> r"
				<< outLatch[1].inst.rF.rd << flush;
			sem_post ( cout_mutex );
*/
			DEBUG(dbgMachine,"\n[ Stage1 ] NOR r" << outLatch[1].inst.rF.rs << " nor r"
	                                << outLatch[1].inst.rF.rt << " -> r"
                                << outLatch[1].inst.rF.rd );
			break;
			
		case FUNCT_OR:
			outLatch[1].resultStage = RESULT_AT_EX;
			
			if ( RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs )
					&& RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = outLatch[1].inst.rF.rd;
			
/*			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] OR r" << outLatch[1].inst.rF.rs << " or r"
				<< outLatch[1].inst.rF.rt << " -> r"
				<< outLatch[1].inst.rF.rd << flush;
			sem_post ( cout_mutex );
*/

			DEBUG(dbgMachine,"\n[ Stage1 ] OR r" << outLatch[1].inst.rF.rs << " or r"
	                                << outLatch[1].inst.rF.rt << " -> r"
	                                << outLatch[1].inst.rF.rd );
			break;
			
		case FUNCT_SLL:
			outLatch[1].resultStage = RESULT_AT_EX;
			
			outLatch[1].A = outLatch[1].inst.rF.shamt;
			if ( RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt ) == true )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = outLatch[1].inst.rF.rd;
			
/*			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] SLL r" << outLatch[1].inst.rF.rt << " << shamt"
				<< outLatch[1].inst.rF.shamt << " -> r"
				<< outLatch[1].inst.rF.rd << flush;
			sem_post ( cout_mutex );
*/
			DEBUG(dbgMachine,"\n[ Stage1 ] SLL r" << outLatch[1].inst.rF.rt << " << shamt"
	                                << outLatch[1].inst.rF.shamt << " -> r"
	                                << outLatch[1].inst.rF.rd);
			break;
			
		case FUNCT_SLLV:
			outLatch[1].resultStage = RESULT_AT_EX;
			
			if ( RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt )
					&& RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = outLatch[1].inst.rF.rd;
			
/*			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] SLLV r" << outLatch[1].inst.rF.rt << " << r"
				<< outLatch[1].inst.rF.rs << " -> r"
				<< outLatch[1].inst.rF.rd << flush;
			sem_post ( cout_mutex );
*/
			DEBUG(dbgMachine,"\n[ Stage1 ] SLLV r" << outLatch[1].inst.rF.rt << " << r"
	                                << outLatch[1].inst.rF.rs << " -> r"
	                                << outLatch[1].inst.rF.rd);
			break;
			
		case FUNCT_SRA:
			outLatch[1].resultStage = RESULT_AT_EX;
			
			outLatch[1].A = outLatch[1].inst.rF.shamt;
			if ( RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt ) == true )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = outLatch[1].inst.rF.rd;
			
/*			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] SRA r" << outLatch[1].inst.rF.rt << " >> shamt"
				<< outLatch[1].inst.rF.shamt << " -> r"
				<< outLatch[1].inst.rF.rd << flush;
			sem_post ( cout_mutex );
	*/
			DEBUG(dbgMachine,"\n[ Stage1 ] SRA r" << outLatch[1].inst.rF.rt << " >> shamt"
	                                << outLatch[1].inst.rF.shamt << " -> r"
	                                << outLatch[1].inst.rF.rd);
			break;
			
		case FUNCT_SRAV:
			outLatch[1].resultStage = RESULT_AT_EX;
			
			if ( RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt )
					&& RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = outLatch[1].inst.rF.rd;
			
/*			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] SRAV r" << outLatch[1].inst.rF.rt << " >> r"
				<< outLatch[1].inst.rF.rs << " -> r"
				<< outLatch[1].inst.rF.rd << flush;
			sem_post ( cout_mutex );
*/
			DEBUG(dbgMachine,"\n[ Stage1 ] SRAV r" << outLatch[1].inst.rF.rt << " >> r"
	                                << outLatch[1].inst.rF.rs << " -> r"
	                                << outLatch[1].inst.rF.rd );
			break;
			
		case FUNCT_SRL:
			outLatch[1].resultStage = RESULT_AT_EX;
			
			outLatch[1].A = outLatch[1].inst.rF.shamt;
			if ( RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt ) == true )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = outLatch[1].inst.rF.rd;
/*			
			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] SRL r" << outLatch[1].inst.rF.rt << " >> shamt"
				<< outLatch[1].inst.rF.shamt << " -> r"
				<< outLatch[1].inst.rF.rd << flush;
			sem_post ( cout_mutex );
*/
			DEBUG(dbgMachine,"\n[ Stage1 ] SRL r" << outLatch[1].inst.rF.rt << " >> shamt"
	                                << outLatch[1].inst.rF.shamt << " -> r"								                            << outLatch[1].inst.rF.rd);
		break;
			
		case FUNCT_SRLV:
			outLatch[1].resultStage = RESULT_AT_EX;
			
			if ( RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt )
					&& RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = outLatch[1].inst.rF.rd;
/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] SRLV r" << outLatch[1].inst.rF.rt << " >> r"
				<< outLatch[1].inst.rF.rs << " -> r"
				<< outLatch[1].inst.rF.rd << flush;
			sem_post ( cout_mutex );
*/
			DEBUG(dbgMachine,"\n[ Stage1 ] SRLV r" << outLatch[1].inst.rF.rt << " >> r"
	                                << outLatch[1].inst.rF.rs << " -> r"
	                                << outLatch[1].inst.rF.rd );
			break;
			
		case FUNCT_SUB:
			outLatch[1].resultStage = RESULT_AT_EX;
			
			if ( RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs )
					&& RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = outLatch[1].inst.rF.rd;
/*			
			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] SUB r" << outLatch[1].inst.rF.rs << " - r"
				<< outLatch[1].inst.rF.rt << " -> r"
				<< outLatch[1].inst.rF.rd << flush;
			sem_post ( cout_mutex );
	*/
			DEBUG(dbgMachine,"\n[ Stage1 ] SUB r" << outLatch[1].inst.rF.rs << " - r"
	                                << outLatch[1].inst.rF.rt << " -> r"
	                                << outLatch[1].inst.rF.rd);
			break;
			
		case FUNCT_XOR:
			outLatch[1].resultStage = RESULT_AT_EX;
			
			if ( RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs )
					&& RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = outLatch[1].inst.rF.rd;
			
/*			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] XOR r" << outLatch[1].inst.rF.rs << " xor r"
				<< outLatch[1].inst.rF.rt << " -> r"
				<< outLatch[1].inst.rF.rd << flush;
			sem_post ( cout_mutex );*/

			DEBUG(dbgMachine,"\n[ Stage1 ] XOR r" << outLatch[1].inst.rF.rs << " xor r"
	                                << outLatch[1].inst.rF.rt << " -> r"
	                                << outLatch[1].inst.rF.rd);
			break;
			
		case FUNCT_SLT:
			outLatch[1].resultStage = RESULT_AT_EX;
			
			if ( RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs )
					&& RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = outLatch[1].inst.rF.rd;
		/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] SLT r" << outLatch[1].inst.rF.rs << " < r"
				<< outLatch[1].inst.rF.rt << " ? 1 : 0  -> r"
				<< outLatch[1].inst.rF.rd << flush;
			sem_post ( cout_mutex );
		*/
			DEBUG(dbgMachine,"\n[ Stage1 ] SLT r" << outLatch[1].inst.rF.rs << " < r"
                                << outLatch[1].inst.rF.rt << " ? 1 : 0  -> r"
                                << outLatch[1].inst.rF.rd);
			break;
			
		case FUNCT_JR:
			outLatch[1].resultStage = NORESULT;
			
			if ( RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] JR to address in r" 
				<< outLatch[1].inst.rF.rs << flush;
			sem_post ( cout_mutex );
			*/
			DEBUG(dbgMachine,"\n[ Stage1 ] JR to address in r"
	                                << outLatch[1].inst.rF.rs);
			break;
			
		case FUNCT_JALR:
			outLatch[1].resultStage = RESULT_AT_ID;
			
			outLatch[1].IDRes = outLatch[1].PC + 4;
			
			if ( RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = outLatch[1].inst.rF.rd;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] JALR to address in r" 
				<< outLatch[1].inst.rF.rs 
				<< ", return address in r"
				<< outLatch[1].inst.rF.rd << flush;
			sem_post ( cout_mutex );
			*/
			DEBUG(dbgMachine,"\n[ Stage1 ] JALR to address in r"
                                << outLatch[1].inst.rF.rs
	                        << ", return address in r"
	                        << outLatch[1].inst.rF.rd);
			break;
			
		case FUNCT_MFHI:
			outLatch[1].resultStage = RESULT_AT_ID;
			
			if ( RegisterFetch ( IDRES_FT, REG_HI ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = outLatch[1].inst.rF.rd;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] MFHI Hi -> r" 
				<< outLatch[1].inst.rF.rd << flush;
			sem_post ( cout_mutex );
			*/
			DEBUG(dbgMachine,"\n[ Stage1 ] MFHI Hi -> r"
                               << outLatch[1].inst.rF.rd);
			break;
			
		case FUNCT_MFLO:
			outLatch[1].resultStage = RESULT_AT_ID;
			
			if ( RegisterFetch ( IDRES_FT, REG_LO ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = outLatch[1].inst.rF.rd;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] MFLO Lo -> r" 
				<< outLatch[1].inst.rF.rd << flush;
			sem_post ( cout_mutex );
			*/
			DEBUG(dbgMachine,"\n[ Stage1 ] MFLO Lo -> r"
                                << outLatch[1].inst.rF.rd);
			break;
			
		case FUNCT_MTHI:
			outLatch[1].resultStage = RESULT_AT_ID;
			
			if ( RegisterFetch ( IDRES_FT, outLatch[1].inst.rF.rs ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = REG_HI;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] MTHI r" << outLatch[1].inst.rF.rs
				<< " -> Hi" << flush;
			sem_post ( cout_mutex );
			*/
			DEBUG(dbgMachine,"\n[ Stage1 ] MTHI r" << outLatch[1].inst.rF.rs
					                                << " -> Hi" );
			break;
			
		case FUNCT_MTLO:
			outLatch[1].resultStage = RESULT_AT_ID;
			
			if ( RegisterFetch ( IDRES_FT, outLatch[1].inst.rF.rs ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			
			outLatch[1].targReg = REG_LO;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] MTLO r" << outLatch[1].inst.rF.rs
				<< " -> Lo" << flush;
			sem_post ( cout_mutex );
			*/
			DEBUG(dbgMachine,"\n[ Stage1 ] MTLO r" << outLatch[1].inst.rF.rs
					                                << " -> Lo");
			break;
			
		case FUNCT_SYSCALL:
			outLatch[1].resultStage = RESULT_AT_ID;
			
			outLatch[1].IDRes = outLatch[1].PC + 4;
			
			//UpdatePC_Stage1 ( 0, PC_ABSOLUTE );		//-- 24/03/2006 --
			
			outLatch[1].finished = true;
			
			outLatch[1].targReg = 31;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] SYSCALL code in r4 "   
				<< ", return address in r31" << flush;
			sem_post ( cout_mutex );
			*/
			DEBUG(dbgMachine,"\n[ Stage1 ] SYSCALL code in r4 "
                                << ", return address in r31" );
			break;
			
		case FUNCT_RDIN:
			outLatch[1].resultStage = RESULT_AT_MEM;
			
			if ( RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.

			outLatch[1].targReg = outLatch[1].inst.rF.rd;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] RDIN read from device in r"
				<< outLatch[1].inst.rF.rt << " to r"
				<< outLatch[1].inst.rF.rd << flush;
			sem_post ( cout_mutex );
			*/
			DEBUG(dbgMachine,"\n[ Stage1 ] RDIN read from device in r"
                                << outLatch[1].inst.rF.rt << " to r"
                                << outLatch[1].inst.rF.rd);
			break;
		
		case FUNCT_RDOUT:
			outLatch[1].resultStage = NORESULT;
			
			// Even though both fetches are allowed to fail as noFail == true
			// for the RegisterFetch(), max one of them only will fail at any 
			// time as Stage3 produces atmost one result.
			if ( RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt, true ) &&
				RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs, true ) )
				outLatch[1].finished = true;
			else outLatch[1].finished = false;	// Not necessary.
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage1 ] RDOUT write to device in r"
				<< outLatch[1].inst.rF.rt << " from r"
				<< outLatch[1].inst.rF.rs << flush;
			sem_post ( cout_mutex );
			*/
			DEBUG(dbgMachine,"\n[ Stage1 ] RDOUT write to device in r"
                                << outLatch[1].inst.rF.rt << " from r"
                                << outLatch[1].inst.rF.rs);
			break;
			
// -------------------------------------------------------------------------------------------------//
//----------- The extra instructions are written from here -- MODIFIED 13-02-2006 ------------------//
                case FUNCT_TEQ:
                        outLatch[1].resultStage = RESULT_AT_EX;
  			if ( RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs )
                                     && RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt ) )
                                outLatch[1].finished = true;
                        else outLatch[1].finished = false;      // Not necessary.
                        //outLatch[1].targReg = outLatch[1].inst.rF.rd;
			/*
			sem_wait ( cout_mutex );
                        cout << "\n[ Stage1 ] TEQ r" << outLatch[1].inst.rF.rs << " == r"
                        << outLatch[1].inst.rF.rt << " ? 1 : 0 "
                        << outLatch[1].inst.rF.rd << flush;
                        sem_post ( cout_mutex );
			*/
			DEBUG(dbgMachine,"\n[ Stage1 ] TEQ r" << outLatch[1].inst.rF.rs << " == r"
	                        << outLatch[1].inst.rF.rt << " ? 1 : 0 "
	                        << outLatch[1].inst.rF.rd);
                        break;
			
                case FUNCT_TGE:
                        outLatch[1].resultStage = RESULT_AT_EX;
                        if ( RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs )
                                  && RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt ) )
                            outLatch[1].finished = true;
                        else outLatch[1].finished = false;      // Not necessary.
                        //outLatch[1].targReg = outLatch[1].inst.rF.rd;
			/*
                        sem_wait ( cout_mutex );
                        cout << "\n[ Stage1 ] TGE r" << outLatch[1].inst.rF.rs << " >= r"
                        << outLatch[1].inst.rF.rt << " ? 1 : 0 "
                        << outLatch[1].inst.rF.rd << flush;
                        sem_post ( cout_mutex );
			*/
			DEBUG(dbgMachine,"\n[ Stage1 ] TGE r" << outLatch[1].inst.rF.rs << " >= r"
	                        << outLatch[1].inst.rF.rt << " ? 1 : 0 "
	                        << outLatch[1].inst.rF.rd);
                        break;
										
                case FUNCT_TLT:
                        outLatch[1].resultStage = RESULT_AT_EX;
                        if ( RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs )
		                                     && RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt ) )
	                                outLatch[1].finished = true;
                        else outLatch[1].finished = false;      // Not necessary.
                        //outLatch[1].targReg = outLatch[1].inst.rF.rd;
			/*
                        sem_wait ( cout_mutex );
                        cout << "\n[ Stage1 ] TLT r" << outLatch[1].inst.rF.rs << " < r"
	                        << outLatch[1].inst.rF.rt << " ? 1 : 0 "
	                        << outLatch[1].inst.rF.rd << flush;
                        sem_post ( cout_mutex );
			*/
			DEBUG(dbgMachine,"\n[ Stage1 ] TLT r" << outLatch[1].inst.rF.rs << " < r"
                                << outLatch[1].inst.rF.rt << " ? 1 : 0 "
                                << outLatch[1].inst.rF.rd );
                        break;
			
                case FUNCT_TNE:
                        outLatch[1].resultStage = RESULT_AT_EX;
                        if ( RegisterFetch ( ALU_A, outLatch[1].inst.rF.rs )
                                    && RegisterFetch ( ALU_B, outLatch[1].inst.rF.rt ) )
                                outLatch[1].finished = true;
                        else outLatch[1].finished = false;      // Not necessary.
                        //outLatch[1].targReg = outLatch[1].inst.rF.rd;
                        /*
			sem_wait ( cout_mutex );
                        cout << "\n[ Stage1 ] TNE r" << outLatch[1].inst.rF.rs << " != r"
                        << outLatch[1].inst.rF.rt << " ? 1 : 0 "
                        << outLatch[1].inst.rF.rd << flush;
                        sem_post ( cout_mutex );
			*/
			DEBUG(dbgMachine,"\n[ Stage1 ] TNE r" << outLatch[1].inst.rF.rs << " != r"
	                        << outLatch[1].inst.rF.rt << " ? 1 : 0 "
	                        << outLatch[1].inst.rF.rd );
                        break;
		
//--------- THE OP_ZERO INSTRUCTIONS END HERE FOR TRAP-----------------------------------------------//
//--------------------------------------------------------------------------------------------------//
		};
		break;
		
	case OP_ONE:
		switch ( outLatch[1].inst.iF.rt )
		{
		case OP_BGEZ:	// The instruction is BGEZ
			outLatch[1].resultStage = NORESULT;

			outLatch[1].Imm = outLatch[1].inst.iF.imm * 4;
			if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs ) )
			{
				if ( outLatch[1].A >= 0 )
				{
					UpdatePC_Stage1( outLatch[1].Imm, PC_RELATIVE);
					/*
					sem_wait ( cout_mutex );
					cout << "\n[ Stage1 ] BGEZ r" 
						<< outLatch[1].inst.iF.rs 
						<< " jumped to relative address " 
						<< outLatch[1].Imm << flush;
					sem_post ( cout_mutex );
					*/
					DEBUG(dbgMachine,"\n[ Stage1 ] BGEZ r"
                                            << outLatch[1].inst.iF.rs
                                            << " jumped to relative address "
                                            << outLatch[1].Imm );
				}
				else
				{/*
					sem_wait ( cout_mutex );
					cout << "\n[ Stage1 ] BGEZ r" 
						<< outLatch[1].inst.iF.rs 
						<< " did not jump to relative address " 
						<< outLatch[1].Imm << flush;
					sem_post ( cout_mutex );*/
					DEBUG(dbgMachine,"\n[ Stage1 ] BGEZ r"
                                              << outLatch[1].inst.iF.rs
                                              << " did not jump to relative address "
                                              << outLatch[1].Imm );
				}

				outLatch[1].finished = true;

			}
			else
				outLatch[1].finished = false;	// Not necessary.
			break;
			
		case OP_BLTZ:	// The instruction is BLTZ
			outLatch[1].resultStage = NORESULT;

			outLatch[1].Imm = outLatch[1].inst.iF.imm * 4;
			if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs ) )
			{
				if ( outLatch[1].A < 0 )
				{
					UpdatePC_Stage1( outLatch[1].Imm, PC_RELATIVE);
					/*
					sem_wait ( cout_mutex );
					cout << "\n[ Stage1 ] BLTZ r" 
						<< outLatch[1].inst.iF.rs 
						<< " jumped to relative address " 
						<< outLatch[1].Imm << flush;
					sem_post ( cout_mutex );*/
					DEBUG(dbgMachine,"\n[ Stage1 ] BLTZ r"
                                              << outLatch[1].inst.iF.rs
                                              << " jumped to relative address "
                                              << outLatch[1].Imm );
				}
				else
				{/*
					sem_wait ( cout_mutex );
					cout << "\n[ Stage1 ] BLTZ r" 
						<< outLatch[1].inst.iF.rs 
						<< " did not jump to relative address " 
						<< outLatch[1].Imm << flush;
					sem_post ( cout_mutex );*/
					DEBUG(dbgMachine,"\n[ Stage1 ] BLTZ r"
                                               << outLatch[1].inst.iF.rs
                                               << " did not jump to relative address "
                                               << outLatch[1].Imm );
				}

				outLatch[1].finished = true;

			}
			else
				outLatch[1].finished = false;	// Not necessary.
			break;
//----------------------------------------------------------------------------------------------------//
//------------------------ The op_one instructions are written here onwards . TRAPS 13-02-2006 -------//

	        case OP_TEQI:
	                outLatch[1].resultStage = RESULT_AT_EX;
	                outLatch[1].Imm = outLatch[1].inst.iF.imm;
	                if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs ) )
	                outLatch[1].finished = true;
	                else outLatch[1].finished = false;      // Not necessary.
 	                //outLatch[1].targReg = outLatch[1].inst.iF.rt;
			/*
	                sem_wait ( cout_mutex );
	                cout << "\n[ Stage1 ] TEQI r" << outLatch[1].inst.iF.rs << " == imm "
	                     << outLatch[1].inst.iF.imm << " -> r"
	                     << outLatch[1].inst.iF.rt << flush;
	                sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage1 ] TEQI r" << outLatch[1].inst.iF.rs << " == imm "
                             << outLatch[1].inst.iF.imm << " -> r"
                             << outLatch[1].inst.iF.rt );
	                break;
								
	        case OP_TGEI:
        	        outLatch[1].resultStage = RESULT_AT_EX;

                	outLatch[1].Imm = outLatch[1].inst.iF.imm;
	                if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs ) )
		                        outLatch[1].finished = true;
	                else outLatch[1].finished = false;      // Not necessary.
	
	                //outLatch[1].targReg = outLatch[1].inst.iF.rt;
			/*
	                sem_wait ( cout_mutex );
	                cout << "\n[ Stage1 ] TGEI r" << outLatch[1].inst.iF.rs << " >= imm "
	                        << outLatch[1].inst.iF.imm << " -> r"
	                        << outLatch[1].inst.iF.rt << flush;
	                sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage1 ] TGEI r" << outLatch[1].inst.iF.rs << " >= imm "
                                << outLatch[1].inst.iF.imm << " -> r"
                                << outLatch[1].inst.iF.rt );
	                break;

	        case OP_TLTI:
	                outLatch[1].resultStage = RESULT_AT_EX;
	                outLatch[1].Imm = outLatch[1].inst.iF.imm;
	                if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs ) )
                        outLatch[1].finished = true;
	                else outLatch[1].finished = false;      // Not necessary.
	                //outLatch[1].targReg = outLatch[1].inst.iF.rt;
			/*
	                sem_wait ( cout_mutex );
	                cout << "\n[ Stage1 ] TLTI r" << outLatch[1].inst.iF.rs << " < imm "
                        << outLatch[1].inst.iF.imm << " -> r"
                        << outLatch[1].inst.iF.rt << flush;
	                sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage1 ] TLTI r" << outLatch[1].inst.iF.rs << " < imm "
	                        << outLatch[1].inst.iF.imm << " -> r"
	                        << outLatch[1].inst.iF.rt );
	                break;
			
	        case OP_TNEI:
        	        outLatch[1].resultStage = RESULT_AT_EX;
                	outLatch[1].Imm = outLatch[1].inst.iF.imm;
	                if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs ) )
	                        outLatch[1].finished = true;
        	        else outLatch[1].finished = false;      // Not necessary.
	                //outLatch[1].targReg = outLatch[1].inst.iF.rt;
			/*
	                sem_wait ( cout_mutex );
        	        cout << "\n[ Stage1 ] TNEI r" << outLatch[1].inst.iF.rs << " != imm "
	        	                << outLatch[1].inst.iF.imm << " -> r"
		                        << outLatch[1].inst.iF.rt << flush;
	                sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage1 ] TNEI r" << outLatch[1].inst.iF.rs << " != imm "
                                      << outLatch[1].inst.iF.imm << " -> r"
                                      << outLatch[1].inst.iF.rt);
        	        break;
			
//----------------------------------------------------------------------------------------------------//

			
		};
		break;
		
	case OP_ADDI:
		outLatch[1].resultStage = RESULT_AT_EX;

		outLatch[1].Imm = outLatch[1].inst.iF.imm;
		if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs ) )
			outLatch[1].finished = true;
		else outLatch[1].finished = false;	// Not necessary.

		outLatch[1].targReg = outLatch[1].inst.iF.rt;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage1 ] ADDI r" << outLatch[1].inst.iF.rs << " + imm "
			<< outLatch[1].inst.iF.imm << " -> r"
			<< outLatch[1].inst.iF.rt << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1 ] ADDI r" << outLatch[1].inst.iF.rs << " + imm "
	                        << outLatch[1].inst.iF.imm << " -> r"
	                        << outLatch[1].inst.iF.rt);
		break;
		
	case OP_ANDI:
		outLatch[1].resultStage = RESULT_AT_EX;

		outLatch[1].Imm = outLatch[1].inst.iF.imm;
		if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs ) )
			outLatch[1].finished = true;
		else outLatch[1].finished = false;	// Not necessary.

		outLatch[1].targReg = outLatch[1].inst.iF.rt;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage1 ] ANDI r" << outLatch[1].inst.iF.rs << " & imm "
			<< outLatch[1].inst.iF.imm << " -> r"
			<< outLatch[1].inst.iF.rt << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1 ] ANDI r" << outLatch[1].inst.iF.rs << " & imm "
                        << outLatch[1].inst.iF.imm << " -> r"
                        << outLatch[1].inst.iF.rt);
		break;
		
	case OP_ORI:
		outLatch[1].resultStage = RESULT_AT_EX;

		outLatch[1].Imm = outLatch[1].inst.iF.imm;
		if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs ) )
			outLatch[1].finished = true;
		else outLatch[1].finished = false;	// Not necessary.

		outLatch[1].targReg = outLatch[1].inst.iF.rt;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage1 ] ORI r" << outLatch[1].inst.iF.rs << " | imm "
			<< outLatch[1].inst.iF.imm << " -> r"
			<< outLatch[1].inst.iF.rt << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1 ] ORI r" << outLatch[1].inst.iF.rs << " | imm "
	                        << outLatch[1].inst.iF.imm << " -> r"
	                        << outLatch[1].inst.iF.rt);
		break;
		
	case OP_XORI:
		outLatch[1].resultStage = RESULT_AT_EX;

		outLatch[1].Imm = outLatch[1].inst.iF.imm;
		if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs ) )
			outLatch[1].finished = true;
		else outLatch[1].finished = false;	// Not necessary.

		outLatch[1].targReg = outLatch[1].inst.iF.rt;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage1 ] XORI r" << outLatch[1].inst.iF.rs << " xor imm "
			<< outLatch[1].inst.iF.imm << " -> r"
			<< outLatch[1].inst.iF.rt << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1 ] XORI r" << outLatch[1].inst.iF.rs << " xor imm "
	                        << outLatch[1].inst.iF.imm << " -> r"
	                        << outLatch[1].inst.iF.rt );
		break;
		
	case OP_LUI:
		outLatch[1].resultStage = RESULT_AT_EX;
		
		outLatch[1].Imm = outLatch[1].inst.iF.imm << 16;
		if ( RegisterFetch ( ALU_B, outLatch[1].inst.iF.rt ) )
			outLatch[1].finished = true;
		else outLatch[1].finished = false;	// Not necessary.
		
		outLatch[1].targReg = outLatch[1].inst.iF.rt;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage1 ] LUI imm" << outLatch[1].inst.iF.imm
			<< " -> upper_16( r" << outLatch[1].inst.iF.rt
			<< " ) " << flush;
		sem_post ( cout_mutex );
		*/
		DEBUG(dbgMachine,"\n[ Stage1 ] LUI imm" << outLatch[1].inst.iF.imm
	                        << " -> upper_16( r" << outLatch[1].inst.iF.rt
	                        << " ) "	);
		break;
	
	case OP_SLTI:
		outLatch[1].resultStage = RESULT_AT_EX;

		outLatch[1].Imm = outLatch[1].inst.iF.imm;
		if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs ) )
			outLatch[1].finished = true;
		else outLatch[1].finished = false;	// Not necessary.

		outLatch[1].targReg = outLatch[1].inst.iF.rt;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage1 ] SLTI r" << outLatch[1].inst.iF.rs << " < imm "
			<< outLatch[1].inst.iF.imm << " ? 1 : 0  -> r"
			<< outLatch[1].inst.iF.rt << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1 ] SLTI r" << outLatch[1].inst.iF.rs << " < imm "
	                        << outLatch[1].inst.iF.imm << " ? 1 : 0  -> r"
	                        << outLatch[1].inst.iF.rt);
		break;
	
	case OP_BEQ:
		outLatch[1].resultStage = NORESULT;

		outLatch[1].Imm = outLatch[1].inst.iF.imm * 4 ;
		if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs )
				&& RegisterFetch ( ALU_B, outLatch[1].inst.iF.rt ) )
			outLatch[1].finished = true;
		else outLatch[1].finished = false;	// Not necessary.
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage1 ] BEQ r" << outLatch[1].inst.iF.rs << ", r"
			<< outLatch[1].inst.iF.rt << " to relative address "
			<< outLatch[1].Imm << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1 ] BEQ r" << outLatch[1].inst.iF.rs << ", r"
                        << outLatch[1].inst.iF.rt << " to relative address "
                        << outLatch[1].Imm );
		break;
	
	case OP_BGTZ:
		outLatch[1].resultStage = NORESULT;

		outLatch[1].Imm = outLatch[1].inst.iF.imm * 4;
		if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs ) )
		{
			if ( outLatch[1].A > 0 )
			{
				UpdatePC_Stage1( outLatch[1].Imm, PC_RELATIVE );
				/*
				sem_wait ( cout_mutex );
				cout << "\n[ Stage1 ] BGTZ r" << outLatch[1].inst.iF.rs 
					<< " jumped to relative address " 
					<< outLatch[1].Imm << flush;
				sem_post ( cout_mutex );*/
				DEBUG(dbgMachine,"\n[ Stage1 ] BGTZ r" << outLatch[1].inst.iF.rs
	                                        << " jumped to relative address "
	                                        << outLatch[1].Imm );
			}
			else
			{/*
				sem_wait ( cout_mutex );
				cout << "\n[ Stage1 ] BGTZ r" << outLatch[1].inst.iF.rs 
					<< " did not jump to relative address " 
					<< outLatch[1].Imm << flush;
				sem_post ( cout_mutex );*/
				DEBUG(dbgMachine,"\n[ Stage1 ] BGTZ r" << outLatch[1].inst.iF.rs
                                        << " did not jump to relative address "
                                        << outLatch[1].Imm );
			}
		
			outLatch[1].finished = true;

		}
		else
			outLatch[1].finished = false;	// Not necessary.
		break;
	
	case OP_BLEZ:
		outLatch[1].resultStage = NORESULT;

		outLatch[1].Imm = outLatch[1].inst.iF.imm * 4;
		if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs ) )
		{
			if ( outLatch[1].A <= 0 )
			{
				UpdatePC_Stage1( outLatch[1].Imm, PC_RELATIVE );
				/*
				sem_wait ( cout_mutex );
				cout << "\n[ Stage1 ] BLEZ r" << outLatch[1].inst.iF.rs 
					<< " jumped to relative address " 
					<< outLatch[1].Imm << flush;
				sem_post ( cout_mutex );*/
				DEBUG(dbgMachine,"\n[ Stage1 ] BLEZ r" << outLatch[1].inst.iF.rs
	                                        << " jumped to relative address "
	                                        << outLatch[1].Imm);
			}
			else
			{	/*
				sem_wait ( cout_mutex );
				cout << "\n[ Stage1 ] BLEZ r" << outLatch[1].inst.iF.rs 
					<< " did not jump to relative address " 
					<< outLatch[1].Imm << flush;
				sem_post ( cout_mutex );*/
				DEBUG(dbgMachine,"\n[ Stage1 ] BLEZ r" << outLatch[1].inst.iF.rs
                                        << " did not jump to relative address "
                                        << outLatch[1].Imm);
			}
		
			outLatch[1].finished = true;

		}
		else
			outLatch[1].finished = false;	// Not necessary.
		break;
	
	case OP_BNE:
		outLatch[1].resultStage = NORESULT;

		//outLatch[1].Imm = outLatch[1].inst.iF.imm * 4;
		outLatch[1].Imm = outLatch[1].inst.iF.imm * 4 ;      //- 21/02/06 - Relocatable
		if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs )
				&& RegisterFetch ( ALU_B, outLatch[1].inst.iF.rt ) )
			outLatch[1].finished = true;
		else outLatch[1].finished = false;	// Not necessary.
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage1 ] BNE r" << outLatch[1].inst.iF.rs << ", r"
			<< outLatch[1].inst.iF.rt << " to relative address "
			<< outLatch[1].Imm << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1 ] BNE r" << outLatch[1].inst.iF.rs << ", r"
				<< outLatch[1].inst.iF.rt << " to relative address "
				<< outLatch[1].Imm );
		break;
	
	case OP_J:
		outLatch[1].resultStage = NORESULT;
		
		//outLatch[1].Imm = outLatch[1].inst.jF.tAddr * 4;
		outLatch[1].Imm = outLatch[1].inst.jF.tAddr * 4 + StartingAddress;      //- 21/02/06 - Relocatable
		
		//cout<<"\n Address "<< outLatch[1].Imm << endl;
		UpdatePC_Stage1( outLatch[1].Imm, PC_ABSOLUTE );
		
		outLatch[1].finished = true;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage1 ] J (jump) to absolute address "
			<< outLatch[1].Imm << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1 ] J (jump) to absolute address "
				<< outLatch[1].Imm );
		break;
	
	case OP_JAL:
		outLatch[1].resultStage = RESULT_AT_ID;
		
		//outLatch[1].Imm = outLatch[1].inst.jF.tAddr * 4;
		outLatch[1].Imm = outLatch[1].inst.jF.tAddr * 4 + StartingAddress;	//- 21/02/06 - Relocatable
		
		UpdatePC_Stage1( outLatch[1].Imm, PC_ABSOLUTE );
		
		outLatch[1].targReg = 31;
		outLatch[1].IDRes = outLatch[1].PC + 4;
		
		outLatch[1].finished = true;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage1 ] JAL (jump) to absolute address "
			<< outLatch[1].Imm << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1 ] JAL (jump) to absolute address "
				<< outLatch[1].Imm);
		break;
		
	case OP_LW:
		outLatch[1].resultStage = RESULT_AT_MEM;

		outLatch[1].Imm = outLatch[1].inst.iF.imm + StartingAddress; //-----23/03/2006 
		if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs ) )
			outLatch[1].finished = true;
		else outLatch[1].finished = false;	// Not necessary.

		outLatch[1].targReg = outLatch[1].inst.iF.rt;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage1 ] LW Memory[" << outLatch[1].inst.iF.imm
			<< " + (r"<< outLatch[1].inst.iF.rs << ")] -> r"
			<< outLatch[1].inst.iF.rt << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1 ] LW Memory[" << outLatch[1].inst.iF.imm
				<< " + (r"<< outLatch[1].inst.iF.rs << ")] -> r"
				<< outLatch[1].inst.iF.rt);
		break;
	
	case OP_SW:
		outLatch[1].resultStage = NORESULT;
		
		// Note that here rt contains the source and rs the destination
		// address to confirm to the same format as LW
		outLatch[1].Imm = outLatch[1].inst.iF.imm + StartingAddress ; //--------23/03/2006
		
		// The Register fetch for rt may fail because we need the data only in 
		// Stage3, and so the data need only be forwarded in Stage2
		if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs ) &&
			RegisterFetch ( ALU_B, outLatch[1].inst.iF.rt, true ) )
			outLatch[1].finished = true;
		else outLatch[1].finished = false;	// Not necessary.
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage1 ] SW Memory[" << outLatch[1].inst.iF.imm
			<< " + (r"<< outLatch[1].inst.iF.rs << ")] <- r"
			<< outLatch[1].inst.iF.rt << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1 ] SW Memory[" << outLatch[1].inst.iF.imm
				<< " + (r"<< outLatch[1].inst.iF.rs << ")] <- r"
				<< outLatch[1].inst.iF.rt);
		break;
		
	case OP_DIN:
		outLatch[1].resultStage = RESULT_AT_MEM;
		
		outLatch[1].Imm = outLatch[1].inst.iF.imm;
		
		outLatch[1].finished = true;
		
		outLatch[1].targReg = outLatch[1].inst.iF.rt;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage1 ] DIN read from device " 
			<< outLatch[1].inst.iF.imm << " to r"
			<< outLatch[1].inst.iF.rt << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1 ] DIN read from device "
				<< outLatch[1].inst.iF.imm << " to r"
				<< outLatch[1].inst.iF.rt );
		break;
		
	case OP_DOUT:
		outLatch[1].resultStage = NORESULT;
		
		outLatch[1].Imm = outLatch[1].inst.iF.imm;
		if ( RegisterFetch ( ALU_A, outLatch[1].inst.iF.rs, true ) )
			outLatch[1].finished = true;
		else outLatch[1].finished = false;	// Not necessary.
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage1 ] DOUT write to device "
			<< outLatch[1].inst.iF.imm << " contents of r"
			<< outLatch[1].inst.iF.rs << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1 ] DOUT write to device "
				<< outLatch[1].inst.iF.imm << " contents of r"
				<< outLatch[1].inst.iF.rs);
		break;
		
//--------------------------------------------------------------------------------------------//
// -------------- The Coprocessor operations are decoded here -- modified 13-02-2006 --------//
	case OP_COP0:
		{

		 if( CP0 -> Mode != KernelMode )
		 {
			 DEBUG( dbgMachine, red << "\n[ Stage1 ] Privileged Instruction in user mode!!" );
			 outLatch[1].IsException = true ;
			 outLatch[1].finished = true;
			 //break;
		   //Terminate();		// Have to SignalException( PRIV_INSTR ) instead !!
		 }
		switch(outLatch[1].inst.rF.funct)
			{
			case FUNCT_ERET:
				outLatch[1].resultStage = NORESULT;
				outLatch[1].finished = true;
				//StartingAddress = SYSTEM_START_ADDRESS;	// -- 24/03/2006--
									// Setting the User Program's StartingAddress back
				/*
				sem_wait(cout_mutex);
				cout << "\n [ Stage 1 ] ERET : setting StartingAddress to 1024"
					<< flush ;
				sem_post(cout_mutex);*/
				DEBUG(dbgMachine,"\n [ Stage 1 ] ERET : setting StartingAddress to 1024");
				break;

                        case FUNCT_TLBP:
                                outLatch[1].resultStage = NORESULT;
                                outLatch[1].finished = true;
				/*
                                sem_wait(cout_mutex);
                                cout << "\n [ Stage 1 ] TLBP "<< flush ;
                                sem_post(cout_mutex);*/
				DEBUG(dbgMachine,"\n [ Stage 1 ] TLBP ");
                                break;	
                        case FUNCT_TLBR:
                                outLatch[1].resultStage = NORESULT;
                                outLatch[1].finished = true;
				/*
                                sem_wait(cout_mutex);
                                cout << "\n [ Stage 1 ] TLBR "<< flush ;
                                sem_post(cout_mutex);*/
				DEBUG(dbgMachine,"\n [ Stage 1 ] TLBR ");
                                break;
                        case FUNCT_TLBW:
                                outLatch[1].resultStage = NORESULT;
                                outLatch[1].finished = true;
				/*
                                sem_wait(cout_mutex);
                                cout << "\n [ Stage 1 ] TLBW "<< flush ;
                                sem_post(cout_mutex);*/
				DEBUG(dbgMachine,"\n [ Stage 1 ] TLBW ");
                                break;

		//------------ Instructions to move from and to coprocessor register----------//
		//---------------Added on 21/03/2006-----------------------------------------
						
			case FUNCT_MFC0:
				{
	                        outLatch[1].resultStage = RESULT_AT_ID;
				word_32 sreg = outLatch[1].inst.rF.rd;
				if( outLatch[1].inst.rF.rs == 1 )	// We need to add 32
					sreg += 32;
	                        if ( RegisterFetch ( IDRES_FT, sreg ) )
	                                outLatch[1].finished = true;
	                        else outLatch[1].finished = false;      // Not necessary.
		                        outLatch[1].targReg = outLatch[1].inst.rF.rt;
				/*	
	                        sem_wait ( cout_mutex );
	                        cout << "\n[ Stage1 ] MFC0 r" << outLatch[1].inst.rF.rt
					<< " <- "  << sreg << flush;
	                        sem_post ( cout_mutex );*/
				DEBUG(dbgMachine,"\n[ Stage1 ] MFC0 r" << outLatch[1].inst.rF.rt
					<< " <- "  << sreg );	
				}
	                        break;

			case FUNCT_MTC0:
				{
                                outLatch[1].resultStage = RESULT_AT_ID;
                                word_32 treg = outLatch[1].inst.rF.rd;
                                if( outLatch[1].inst.rF.rs == 1 )       // We need to add 32
                                        treg += 32;
                                if ( RegisterFetch ( IDRES_FT, outLatch[1].inst.rF.rt ) )
                                        outLatch[1].finished = true;
                                else outLatch[1].finished = false;      // Not necessary.
                                outLatch[1].targReg = treg;
				/*
                                sem_wait ( cout_mutex );
                                cout << "\n[ Stage1 ] MTC0 r" << outLatch[1].inst.rF.rt
                                        << " -> "  << treg << flush;
                                sem_post ( cout_mutex );*/
				DEBUG(dbgMachine,"\n[ Stage1 ] MTC0 r" << outLatch[1].inst.rF.rt
						<< " -> "  << treg);
				}
                                break;
															
			}
		break;
		}


//-------------------------------------------------------------------------------------------//
	};
}

bool Processor :: RegisterFetch ( RegisterFetchTarget target, int regNumber, bool noFail )
{
	word_32 fetchResult;
	if ( regNumber == 0 )
	{/*
		sem_wait ( cout_mutex );
		cout << violet << "\n[ Stage1:RegisterFetch ] Register $zero is always 0" 
			<< reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1:RegisterFetch ] Register $zero is always 0");
		fetchResult = 0;
	}
	else if ( inLatch[2].targReg == regNumber && inLatch[2].resultStage == RESULT_AT_ID)
	{/*
		sem_wait ( cout_mutex );
		cout << violet << "\n[ Stage1:RegisterFetch ] Result from current EX-IDRes" 
			<< reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1:RegisterFetch ] Result from current EX-IDRes");
		fetchResult = inLatch[2].IDRes;
	}
	else if ( ( inLatch[2].targReg == regNumber || inLatch[2].targReg2 == regNumber )
			&& inLatch[2].resultStage == RESULT_AT_MEM )
	{/*
		sem_wait ( cout_mutex );
		cout << violet << "\n[ Stage1:RegisterFetch ] Result unavailable" 
			<< reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1:RegisterFetch ] Result unavailable");
		if ( noFail == false )
			return false;	// Will only get result in next clock
		else 
		{/*
			sem_wait ( cout_mutex );
			cout << violet << "\n[ Stage1:RegisterFetch ] " 
				<< "will forward while in Stage2"
				<< reset << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage1:RegisterFetch ] "
					<< "will forward while in Stage2");
			outLatch[1].dataFetchIncomplete = true;
			outLatch[1].FetchFailedFor = target;
			return true;
		}
	}
	else if ( ( inLatch[2].targReg == regNumber || inLatch[2].targReg2 == regNumber )
			&& inLatch[2].resultStage == RESULT_AT_EX )
	{
		// Have to wait till result has been computed
		while ( outLatch[2].finished == false )
			sched_yield ( );	
				// Relinquish processor instead of busyWaiting.
				
		if ( regNumber == inLatch[2].targReg )
		{/*
			sem_wait ( cout_mutex );
			cout << violet << "\n[ Stage1:RegisterFetch ]"
				<< " Result from current EX - ALUOutput" 
				<< reset << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage1:RegisterFetch ]"
					<< " Result from current EX - ALUOutput");
			fetchResult = outLatch[2].ALUOutput;
		}
		else 
		{/*
			sem_wait ( cout_mutex );
			cout << violet << "\n[ Stage1:RegisterFetch ]"
				<< " Result from current EX - ALUOutputHi"
				<< reset << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage1:RegisterFetch ]"
					<< " Result from current EX - ALUOutputHi");
			fetchResult = outLatch[2].ALUOutputHi;
		}
	}
	else if ( inLatch[3].targReg == regNumber 
			&& inLatch[3].resultStage == RESULT_AT_ID )
	{/*
		sem_wait ( cout_mutex );
		cout << violet << "\n[ Stage1:RegisterFetch ]"
			<< " Result from current MEM - IDRes" 
			<< reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1:RegisterFetch ]"
				<< " Result from current MEM - IDRes");
		fetchResult = inLatch[3].IDRes;
	}
	else if ( ( inLatch[3].targReg == regNumber || inLatch[3].targReg2 == regNumber )
			&& inLatch[3].resultStage == RESULT_AT_EX )
	{
		// result has already been computed in the previous clock
		if ( regNumber == inLatch[3].targReg )
		{/*
			sem_wait ( cout_mutex );
			cout << violet << "\n[ Stage1:RegisterFetch ]"
				<< " Result from current MEM - ALUOutput"
				<< reset << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage1:RegisterFetch ]"
					<< " Result from current MEM - ALUOutput");
			fetchResult = inLatch[3].ALUOutput;
		}
		else
		{/*
			sem_wait ( cout_mutex );
			cout << violet << "\n[ Stage1:RegisterFetch ]"
				<< " Result from current MEM - ALUOutputHi" 
				<< reset << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage1:RegisterFetch ]"
					<< " Result from current MEM - ALUOutputHi");
			fetchResult = inLatch[3].ALUOutputHi;
		}
	}
	else if ( inLatch[3].targReg == regNumber 
			&& inLatch[3].resultStage == RESULT_AT_MEM )
	{
		// Have to wait till result has been computed
		while ( outLatch[3].finished == false )
			sched_yield ( );	
				// Relinquish processor instead of busywaiting.
		/*
		sem_wait ( cout_mutex );
		cout << violet << "\n[ Stage1:RegisterFetch ]"
			<< " Result from current MEM - LMD" 
			<< reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1:RegisterFetch ]"
				<< " Result from current MEM - LMD");
		fetchResult = outLatch[3].LMD;
	}
	else	// first wait for write register stage to complete, then read the register
	{
		while ( outLatch[4].finished == false )
			sched_yield ( );	
				// Relinquish processor instead of busywaiting.
		/*
		sem_wait ( cout_mutex );
		cout << violet << "\n[ Stage1:RegisterFetch ] Result from Register files" 
			<< reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1:RegisterFetch ] Result from Register files");
		
		switch ( regNumber )
		{
		case REG_LO:
			fetchResult = Lo;
			break;
		case REG_HI:
			fetchResult = Hi;
			break;
		//------------------------added on 20/03/2006--------------------------//
			
                case REG_PC:
                       fetchResult = PCreg;
                         break;
                case REG_NPC:
                       fetchResult = NPCreg;
                        break;
                case REG_INDEX:
                       fetchResult = CP0->Index;
                       break;
		case REG_ENTRYHI:
		       fetchResult = CP0->EntryHi;
		       break;
		case REG_ENTRYLO1:
		       fetchResult = CP0->EntryLo1;
		       break;
		case REG_ENTRYLO0:
		       fetchResult = CP0->EntryLo0;
		       break;
                case REG_CONTEXT:
                       fetchResult = CP0->Context;
                        break;
                case REG_PAGEMASK:
                       fetchResult = CP0->PageMask;
                       break;
                case REG_BADVADDR:
                       fetchResult = CP0->BadVAddr;
                        break;
                case REG_COUNT:
                        fetchResult = CP0->Count;
                        break;
                case REG_COMPARE:
                        fetchResult = CP0->Compare;
                        break;
                case REG_CAUSE:
                        fetchResult = CP0->Cause;
                        break;
                case REG_EPC:
                        fetchResult = CP0->EPC;
                        break;
		//--- Added on 29/01/2008 by Rajeesh ---//
		case REG_SR:
			fetchResult = CP0->StatusReg;
			break;
		//--------------------------------------//
			
			
		//-------------------------------------------------------------------//			
		default:
			fetchResult = reg[regNumber];
			break;
		};
	}
	
	switch ( target )
	{
	case ALU_A:
		outLatch[1].A = fetchResult;
		/*
		sem_wait ( cout_mutex );
		cout << violet << "\n[ Stage1:RegisterFetch ] A = " << outLatch[1].A 
			<< reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1:RegisterFetch ] A = " << outLatch[1].A);
		break;
	case ALU_B:
		outLatch[1].B = fetchResult;
		/*
		sem_wait ( cout_mutex );
		cout << violet << "\n[ Stage1:RegisterFetch ] B = " << outLatch[1].B 
			<< reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage1:RegisterFetch ] B = " << outLatch[1].B);
		break;
	case IDRES_FT:
		outLatch[1].IDRes = fetchResult;
		/*
		sem_wait ( cout_mutex );
		cout << violet << "\n[ Stage1:RegisterFetch ] IDRes = "
			<< outLatch[1].IDRes << reset << flush;
		sem_post ( cout_mutex ); */
		DEBUG(dbgMachine,"\n[ Stage1:RegisterFetch ] IDRes = "<< outLatch[1].IDRes );
		break;
	};
	return true;
}

void Processor :: UpdatePC_Stage1 ( word_32 value, PCUpdateType updateType )
{

	
	switch ( updateType )
	{
	case PC_ABSOLUTE:
		
		if ( PCreg == static_cast<u_word_32>(value) )
		{/*
			sem_wait ( cout_mutex );
			cout << skyblue << "\n[ Stage1:UpdatePC ]"
				<< " instruction already in IF stage"
				<< reset << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage1:UpdatePC ]"
					<< " instruction already in IF stage");
			return;
		}
		else
		{
			while ( outLatch[2].finished == false )
				sched_yield ( );
			sem_wait ( pc_mutex );
			if ( flushStage[1] == true )
			{/*
				sem_wait ( cout_mutex );
				cout << skyblue << "\n[ Stage1:UpdatePC ]"
					<< " this stage was flushed by"
					<< " stage2, disallowing execution" 
					<< reset << flush;
				sem_post ( cout_mutex );*/
				DEBUG(dbgMachine,"\n[ Stage1:UpdatePC ]"
						<< " this stage was flushed by"
						<< " stage2, disallowing execution");
				
				sem_post ( pc_mutex );
				return;
			}
			sem_post ( pc_mutex );
			flushStage[0] = true;
			PC_update_control ( value, 1 );
			/*
			sem_wait ( cout_mutex );
			cout << skyblue << "\n[ Stage1:UpdatePC ]"
				<< " updated NPC with absolute address"
				<< reset << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage1:UpdatePC ]"
					<< " updated NPC with absolute address");
		}
		break;
	case PC_RELATIVE:
		value = value + PCreg;
		if ( PCreg == static_cast<u_word_32>(value) )
		{/*
			sem_wait ( cout_mutex );
			cout << skyblue << "\n[ Stage1:UpdatePC ]"
				<< " instruction already in IF stage"
				<< reset << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage1:UpdatePC ]"
					<< " instruction already in IF stage");
			return;
		}
		else
		{
			while ( outLatch[2].finished == false )
				sched_yield ( );
			sem_wait ( pc_mutex );
			if ( flushStage[1] == true )
			{/*
				sem_wait ( cout_mutex );
				cout << skyblue << "\n[ Stage1:UpdatePC ]"
					<< " this stage was flushed by"
					<< " stage2, disallowing execution" 
					<< reset << flush;
				sem_post ( cout_mutex );
			*/
			DEBUG(dbgMachine,"\n[ Stage1:UpdatePC ]"
				<< " this stage was flushed by"
				<< " stage2, disallowing execution");	
				sem_post ( pc_mutex );
				return;
			}
			sem_post ( pc_mutex );
			flushStage[0] = true;
			PC_update_control ( value, 1 );
			/*
			sem_wait ( cout_mutex );
			cout << skyblue << "\n[ Stage1:UpdatePC ]"
				<< " updated NPC with relative address"
				<< reset << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage1:UpdatePC ]"
					<< " updated NPC with relative address");
		}
		break;
	};
}
