/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

 
#include "latch.h"

void Latch :: Initialise ( )
{
	PC = 0;
	inst.iV = 0;
	
	targReg = -1;
	targReg2 = -1;
	
	IsException = false ; // 25/03/06 the isexception boolean is intialy false as there is no exception
	
	resultStage = NORESULT;
	
	dataFetchIncomplete = false;	// indicates fetch did not fail.
	FetchFailedFor = IDRES_FT;	// indicates fetch did not fail.
	finished = false;
}

void LatchCopy ( Latch &ldest, Latch &lsource )
{
	ldest.PC = lsource.PC;
	ldest.inst.iV = lsource.inst.iV;
	
	ldest.targReg = lsource.targReg;
	ldest.targReg2 = lsource.targReg2;
	
	ldest.resultStage = lsource.resultStage;
	
	ldest.A = lsource.A;
	ldest.B = lsource.B;
	
	ldest.dataFetchIncomplete = lsource.dataFetchIncomplete;
	ldest.FetchFailedFor = lsource.FetchFailedFor;
	
	ldest.Imm = lsource.Imm;
	ldest.IDRes = lsource.IDRes;
	ldest.ALUOutput = lsource.ALUOutput;
	ldest.ALUOutputHi = lsource.ALUOutputHi;
	ldest.LMD = lsource.LMD;

	ldest.IsException =lsource.IsException; // 25/03/06 Copying of the latch isexception boolean
			
	ldest.finished = false;
}
