/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

 
# ifndef __PROCESSOR_H
# define __PROCESSOR_H

#include "memory.h"
#include "portmanager.h"
#include "latch.h"
#include "opcodes.h"
#include "globalvars.h"

#include <pthread.h>
#include <semaphore.h>

# define SYSCALL_HANDLER_ADDRESS 0
# define SYSTEM_START_ADDRESS 1024		// -- For User Programs...
 
# define REG_HI 	32
# define REG_LO 	33
//-------------------added 20/03/2006----------------------------------
# define REG_PC 	34
# define REG_NPC 	35
# define REG_INDEX 	36
# define REG_ENTRYHI 	37
# define REG_ENTRYLO1 	38
# define REG_ENTRYLO0 	39
# define REG_CONTEXT 	40
# define REG_PAGEMASK 	41
# define REG_BADVADDR 	42
# define REG_COUNT 	43
# define REG_COMPARE 	44
# define REG_CAUSE 	45
# define REG_EPC 	46
//--------------------------------------------------------------------
//----------------- added on 25/01/2008 by Rajeesh -------------------
# define REG_SR 	47	// Status Register - for Interrupt Enable/Disable
//--------------------------------------------------------------------


# define BREAKPOINTARRAYSIZE 16

//sign bit needed to find out the overflow
//--------------------------------------

#define SIGN_BIT 0x80000000

#define TLBWAddress	52

//---------------------------------------


#include "translate.h"			// For TLB in CP0


//class SystemControlCoprocessor;		// Defined below...


class Processor
{
private:
	word_32 reg[32];
	word_32 Hi, Lo;		// numbers REG_HI & REG_LO will be used to 
				// address these registers
	
	u_word_32 PCreg;
	u_word_32 NPCreg;
	PCWritingStage NPCfrom;
	
	bool flushStage[5];
	
	MainMemory * mem;
	Cache * dataCache;
	Cache * instrCache;
	
	PortManager * pman;
	
//-------------- 20/02/2006 ---------------//
//--------------   Rajeesh  ---------------//
	
	friend class SystemControlCoprocessor ;

	bool IsException;	// To check whether an exception occured in any of the
				// pipeline stages in a clock tick
	//--- Commented out on 06/02/2008 by Rajeesh ---//
	// Since we are now using StatusReg in CP0 itself for Interrupt Enable/Disable
	// which is the correct implementation, we can get rid of this
	//bool InterruptEnabled;	// Whether interrupts are allowed or not
	//----------------------------------------------//
	
//-----------------------------------------//
	
	
	Latch inLatch [5];
	Latch outLatch [5];
	/** 
	 * Note that inLatch[0] and 
	 * outLatch[4] have rudimentary
	 * use.
	**/
	
	// NOTE: The following booleans are flags...
	bool blockUpdate;	// This is set when mem/reg updates should no
				// longer be allowed to happen.
	bool requestProgramTermination;

	/********************************************************
	 * Following are Threading related variables
	********************************************************/
	pthread_t stagethread[5];
	
	sem_t * clocksem;
	sem_t * stagesem[5];
	sem_t * pc_mutex;
	
	// The following variables are used for the stepping 
	// and controlling the execution of the user program
	// on our simulated processor.
	int continueCount;
	word_32 breakPointArray[BREAKPOINTARRAYSIZE];
public:
	//bool SingleStep;  // TODO
	//bool Pause;       // TODO
	static int clock_count ;

	Processor ( MainMemory * m, Cache * dc, Cache * ic, PortManager * pm );
	~Processor ( );  // calls AtExit (); note that this destructor is never invoked.
	void AtExit ( ); // Destroys the threads.
	
	void Terminate ( ); // Oversees Termination of program in case of error.
	
	void Execute ( ); // Creates the threads and starts ExecutionThread
	void ExecutionThread ( );
		// Manages the clock for the processor.
		// Executes the oneClock ( ) function within an infinite loop.
	void Clock ( int clk );
		// Manages transfering and setting up
		// the pipelineLatch objects
		// and gives the user control over the execution of the
		// user program on the simulated processor.
		
	bool RegisterFetch ( RegisterFetchTarget target, int regNumber, 
		bool noFail = false ); // if noFail = true, will not fail even
		// if the data will become available only in the next cycle,
		// because the instruction needs that data only in stage3,
		// i.e., the instruction is a sw/rdout/dout
	bool RegisterFetch_Stage2 ( RegisterFetchTarget target, int regNumber );
	// Abstracts away the details of data forwarding etc.
	bool RegisterWrite ( int regNumber, RegisterWriteSource source );
	// controls the writing of registers, prevents the modification of $zero
	
	// The foll two functions provide a simple abstraction for memory access.
	// also can be used to aid in preventing memory write when 
	// implementing hardware exceptions...
	ExceptionType ReadMem ( word_32 address, word_32 & result, int noOfBytes );
	ExceptionType WriteMem ( word_32 address, word_32 value, int noOfBytes );
	
	// Abstracts away the process of updating the NPC register.
	void UpdatePC_Stage1 ( word_32 value, PCUpdateType updateType );
	void UpdatePC_Stage2 ( word_32 value, PCUpdateType updateType );
	
	// Critical section in the above two functions...
	// Also used by stage 0 to update pc.
	void PC_update_control ( word_32 value, int stage );
	
	// Each of the following is spawned as different
	// threads by the constructor of this class.
	// The friend function is the entry point which calls the 
	// corresponding member function.

//-----------------------08/03/2006----------------------------------------------------------

	void SetException(bool TorF) ;

        //--- Commented out on 06/02/2008 by Rajeesh ---//
	// Since we are now using StatusReg in CP0 itself for Interrupt Enable/Disable
	// which is the correct implementation, we can get rid of this and implement in CP0

	// bool SetInterruptStatus( bool TorF ) ;	// To enable/diable interrupts

	// bool GetInterruptStatus() ;		// To get the current interrupt status
	// --------------------------------------------//
		
//------------------------------------------------------------------------------	
	friend void* stage0 ( void * );
	void Stage0 ( );
	
	friend void* stage1 ( void * );
	void Stage1 ( );
	
	friend void* stage2 ( void * );
	void Stage2 ( );
	
	friend void* stage3 ( void * );
	void Stage3 ( );
	
	friend void* stage4 ( void * );
	void Stage4 ( );
};



//--------------------------------- 17/02/2006 ---------------------------------//
//---------------------------------   Rajeesh  ---------------------------------//

/*	An addition to the naive MIPS processor, the System Control Coprocessor
	CP0 ( c-p-zero ). CP0 is responsible for Memory Management, TLB and
	Exception Handling.
*/

class SystemControlCoprocessor {

	private:
	        static	word_32 Index;			// Index to the TLB entry
		//-------- 30/03/2006 ----------//
		word_32 EntryHi  ;		// Splitting the Entry register
		word_32 EntryLo1 ;
		word_32 EntryLo0 ;
		//TranslationEntry Entry;	// Address which maps to Index - this has been changed to above 3
		//------------------------------//
		word_32 Context;		// Address of kernel PTE entry - interrupt vector
		word_32 PageMask;		//
		word_32 BadVAddr;		// Address of memory content cause exception
		word_32 Count;			// A count regsiter, used for timer interrupts
		word_32 Compare;		//
		word_32 Cause;			// Cause of the last exception
		word_32 EPC;			// PCreg value of instruction caused exception
		//-------- 25/01/2008 ----------//
		word_32 StatusReg;		// Status Register for Interrupt Enable/Disable
		//------------------------------//
	
	public:
		Status Mode;			// User Mode or Kernel Mode, set if Kernel Mode.
		TranslationEntry * TLB;		// The TLB, for address translation
		                        	// the size of the TLB needs to be fixed??     
		SystemControlCoprocessor();
		~SystemControlCoprocessor();

		//--- Added on 06/02/2008 by Rajeesh ---//
		bool SetInterruptStatus( bool TorF ) ;  // To enable/diable interrupts

		bool GetInterruptStatus() ;   	        // To get the current interrupt status
		//--------------------------------------//

		void SignalException( ExceptionType Type );
		void SwitchMode( Status mode );
		void FlushStage();		// Flush the pipeline and disable the Write Register stage.
	

		void SetCurrentRegisters(u_word_32 epc, u_word_32 bvaddr);
	       					// to store the values in the registers for the EX stage 

		void SaveRegisterValues ();  	// To save the register values to a global array so that it 
		void RestoreRegisterValues();	// it could be accessed by the operating system .. 09-03-06

		void ReadTLBIndex();		// 13/03/06 -   For use by TLBR instruction
		
		void WriteTLBIndex();		//For use by TLBW instruction

		void ProbeTLBEntry();				// 		For use by TLBP instruction

		friend class Processor;

};

//------------------------------------------------------------------------------//
		
# endif		// __PROCESSOR_H
