/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/



//--- Added on 07/02/2008 by Rajeesh ---//
// To include the copyleft message in executable created
// the const char *copyleft should be included only one when compiling. Note the #define and #undef
#define __COPYLEFT
#include "copyleft.h"
#undef  __COPYLEFT
//-------------------------------------//

#include <iostream>
using std::cout;
using std::cin;
using std::flush;

#include <fcntl.h>
#include <semaphore.h>
sem_t * cout_mutex;	// Refered as extern from all
	// other files in 'mips' which uses cout
	// and which might encounter thread switches 
	// while c-ing out...

#include "processor.h"
#include "memory.h"
#include "simple_cache.h"
#include "portmanager.h"

#include "addressspace.h"

#include "color.h"

Cache * pickCache ( Cache * mem, bool noMultilevel, char * type, int level );

MainMemory *mem;	// Globally accessible: memory -- 23/01/2006
Cache *dc = NULL, *ic = NULL;
PortManager *pMan;
Processor *proc;
SystemControlCoprocessor *CP0;
//AddressSpace *addrSpace;

//-------------------------------------------------------------------//
//----------------   29-03-06     -----------------------------------//

#include "kernel.h"
#include "thread.h"

Kernel *kernel;
Scheduler *scheduler;
Thread *thread;


//-------------------------------------------------------------------//

u_word_32 RegisterArray[50];		// For Accessing Registers Globally

TranslationEntry TLBValues ;

word_32	StartingAddress	;		// Starting Address where UserProgram is loaded - needed for relocation

Debug *debug ;


#include "debug.h"

#include <iostream>

#include "routines.h"		// For WhenUserAbort
// The CleanUp function is called to ensure proper cleanup when user presses '^C' to abort execution
void CleanUp( int sigCode )
{
	std::cerr << red << "\n\tCleaning up after getting signal " << sigCode << "\n\n" << reset;
	std::cout << reset;
	
	delete proc ;
	delete CP0 ;
	delete kernel ;
}


int main ( int argc , char **argv )
{	
	cout_mutex = sem_open ( "coutmutex", O_CREAT | O_EXCL, O_RDWR, 1 );
	if ( cout_mutex == NULL )
	{
		sem_unlink ( "coutmutex" );
		cout_mutex = sem_open ( "coutmutex", O_CREAT, O_RDWR, 1 );
	}
	if ( cout_mutex == NULL )
	{
		cout << red << "\nError, cout_mutex semaphore couldn't be opened." 
			<< "\nTerminating... \n" << reset << flush; 
		return -3;
	}
	
	WhenUserAbort( CleanUp ) ;		// Call CleanUp when user presses '^C'
	
	string arglist = "" ;
	
	for( int i = 1 ; i < argc ; i++ )
	{
		if( strcmp(argv[i], "-d") == 0 )
		{ 
			i++;
			arglist += argv[i];	// Next argument is the debug string
		}
		
	}
	if( ! arglist.empty() )
		debug = new Debug( (char*)arglist.c_str() );
	else
		debug = new Debug( "" );
	
	mem = new MainMemory ( MemorySize ); // 4 MB
	DEBUG(dbgMachine, skyblue << "\nMain Memory Initialized");
		
	if( mem == NULL )
	{
		cout << red << "Error !! Could not allocate Memory...\n";
		return 1;
	}
	
	
	//-------------- 20/01/2006 ----------------//
	
	StartingAddress = SYSTEM_START_ADDRESS;		// -- 21/03/2006
	
	// Here we initialise the memory system
	// so that the processor starting address 
	// contains the bootloader snippet.
	

//------------------------ 09/02/2006 ----------------------------//	
//-------------------------- Rajeesh -----------------------------//
/*	
	cout << "\nChoose the type of cache you want for " 
		<< green << "Data" << reset << flush;
	dc = pickCache ( mem, false, "DATA", 1 );	// Multilevels are allowed
	
	cout << "\nChoose the type of cache you want for "
		<< green << "Instructions" << reset << flush;
	ic = pickCache ( mem, false, "INSTRUCTION", 1 );	// Multilevels are allowed
	
	if ( dc == NULL ) // No data cache
	{
		cout << red << "\nError, No Data Cache specified... \nTerminating... \n"
			<< reset << flush;
		return -1;
	}

	if ( ic == NULL ) // No data cache
	{
		cout << red << "\nError, No Instruction Cache specified... "
			<< "\nTerminating... \n" << reset << flush;
		return -1;
	}
*/
	
	dc = dynamic_cast<Cache *>( new SimpleCache(dynamic_cast<Cache*>(mem), 32, 64,
                                        2, "DATA", 1, false) );
	DEBUG(dbgMachine, skyblue << "\nSimple Data Cache initialized !!");

	
	if ( dc == NULL ) // No data cache
	{
		cout << red << "\nError, No Data Cache specified... \nTerminating... \n"
			<< reset << flush;
		return -1;
	}

	ic = dynamic_cast<Cache *>( new SimpleCache(dynamic_cast<Cache*>(mem), 32, 64,
                                        2, "INSTRUCTION", 1, false) );

	DEBUG(dbgMachine, skyblue << "\nSimple Instruction Cache Initialized !!");
	
	if ( ic == NULL ) // No data cache
	{
		cout << red << "\nError, No Instruction Cache specified... "
			<< "\nTerminating... \n" << reset << flush;
		return -1;
	}

//----------------------------------------------------------------//	
	// We also need to create the port manager system
	// And set up the mapping between ports or device numbers
	// and sockets...
	pMan = new PortManager ( );
	pMan -> AddPort ( 1, 5678 );	// A character Input device 
	DEBUG(dbgNet, skyblue << "\nAdded port for input device");
	
	pMan -> AddPort ( 2, 5680 );	// A character Output device
	DEBUG(dbgNet, skyblue << "\nAdded port for output device");
	
	proc = new Processor ( mem, dc,ic, pMan );
	DEBUG(dbgMachine, skyblue << "\nInitialized the Processor !!");
	
	CP0 = new SystemControlCoprocessor ;
	DEBUG(dbgMachine, skyblue << "\nInitialized the System Control CoProcessor");

//------------------------- 19/03/2006 --------------------------//

	if( ! mem -> Load_MIPS_program ( "ISR" ) )	//------------//
	{
                cout << red << "\nError, could not load the \"ISR\" ..."
                     << "\nTerminating... \n" << reset << flush;
                return -2;
	}
	
	DEBUG(dbgMachine, violet << "\n The ISR is loaded");

	// We need to setup the very first TLB entry - to avoid TLB miss for ISR !!!

	TranslationEntry entry;
	entry.virtualPage = 0;
	entry.physicalPage = 0;
	entry.valid = true;
	entry.use = true;
	entry.dirty = false;
	entry.readOnly = false;

	CP0->TLB[0] = entry;	// Set up first TLB entry for ISR -- 19/03/2006 --
	
//---------------------------------------------------------------//

	kernel = new Kernel();			//Create the kernel
	DEBUG( dbgThread , skyblue << "\nThe Kernel initialized ");
	if ( kernel -> Initialize () == -2 )	// The kernel's initialize function is called to make the initializations
		return -2;

	//kernel ->console -> pMan = pMan;	
	proc->Execute ( );	// Now this thread runs the processor clock function...
	
	return 0;
}

# define MULTILEVEL 3

Cache * pickCache ( Cache * mem, bool noMultilevel, char * type, int level )
{
	cout << "\n" << level << "-level " << type 
		<< " Cache : The choices available are"
		<< "\n 1. No Cache "
		<< "\n 2. Simple single level FIFO writeback cache ";
	if ( noMultilevel == false )
		cout << "\n " << MULTILEVEL << ". Multilevel Cache ";
	cout << "\nPlease enter your choice : " << flush;
	
	int choice;
	cin >> choice;
	while ( choice < 1 || ( noMultilevel == true && choice > (MULTILEVEL-1) ) ||
		( noMultilevel == false && choice > MULTILEVEL ) )
	{
		cout << red << "\nBad choice, Enter again : " << reset << flush;
		cin >> choice;
	}
	
	Cache * c = NULL;
	
	switch ( choice )
	{
	case 1:
		c = dynamic_cast<Cache *>( new NoCache (dynamic_cast<Cache*>(mem), type, level) );
		break;
	case 2: 
		{
			cout << "\nEnter number of Blocks in cache : ";
			int nob; cin >> nob;
			cout << "\nEnter number of words per block : ";
			int wpb; cin >> wpb;
			cout << "\nEnter associativity : ";
			int assoc; cin >> assoc;
			cout << "\nSelect extent of cache info displayed : "
				<< "\n 1.verbose"
				<< "\n 2.silent"
				<< "\nEnter your choice : ";
			int display; cin >> display;
			while ( display < 1 || display > 2 )
			{
				cout << red << "\nBad choice, Enter again : " 
					<< reset << flush;
				cin >> display;
			}
			bool verbose = ( display == 1 )? true : false ;
			
			c = dynamic_cast<Cache *>( new SimpleCache(dynamic_cast<Cache*>(mem), nob, wpb,
					assoc, type, level, verbose) );
		}
		break;
	case MULTILEVEL:
		{
			cout << "\nEnter the number of levels in the Cache : ";
			int noLevels; cin >> noLevels;
			Cache * prev_c = mem;
			for ( int i = 0; i < noLevels; i++ )
			{
				cout << "\nChoose the " << noLevels - i << "-level cache : ";
				c = pickCache ( prev_c, true, type, noLevels - i );
					// 2-nd arg = true => no multilevel
				prev_c = c;
			}
		}
		break;
	};
	return c;
}
