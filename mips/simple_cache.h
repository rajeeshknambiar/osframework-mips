/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

 
# ifndef __SIMPLE_CACHE
# define __SIMPLE_CACHE

#include "memory.h"

#include "translate.h"		// For ExceptionType

class SimpleCache_TagRecord
{
public:
	int tag;
	bool valid;
	bool modified;
	
	SimpleCache_TagRecord ( );
};

class SimpleCache : public Cache
{
private:
	// Memory is also a type of cache to 
	// make multilevel cache's easier to implement
	Cache * mem;
	
	char type[TYPEFIELDSIZE];
	int level;
	bool verbose;
	
	int noOfBlocks;
	int wordsPerBlock;
	int associativity;
	int noOfSets;

	word_32 *** cache;
	SimpleCache_TagRecord ** tagArray;
	int * fifoIndex;
	
	int readCount;
	int readHitCount;
	int writeCount;
	int writeHitCount;
public:
	SimpleCache ( Cache * memory, int nob, int wpb, int assoc, const char * ty, int lev,
		bool verbos );
	void Statistics ( );
	ExceptionType Read ( word_32 address, word_32 & result, int noOfBytes );// 27/03/06 - changed from bool to 
										// 		ExceptionType
	bool Read_nofetch ( word_32 address, word_32 & result, int noOfBytes );
	ExceptionType Write ( word_32 address, word_32 value, int noOfBytes );  // 27/03/06 - same as Read
	void AtExit ( );
};

# endif
