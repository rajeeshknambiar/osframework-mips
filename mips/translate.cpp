/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

//-------------------- 19/01/2006 ---------------------//
//--------------------   Rajeesh  ---------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

#include "copyleft.h"

/********************************************************************************
  This file is part of the software authored by the following persons : 	

  Rajeesh K V
  Thamanna Q R
  Dinesh V

********************************************************************************/


/*

   The sole purpose of this file is to provide the implementation of the 
   Address Translation mechanism for a processor. For every memory read/write
   signal issued by the processor, the associated Virtual address has to be
   mapped to a so called Physical address. This process of mapping the
   virtual address to physical address based on a Page Table/Translation Look
   ahead Buffer(TLB) is called address translation.

*/


#include "translate.h"
#include "color.h"
#include "debug.h"


TranslationEntry :: TranslationEntry()
{
	virtualPage = physicalPage = -1;
	valid = use = dirty = readOnly = false;
}

TranslationEntry :: ~TranslationEntry()
{
}

bool TranslationEntry :: operator == ( TranslationEntry entry )
{
	if( this->virtualPage  == entry.virtualPage &&
	    this->physicalPage == entry.physicalPage )
		return true;
	else
		return false;
}
	    

ExceptionType TranslationEntry ::
Translate( word_32 virtualAddress, word_32 & physicalAddress )
{
	u_word_32 vpn	 = virtualAddress / PageSize ;
	u_word_32 offSet = virtualAddress % PageSize ;
	
	if( this[vpn].valid == false || this[vpn].physicalPage == -1 )	// The pageframe found out is invalid
	{
		DEBUG( dbgMachine, red << "\n[ Translate ] Address Miss !" ) ;
		return TLBMISS;
	}
					// If found, compute physical address
	physicalAddress = this[vpn].physicalPage * PageSize + offSet;
	return NOEXCEPTION;
}
	
