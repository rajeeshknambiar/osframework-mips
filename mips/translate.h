/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

//--------------------- 19/01/2006 ----------------------//
//---------------------   Rajeesh  ----------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

#include "copyleft.h"

#ifndef __TRANSLATE_H
#define __TRANSLATE_H

#include "types.h"
#include <iostream>

/*
   This header file is intended to provide definitions for the use of 
   address translation mechanism. Initially, only Page table is supported,
   which could be extended to a TLB too.

*/

#define PageSize 1024
#define TLBSIZE 48


using std::cout;
using std::endl;

enum ExceptionType{
	NOEXCEPTION,
	BADADDRESS,
	TLBMISS,
	ALIGNMENTERROR,
	SYSCALL,
	TRAP,
	OVERFLOW,
	DIVIDE_ERROR,
	TIMERINTERRUPT,
	RESERVED_INSTRUCTION
};

enum Status{
	KernelMode,
	UserMode
};
	

class TranslationEntry{
	public:
		word_32 virtualPage ;
		word_32 physicalPage;
		bool valid;
		bool use;
		bool dirty;
		bool readOnly;

		TranslationEntry();
		~TranslationEntry();
		bool operator == ( TranslationEntry entry );

		ExceptionType Translate( word_32 virtualAddress, word_32 & physicalAddress );
};




#endif // __TRANSLATE_H
