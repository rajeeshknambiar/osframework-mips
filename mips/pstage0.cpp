/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

 
//#include "processor.h"

#include <iostream>
using std::cout;
using std::flush;

#include "color.h"

#include <semaphore.h>
extern sem_t * cout_mutex;	// Defined in main.cpp

#include "main.h"		// For CP0

void Processor :: Stage0 ( )
{
	LatchCopy ( outLatch[0], inLatch[0] ); 
	// Copy inLatch into outLatch... NowForth work with outLatch
	// Note the invariant that inLatch[0] is a constant for all practical 
	// purposes.
	//	cout<< "\n PC reg value"<< PCreg <<endl;

	word_32 oldPC = PCreg;
	ExceptionType exception;

	//if ( instrCache -> Read ( PCreg, outLatch[0].inst.iV, 4 ) == true )
	if ( (exception=instrCache -> Read ( PCreg, outLatch[0].inst.iV, 4 )) == NOEXCEPTION )
	{
		outLatch[0].PC = PCreg;
	
		PC_update_control ( PCreg + 4, 0 );
	
		outLatch[0].finished = true;
/*		sem_wait ( cout_mutex );
		cout << "\n[ Stage0 ] PC to fetch = " << PCreg << ", Instruction = " 
			<< outLatch[0].inst.iV << flush;
		sem_post ( cout_mutex );
*/
		DEBUG(dbgMachine,"\n[ Stage0 ] PC to fetch = "<< PCreg << ", Instruction = "
				<< outLatch[0].inst.iV);
	}
	else
	{
		outLatch[0].finished = false;
/*		sem_wait ( cout_mutex );
		cout << red << "\n[ Stage0 ] PC ("<< oldPC <<") fetch failed, will try again in next clock"
			<< reset << flush;
		sem_post ( cout_mutex );
*/

		DEBUG(dbgMachine,red <<"\n[ Stage0 ] PC ("<< oldPC <<") fetch failed, will try again in next clock");		
		//----------------------- 27/03/2006 --------------------------//
		//-----------------------   Rajeesh  --------------------------//

		SetException( true );
		//proc -> blockUpdate = true;	// The blockUpdate boolean in Processor has been set.
		CP0 -> SetCurrentRegisters( PCreg, PCreg );	// EPC = PC and BVADDR = PC ...
		CP0 -> SignalException( exception );
		
		//-------------------------------------------------------------//

	}
}

void Processor :: PC_update_control ( word_32 value, int stage )
{
	sem_wait ( pc_mutex );
	
	switch ( stage )
	{
	case 2:
		NPCreg = value;
		NPCfrom = PC_STAGE2;
/*		sem_wait ( cout_mutex );
		cout << skyblue << "\n[__ PC_update_control __] NPC updated by stage 2 to "
			<< reset << NPCreg << flush;
		sem_post ( cout_mutex );
	*/
		DEBUG(dbgMachine,skyblue <<"\n[__ PC_update_control __] NPC updated by stage 2 to"<< NPCreg);
		break;
	case 1:
		if ( NPCfrom == PC_STAGE2 )
		{
/*			sem_wait ( cout_mutex );
			cout << skyblue << "\n[__ PC_update_control __]"
				<< " NPC update by stage 1 aborted"
				<< reset << flush;
			sem_post ( cout_mutex );
*/
			DEBUG(dbgMachine,skyblue <<"\n[__ PC_update_control __] NPC update by stage 1 aborted");
			break;
		}
		NPCreg = value;
		NPCfrom = PC_STAGE1;
/*		sem_wait ( cout_mutex );
		cout << skyblue << "\n[__ PC_update_control __] NPC updated by stage 1 to "
			<< reset << NPCreg << flush;
		sem_post ( cout_mutex );
*/
		DEBUG(dbgMachine,skyblue <<"\n[__ PC_update_control __] NPC updated by stage 1 to "<< NPCreg );
		break;
	case 0:
		if ( NPCfrom == PC_STAGE2 || NPCfrom == PC_STAGE1 )
		{
/*			sem_wait ( cout_mutex );
			cout << skyblue << "\n[__ PC_update_control __]"
				<< " NPC update by stage 0 aborted"
				<< reset << flush;
			sem_post ( cout_mutex );
*/
			DEBUG(dbgMachine,skyblue <<"\n[__ PC_update_control __] NPC update by stage 0 aborted");
			break;
		}
		NPCreg = value;
		NPCfrom = PC_STAGE0;
/*		sem_wait ( cout_mutex );
		cout << skyblue << "\n[__ PC_update_control __] NPC updated by stage 0 to "
			<< reset << NPCreg << flush;
		sem_post ( cout_mutex );
*/
		DEBUG(dbgMachine,skyblue <<"\n[__ PC_update_control __] NPC updated by stage 0 to "<< NPCreg);
		break;
	};
	
	sem_post ( pc_mutex );
}
