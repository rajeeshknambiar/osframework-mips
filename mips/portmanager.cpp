/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

 

#include "portmanager.h"

#include <sys/socket.h>

//#include <netinet/in.h>
// Redundant, for struct sockaddr_in.

#include <netdb.h>
// For sockaddr_in, htons(), hostent, gethostbyname().

#include <unistd.h>
// For close()

#include <cstring>
using std::strcpy;
using std::memcpy;

#include <iostream>
using std::cout;
using std::flush;

#include "color.h"

#include <semaphore.h>
extern sem_t * cout_mutex;	// Defined in main.cpp
// Only Read and Write functions need mutual exclusion for 
// cout statements.

PortManager::PortManager ( )
{
	strcpy ( hostname, "localhost" );
	for ( int i = 0; i < MAX_PORTS ; i++ )
	{
		portMap [i] = -1;
	}
}

PortManager :: ~PortManager ( )
{
	AtExit ( );
}

void PortManager :: AtExit ( )
{
	for ( int i = 0; i < MAX_PORTS ; i++ )
	{
		if ( portMap [i] != -1 )
			RemovePort ( i );
	}
}

int PortManager :: AddPort ( int portNo, int mapsTo )
{
	if ( portMap [portNo] != -1 ) return -1;
	portMap [portNo] = socket ( AF_INET, SOCK_STREAM, 0 );
	if ( portMap [portNo] == -1 ) return -2;
	
	sockaddr_in deviceManager;
	deviceManager.sin_family = AF_INET;
	deviceManager.sin_port = htons(mapsTo);
	
	hostent * hp = gethostbyname ( hostname );
	if ( hp == NULL )
	{
		close ( portMap [portNo] );
		portMap [portNo] = -1;
		return -3;
	}
	
	memcpy(reinterpret_cast<char*>(&deviceManager.sin_addr),
			reinterpret_cast<char*>(hp->h_addr_list[0]), hp->h_length);
	
	if ( connect( portMap[portNo], reinterpret_cast<sockaddr*>(&deviceManager),
				sizeof(deviceManager)) != 0 )
	{
/*
   cout << red << "\n[ PortManager :: AddPort ] Attempt to connect Device "
			<< portNo << " to socket " << mapsTo << " falied" 
			<< reset << flush;
*/
		DEBUG(dbgNet, red << "\n[ PortManager :: AddPort ] Attempt to connect Device "
				<< portNo << " to socket " << mapsTo << " falied" << reset );
		close ( portMap [portNo] );
		portMap [portNo] = -1;
		return -4;
	}
	else 
	{
/*	
		cout << green << "\n[ PortManager :: AddPort ] Connected Device " << portNo
		<< " to socket " << mapsTo << " successfully" << reset << flush;
*/
		DEBUG(dbgNet, green << "\n[ PortManager :: AddPort ] Connected Device "
				<< " to socket " << mapsTo << " successfully" << reset );
	}
		
	return 0;
}

int PortManager :: RemovePort ( int portNo )
{
	if ( portMap [ portNo ] == -1 )
	{
//		cout << red << "\n[ PortManager :: RemovePort ] User attempted to"
//			<< " release unallocated Device No "
//			<< portNo << reset << flush;
		DEBUG(dbgNet,red << "\n[ PortManager :: RemovePort ] User attempted to"
				<< " release unallocated Device No "<< portNo );
		return -1;
	}
	shutdown ( portMap [ portNo ], 2 );
	close ( portMap [ portNo ] );
	portMap [ portNo ] = -1 ;
	/*
	cout << green << "\n[ PortManager :: RemovePort ] Successfully released Device " 
		<< portNo << reset << flush;*/

	DEBUG(dbgNet,green << "\n[ PortManager :: RemovePort ] Successfully released Device "
			<< portNo);
	return 0;
}

int PortManager :: Write ( int portNo, word_32 oneWord )
{
	if ( portMap [ portNo ] == -1 )
	{/*
		sem_wait ( cout_mutex );
		cout << red << "\n[ PortManager :: Write ] User attempted to write"
			<< " to unallocated Device "
			<< portNo << reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgNet,red << "\n[ PortManager :: Write ] User attempted to write"
				<< " to unallocated Device "<< portNo);
		return -1;
	}
	if ( send ( portMap [ portNo ], &oneWord, 4, 0 ) == -1 )
	{/*
		sem_wait ( cout_mutex );
		cout << red << "\n[ PortManager :: Write ] Error while writing to Device " 
			<< portNo << reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgNet,red << "\n[ PortManager :: Write ] Error while writing to Device "<< portNo);
		return -2;
	}
	/*
	sem_wait ( cout_mutex );
	cout << green << "\n[ PortManager :: Write ] Wrote word to Device " 
		<< portNo << reset << flush;
	sem_post ( cout_mutex );*/

	DEBUG(dbgNet,green << "\n[ PortManager :: Write ] Wrote word to Device "<< portNo);
	return 0;
}

int PortManager :: Read ( int portNo, word_32 & oneWord )
{
	if ( portMap [ portNo ] == -1 )
	{/*
		sem_wait ( cout_mutex );
		cout << red << "\n[ PortManager :: Read ] User attempted to read"
			<< " from unallocated Device "
			<< portNo << reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgNet,red << "\n[ PortManager :: Read ] User attempted to read"
				<< " from unallocated Device "<< portNo );
		return -1;
	}
	if ( recv ( portMap [ portNo ], &oneWord, 4, 0 ) == -1 )
	{/*
		sem_wait ( cout_mutex );
		cout << red << "\n[ PortManager :: Read ] Error while reading from Device " 
			<< portNo << reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgNet,red << "\n[ PortManager :: Read ] Error while reading from Device "<< portNo);
		return -2;
	}
	/*
	sem_wait ( cout_mutex );
	cout << green << "\n[ PortManager :: Read ] Read word from Device " 
		<< portNo << reset << flush;
	sem_post ( cout_mutex );*/
	DEBUG(dbgNet,green << "\n[ PortManager :: Read ] Read word from Device "<< portNo);
	return 0;
}
