/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

  
//#include "processor.h"
#include "main.h"


#include <iostream>
using std::cout;
using std::cout;
using std::flush;

#include "color.h"

#include <semaphore.h>
extern sem_t * cout_mutex;	// Defined in main.cpp

void Processor :: Stage4 ( )
{
	// Copy inLatch into outLatch... NowForth work with outLatch
	LatchCopy (outLatch[4], inLatch[4]);
	
	// Fisrt check for NOP
	if ( outLatch[4].inst.iV == 0 )
	{
		// Do Nothing 
		outLatch[4].finished = true;
/*		
		sem_wait ( cout_mutex );
		cout << "\n[ Stage4 ] NOP" << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage4 ] NOP");
	}
	else // else perform the switch code
	switch ( outLatch[4].inst.noF.op )
	{
	case OP_ZERO:
		switch ( outLatch[4].inst.rF.funct )
		{
		case FUNCT_ADD:
			if( outLatch[4].IsException )
			{
				CP0 -> EPC = outLatch[4].PC;
				CP0 -> SignalException (OVERFLOW);
			}	
			else
			{
				RegisterWrite ( outLatch[4].targReg, ALU );
				outLatch[4].finished = true;
				/*
				sem_wait ( cout_mutex );
				cout << "\n[ Stage4 ] ADD stored " << outLatch[4].ALUOutput
					<< " into r" << outLatch[4].targReg << flush;
				sem_post ( cout_mutex );*/
				DEBUG(dbgMachine,"\n[ Stage4 ] ADD stored " << outLatch[4].ALUOutput
						<< " into r" << outLatch[4].targReg);
			}
				break;
			
		case FUNCT_AND:
			RegisterWrite ( outLatch[4].targReg, ALU );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] AND stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] AND stored " << outLatch[4].ALUOutput
					<< " into r" << outLatch[4].targReg);
			break;
			
		case FUNCT_DIV:
			if( outLatch[4].IsException )
			{
				SetException( true );
				CP0->SignalException( DIVIDE_ERROR );
				outLatch[4].finished = true;
				break;
			}
			
			RegisterWrite ( outLatch[4].targReg, ALU );
			RegisterWrite ( outLatch[4].targReg2, ALU_HI );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] DIV stored " << outLatch[4].ALUOutput
				<< " into Lo" << flush;
			cout << "\n[ Stage4 ] DIV stored " << outLatch[4].ALUOutputHi
				<< " into Hi" << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] DIV stored " << outLatch[4].ALUOutput
					<< " into Lo"<< "\n[ Stage4 ] DIV stored " << outLatch[4].ALUOutputHi
					<< " into Hi" );
			break;
			
		case FUNCT_MULT:
			RegisterWrite ( outLatch[4].targReg, ALU );
			RegisterWrite ( outLatch[4].targReg2, ALU_HI );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] MULT stored " << outLatch[4].ALUOutput
				<< " into Lo" << flush;
			cout << "\n[ Stage4 ] MULT stored " << outLatch[4].ALUOutputHi
				<< " into Hi" << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] MULT stored " << outLatch[4].ALUOutput
					<< " into Lo"<< "\n[ Stage4 ] MULT stored " << outLatch[4].ALUOutputHi
					<< " into Hi");
			break;
			
		case FUNCT_NOR:
			RegisterWrite ( outLatch[4].targReg, ALU );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] NOR stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] NOR stored " << outLatch[4].ALUOutput
					<< " into r");
			break;
			
		case FUNCT_OR:
			RegisterWrite ( outLatch[4].targReg, ALU );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] OR stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] OR stored " << outLatch[4].ALUOutput
					<< " into r" << outLatch[4].targReg);
			break;
			
		case FUNCT_SLL:
			RegisterWrite ( outLatch[4].targReg, ALU );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] SLL stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] SLL stored " << outLatch[4].ALUOutput
					<< " into r" << outLatch[4].targReg);
			break;
			
		case FUNCT_SLLV:
			RegisterWrite ( outLatch[4].targReg, ALU );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] SLLV stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] SLLV stored " << outLatch[4].ALUOutput
					<< " into r" << outLatch[4].targReg);
			break;
			
		case FUNCT_SRA:
			RegisterWrite ( outLatch[4].targReg, ALU );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] SRA stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] SRA stored " << outLatch[4].ALUOutput
					<< " into r" << outLatch[4].targReg );
			break;
			
		case FUNCT_SRAV:
			RegisterWrite ( outLatch[4].targReg, ALU );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] SRAV stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] SRAV stored " << outLatch[4].ALUOutput
					<< " into r" << outLatch[4].targReg );
			break;
			
		case FUNCT_SRL:
			RegisterWrite ( outLatch[4].targReg, ALU );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] SRL stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] SRL stored " << outLatch[4].ALUOutput
					<< " into r" << outLatch[4].targReg);
			break;
			
		case FUNCT_SRLV:
			RegisterWrite ( outLatch[4].targReg, ALU );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] SRLV stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] SRLV stored " << outLatch[4].ALUOutput
					<< " into r" << outLatch[4].targReg);
			break;
			
		case FUNCT_SUB:
			RegisterWrite ( outLatch[4].targReg, ALU );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] SUB stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] SUB stored " << outLatch[4].ALUOutput
					<< " into r" << outLatch[4].targReg);
			break;
			
		case FUNCT_XOR:
			RegisterWrite ( outLatch[4].targReg, ALU );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] XOR stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] XOR stored " << outLatch[4].ALUOutput
					<< " into r" << outLatch[4].targReg);
			break;
			
		case FUNCT_SLT:
			RegisterWrite ( outLatch[4].targReg, ALU );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] SLT stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] SLT stored " << outLatch[4].ALUOutput
					<< " into r" << outLatch[4].targReg );
			break;
		
		case FUNCT_JR:
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] JR idle" << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] JR idle");
			break;
			
		case FUNCT_JALR:
			RegisterWrite ( outLatch[4].targReg, IDRES );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] JALR stored " << outLatch[4].IDRes
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] JALR stored " << outLatch[4].IDRes
					<< " into r" << outLatch[4].targReg);
			break;
			
		case FUNCT_MFHI:
			RegisterWrite ( outLatch[4].targReg, IDRES );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] MFHI stored " << outLatch[4].IDRes
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] MFHI stored " << outLatch[4].IDRes
					<< " into r" << outLatch[4].targReg );
			break;
			
		case FUNCT_MFLO:
			RegisterWrite ( outLatch[4].targReg, IDRES );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] MFLO stored " << outLatch[4].IDRes
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] MFLO stored " << outLatch[4].IDRes
					<< " into r" << outLatch[4].targReg );
			break;
			
		case FUNCT_MTHI:
			RegisterWrite ( outLatch[4].targReg, IDRES );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] MTHI stored " << outLatch[4].IDRes
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] MTHI stored " << outLatch[4].IDRes
					<< " into r" << outLatch[4].targReg);
			break;
			
		case FUNCT_MTLO:
			RegisterWrite ( outLatch[4].targReg, IDRES );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] MTLO stored " << outLatch[4].IDRes
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] MTLO stored " << outLatch[4].IDRes
					<< " into r" << outLatch[4].targReg );
			break;
			
		case FUNCT_SYSCALL:
			RegisterWrite ( outLatch[4].targReg, IDRES );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] SYSCALL stored " << outLatch[4].IDRes
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] SYSCALL stored " << outLatch[4].IDRes
					<< " into r" << outLatch[4].targReg );

			SetException( true );

			proc -> blockUpdate = true;	// The blockUpdate boolean in Processor has been set.
			CP0 -> EPC = outLatch[4].IDRes; // Setting the EPC to (PC+4) or Branch Delay slot PC,
		       					  // 'Latch.IDRes' contains the correct value   --13/02/2006--
							  // this is done in the next step - SetCurrentTegisters

			//CP0 -> SetCurrentRegisters( outLatch[4].IDRes, outLatch[4].PC );
			CP0 -> SignalException( SYSCALL );	// Raise the exception 
			
			break;
		
		case FUNCT_RDIN:
			RegisterWrite ( outLatch[4].targReg, LOAD );
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] RDIN stored " << outLatch[4].LMD 
				<< " into r" << outLatch[4].targReg << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] RDIN stored " << outLatch[4].LMD
					<< " into r" << outLatch[4].targReg);
			//SetInterruptStatus( true );
			break;
			
		case FUNCT_RDOUT:
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] RDOUT idle" << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine, "\n[ Stage4 ] RDOUT idle");
			//SetInterruptStatus( true );
			break;
//-------------------------------------------------------------------------------------------------------//
//---------------------modified 13-02-2006----------OP_ZERO TRAP instructions---------------------------//
	
		case FUNCT_TEQ:   // the instruction is TEQ
			if ( outLatch[4].IsException )
			{
				CP0 -> EPC = outLatch[4].PC;
				CP0 -> SignalException ( TRAP );
			}
			
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n [ Stage4 ]	TEQ idle" <<flush;
			sem_post (cout_mutex );*/
			DEBUG(dbgMachine,"\n [ Stage4 ]  TEQ idle");
			break;
                case FUNCT_TGE: // the instruction is TGE
                        if ( outLatch[4].IsException )
			{
				CP0 -> EPC = outLatch[4].PC;
                           	CP0 -> SignalException ( TRAP );
			}
						
                        outLatch[4].finished = true;
			/*
                        sem_wait ( cout_mutex );
                        cout << "\n [ Stage4 ]  TGE idle" <<flush;
                        sem_post (cout_mutex );*/
			DEBUG(dbgMachine,"\n [ Stage4 ]  TGE idle");
                        break;
                case FUNCT_TLT: //the instruction is TLT
                        if ( outLatch[4].IsException )
			{
				CP0 -> EPC = outLatch[4].PC;
				CP0 -> SignalException ( TRAP );
			}
			outLatch[4].finished = true;
			/*
                        sem_wait ( cout_mutex );
                        cout << "\n [ Stage4 ]  TLE idle" <<flush;
                        sem_post (cout_mutex );*/
			DEBUG(dbgMachine,"\n [ Stage4 ]  TLE idle");
                        break;
                case FUNCT_TNE: // the instruction is TNE
                        if ( outLatch[4].IsException )
			{
				CP0 -> EPC = outLatch[4].PC;
				CP0 -> SignalException ( TRAP );
			}
			outLatch[4].finished = true;
			/*
                        sem_wait ( cout_mutex );
                        cout << "\n [ Stage4 ]  TNE idle" <<flush;
                        sem_post (cout_mutex );*/
			DEBUG(dbgMachine,"\n [ Stage4 ]  TNE idle");
                        break;
                		
//------------------------------------------------------------------------------------------------------//			
		};
		break;
		
	case OP_ONE:
		switch ( outLatch[4].inst.iF.rt )
		{
		case OP_BGEZ:	// The instruction is BGEZ
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] BGEZ idle" << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] BGEZ idle");
			break;
		case OP_BLTZ:	// The instruction is BLTZ
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ] BLTZ idle" << flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ] BLTZ idle");
			break;
//-----------------------------modified 13-02-2006-----------------------------------------------------//
//-------------------------OP_ONE instructions for TRAP----------------------------------------------//
			
		case OP_TEQI: // The instruction is TEQI			
                        if ( outLatch[4].IsException )
			{
				CP0 -> EPC = outLatch[4].PC;
				CP0 -> SignalException ( TRAP );
			}
			outLatch[4].finished = true;
			/*
			sem_wait ( cout_mutex );
			cout << "\n[ Stage4 ]TEQI idle"<< flush;
			sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ]TEQI idle");
			break;
                case OP_TGEI: // The instruction is TGEI
                        if ( outLatch[4].IsException )
			{
				CP0 -> EPC = outLatch[4].PC;
				CP0 -> SignalException ( TRAP );
			}
			outLatch[4].finished = true;
			/*
                        sem_wait ( cout_mutex );
                        cout << "\n[ Stage4 ]TGEI idle"<< flush;
                        sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ]TGEI idle");
                        break;
                case OP_TLTI: // The instruction is TLTI
                        if ( outLatch[4].IsException )
			{
				CP0 -> EPC = outLatch[4].PC;
				CP0 -> SignalException ( TRAP );
			}
			outLatch[4].finished = true;
			/*
                        sem_wait ( cout_mutex );
                        cout << "\n[ Stage4 ]TLTI idle"<< flush;
                        sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ]TLTI idle");
                        break;
                case OP_TNEI: // The instruction is TNEI
                        if ( outLatch[4].IsException )
			{
				CP0 -> EPC = outLatch[4].PC;
				CP0 -> SignalException ( TRAP );
			}
			outLatch[4].finished = true;
			/*
                        sem_wait ( cout_mutex );
                        cout << "\n[ Stage4 ]TNEI idle"<< flush;
                        sem_post ( cout_mutex );*/
			DEBUG(dbgMachine,"\n[ Stage4 ]TNEI idle");
                        break;
//--------------------------------------------------------------------------------------------------//			
		};
		break;
		
	case OP_ADDI:
		if ( outLatch[4].IsException )
		{
			CP0 -> EPC = outLatch[4].PC;
			CP0 -> SignalException( OVERFLOW );
		}
		else
		{
		RegisterWrite ( outLatch[4].targReg, ALU );
		outLatch[4].finished = true;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage4 ] ADDI stored " << outLatch[4].ALUOutput
			<< " into r" << outLatch[4].targReg << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage4 ] ADDI stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg);
		}
		break;
		
	case OP_ANDI:
		RegisterWrite ( outLatch[4].targReg, ALU );
		outLatch[4].finished = true;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage4 ] ANDI stored " << outLatch[4].ALUOutput
			<< " into r" << outLatch[4].targReg << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage4 ] ANDI stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg);
		break;
		
	case OP_ORI:
		RegisterWrite ( outLatch[4].targReg, ALU );
		outLatch[4].finished = true;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage4 ] ORI stored " << outLatch[4].ALUOutput
			<< " into r" << outLatch[4].targReg << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage4 ] ORI stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg );
		break;
		
	case OP_XORI:
		RegisterWrite ( outLatch[4].targReg, ALU );
		outLatch[4].finished = true;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage4 ] XORI stored " << outLatch[4].ALUOutput
			<< " into r" << outLatch[4].targReg << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage4 ] XORI stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg );
		break;
		
	case OP_LUI:
		RegisterWrite ( outLatch[4].targReg, ALU );
		outLatch[4].finished = true;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage4 ] LUI stored " << outLatch[4].ALUOutput
			<< " into r" << outLatch[4].targReg << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage4 ] LUI stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg );
		break;
		
	case OP_SLTI:
		RegisterWrite ( outLatch[4].targReg, ALU );
		outLatch[4].finished = true;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage4 ] SLTI stored " << outLatch[4].ALUOutput
			<< " into r" << outLatch[4].targReg << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage4 ] SLTI stored " << outLatch[4].ALUOutput
				<< " into r" << outLatch[4].targReg );
		break;
		
	case OP_BEQ:
		outLatch[4].finished = true;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage4 ] BEQ idle" << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage4 ] BEQ idle" );
		break;
		
	case OP_BGTZ:
		outLatch[4].finished = true;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage4 ] BGTZ idle" << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage4 ] BGTZ idle");
		break;
		
	case OP_BLEZ:
		outLatch[4].finished = true;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage4 ] BLEZ idle" << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage4 ] BLEZ idle");
		break;
		
	case OP_BNE:
		outLatch[4].finished = true;
		/*	
		sem_wait ( cout_mutex );
		cout << "\n[ Stage4 ] BNE idle" << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage4 ] BNE idle" );
		break;
		
	case OP_J:
		outLatch[4].finished = true;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage4 ] J idle" << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage4 ] J idle" );
		break;
		
	case OP_JAL:
		RegisterWrite ( outLatch[4].targReg, IDRES );
		outLatch[4].finished = true;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage4 ] JAL stored " << outLatch[4].IDRes
			<< " into r" << outLatch[4].targReg << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage4 ] JAL stored " << outLatch[4].IDRes
				<< " into r" << outLatch[4].targReg );
		break;
		
	case OP_LW:
		RegisterWrite ( outLatch[4].targReg, LOAD );
		outLatch[4].finished = true;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage4 ] LW stored " << outLatch[4].LMD 
			<< " into r" << outLatch[4].targReg << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage4 ] LW stored " << outLatch[4].LMD
				<< " into r" << outLatch[4].targReg);
		break;
		
	case OP_SW:
		outLatch[4].finished = true;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage4 ] SW idle" << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage4 ] SW idle");
		break;
		
	case OP_DIN:
		RegisterWrite ( outLatch[4].targReg, LOAD );
		outLatch[4].finished = true;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage4 ] DIN stored " << outLatch[4].LMD 
			<< " into r" << outLatch[4].targReg << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage4 ] DIN stored " << outLatch[4].LMD
				<< " into r" << outLatch[4].targReg );
		//SetInterruptStatus( true );
		break;
		
	case OP_DOUT:
		outLatch[4].finished = true;
		/*
		sem_wait ( cout_mutex );
		cout << "\n[ Stage4 ] DOUT idle" << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,"\n[ Stage4 ] DOUT idle");
		//SetInterruptStatus( true );
		break;
//----------------------------The co-procesor instructions--------------------------------------------//
//---------------------------modified 13-02-206------------------------------------------------------//		
	case OP_COP0:
		  if( outLatch[4].IsException )
		  {
			  DEBUG(dbgMachine, red << "\n[Stage4 ] Raising Reserved Instruction Exception");
			  SetException ( true ) ;
			  CP0->SignalException( RESERVED_INSTRUCTION );
			  outLatch[4].finished = true;
			  break;
		  }
		  
		switch(outLatch[4].inst.rF.funct)
		{
			case FUNCT_ERET: // The instruction is ERET
				outLatch[4].finished = true;
				/*
				sem_wait ( cout_mutex );
				cout << "\n[Stage4 ] ERET "<< flush;
				sem_post ( cout_mutex );*/

				CP0->SwitchMode( mode );                // Switching from KernelMode to previous mode
				DEBUG(dbgMachine,"\n[Stage4 ] ERET ");
				break;
                        case FUNCT_TLBP:// The instruction is TLBP
				outLatch[4].finished = true;
				/*
                                sem_wait ( cout_mutex );
                                cout << "\n[Stage4 ] TLBP "<< flush;
                                sem_post ( cout_mutex );*/
				DEBUG(dbgMachine,"\n[Stage4 ] TLBP ");
                                break;
                        case FUNCT_TLBR:// The instruction is TLBR
				outLatch[4].finished = true;
				/*
                                sem_wait ( cout_mutex );
                                cout << "\n[Stage4 ] TLBR "<< flush;
                                sem_post ( cout_mutex );*/
				DEBUG(dbgMachine,"\n[Stage4 ] TLBR ");
                                break;
                        case FUNCT_TLBW:// The instruction is TLBW
				//CP0->WriteTLBIndex();
				outLatch[4].finished = true;
				/*
                                sem_wait ( cout_mutex );
                                cout << "\n[Stage4 ] TLBW "<< flush;
                                sem_post ( cout_mutex );*/
				DEBUG(dbgMachine,"\n[Stage4 ] TLBW ");
                                break;

	//------------------------------- 21/03/2006 ---------------------------------//
	//-------------------------------   Rajeesh  ---------------------------------//
	                case FUNCT_MFC0:
	                        RegisterWrite ( outLatch[4].targReg, IDRES );
	                        outLatch[4].finished = true;
				/*
	                        sem_wait ( cout_mutex );
	                        cout << "\n[ Stage4 ] MFC0 stored " << outLatch[4].IDRes
                                << " into r" << outLatch[4].targReg << flush;
	                        sem_post ( cout_mutex );*/
				DEBUG(dbgMachine,"\n[ Stage4 ] MFC0 stored " << outLatch[4].IDRes
						<< " into r" << outLatch[4].targReg );
	                        break;
				
	                case FUNCT_MTC0:
        	                RegisterWrite ( outLatch[4].targReg, IDRES );
                	        outLatch[4].finished = true;
				/*
	                        sem_wait ( cout_mutex );
        	                cout << "\n[ Stage4 ] MTC0 stored " << outLatch[4].IDRes
	                                << " into r" << outLatch[4].targReg << flush;
                	        sem_post ( cout_mutex );*/
				DEBUG(dbgMachine,"\n[ Stage4 ] MTC0 stored " << outLatch[4].IDRes
						<< " into r" << outLatch[4].targReg);
                        	break;
	//-----------------------------------------------------------------------------//
                        
		}        
		
                break;     
		
//-----------------------------------------------------------------------------------------------------//
		
	};
}

bool Processor :: RegisterWrite ( int regNumber, RegisterWriteSource source )
{
	if ( blockUpdate == true ) return false;
	
	word_32 writeValue;
	switch ( source )
	{
	case IDRES:
		writeValue = inLatch[4].IDRes;
		break;
	case ALU:
		writeValue = inLatch[4].ALUOutput;
		break;
	case ALU_HI:
		writeValue = inLatch[4].ALUOutputHi;
		break;
	case LOAD:
		writeValue = inLatch[4].LMD;
		break;
	};
	
	switch ( regNumber )
	{
	case 0: // $zero
		/*
		sem_wait ( cout_mutex );
		cout << red << "\n[ Stage4:RegisterWrite ] instruction attempting"
			<< " to modify $zero, ignoring..." << reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,red << "\n[ Stage4:RegisterWrite ] instruction attempting"
				<< " to modify $zero, ignoring..." << reset);
		return false;		// This register cannot be modified.
	case REG_LO:
		/*
		sem_wait ( cout_mutex );
		cout << violet << "\n[ Stage4:RegisterWrite ] writing value "
			<< writeValue << " to register Lo" << reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,violet << "\n[ Stage4:RegisterWrite ] writing value "
				<< writeValue << " to register Lo" << reset );
		Lo = writeValue;
		break;
	case REG_HI:
		/*
		sem_wait ( cout_mutex );
		cout << violet << "\n[ Stage4:RegisterWrite ] writing value "
			<< writeValue << " to register Hi" << reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,violet << "\n[ Stage4:RegisterWrite ] writing value "
				<< writeValue << " to register Hi" << reset );
		Hi = writeValue;
		break;

//------------------------------- 20/03/2006 ------------------------------------//
		
        case REG_PC:
		/*
                sem_wait ( cout_mutex );
                cout << violet << "\n[ Stage4:RegisterWrite ] writing value "
                        << writeValue << " to register PCreg" << reset << flush;
                sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,violet << "\n[ Stage4:RegisterWrite ] writing value "
				<< writeValue << " to register PCreg" << reset);
                PCreg = writeValue;
                break;
        case REG_NPC:
		/*
                sem_wait ( cout_mutex );
                cout << violet << "\n[ Stage4:RegisterWrite ] writing value "
                        << writeValue << " to register NPCreg" << reset << flush;
                sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,violet << "\n[ Stage4:RegisterWrite ] writing value "
				<< writeValue << " to register NPCreg" << reset );
                NPCreg = writeValue;
                break;
	case REG_INDEX:
		/*
                sem_wait ( cout_mutex );
                cout << violet << "\n[ Stage4:RegisterWrite ] writing value "
	                        << writeValue << " to register Index" << reset << flush;
                sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,violet << "\n[ Stage4:RegisterWrite ] writing value "
				<< writeValue << " to register Index" << reset );
                CP0->Index = writeValue;
                break;
	case REG_ENTRYHI:
		/*
		sem_wait ( cout_mutex );
		cout << violet << "\n[ Stage4:RegisterWrite ] writing value "
			<< writeValue << " to register EntryHi" << reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,violet << "\n[ Stage4:RegisterWrite ] writing value "
				<< writeValue << " to register EntryHi" << reset);
		CP0->EntryHi = writeValue;
		break;
	case REG_ENTRYLO0:
		/*
		sem_wait ( cout_mutex );
		cout << violet << "\n[ Stage4:RegisterWrite ] writing value "
			<< writeValue << " to register EntryLo0" << reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,violet << "\n[ Stage4:RegisterWrite ] writing value "
				<< writeValue << " to register EntryLo0" << reset);
		CP0->EntryLo0 = writeValue;
		break;
	case REG_ENTRYLO1:
		/*
		sem_wait ( cout_mutex );
		cout << violet << "\n[ Stage4:RegisterWrite ] writing value "
			<< writeValue << " to register EntryLo1" << reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,violet << "\n[ Stage4:RegisterWrite ] writing value "
				<< writeValue << " to register EntryLo1" << reset );
		CP0->EntryLo1 = writeValue;
		break;
	
        case REG_CONTEXT:
		/*
                sem_wait ( cout_mutex );
                cout << violet << "\n[ Stage4:RegisterWrite ] writing value "
	                        << writeValue << " to register- Context" << reset << flush;
                sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,violet << "\n[ Stage4:RegisterWrite ] writing value "
				<< writeValue << " to register- Context" << reset);
                CP0->Context = writeValue;
                break;
        case REG_PAGEMASK:
		/*
                sem_wait ( cout_mutex );
                cout << violet << "\n[ Stage4:RegisterWrite ] writing value "
	                        << writeValue << " to register - PageMask" << reset << flush;
                sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,violet << "\n[ Stage4:RegisterWrite ] writing value "
				<< writeValue << " to register - PageMask" << reset);
                CP0->PageMask = writeValue;
                break;
        case REG_BADVADDR:
		/*
                sem_wait ( cout_mutex );
                cout << violet << "\n[ Stage4:RegisterWrite ] writing value "
	                        << writeValue << " to register BadVaddr" << reset << flush;
                sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,violet << "\n[ Stage4:RegisterWrite ] writing value "
				<< writeValue << " to register BadVaddr" << reset );
                CP0->BadVAddr = writeValue;
                break;
        case REG_COUNT:
		/*
                sem_wait ( cout_mutex );
                cout << violet << "\n[ Stage4:RegisterWrite ] writing value "
	                        << writeValue << " to register Count" << reset << flush;
                sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,violet << "\n[ Stage4:RegisterWrite ] writing value "
				<< writeValue << " to register Count" << reset );
                CP0->Count = writeValue;
                break;
        case REG_COMPARE:
                sem_wait ( cout_mutex );
                cout << violet << "\n[ Stage4:RegisterWrite ] writing value "
	                        << writeValue << " to register Compare" << reset << flush;
                sem_post ( cout_mutex );
                CP0->Compare = writeValue;
                break;
        case REG_CAUSE:
		/*
                sem_wait ( cout_mutex );
                cout << violet << "\n[ Stage4:RegisterWrite ] writing value "
	                        << writeValue << " to register Cause" << reset << flush;
                sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,violet << "\n[ Stage4:RegisterWrite ] writing value "
				<< writeValue << " to register Cause" << reset);
                CP0->Cause = writeValue;
                break;
        case REG_EPC:
		/*
                sem_wait ( cout_mutex );
                cout << violet << "\n[ Stage4:RegisterWrite ] writing value "
	                        << writeValue << " to register EPC" << reset << flush;
                sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,violet << "\n[ Stage4:RegisterWrite ] writing value "
				<< writeValue << " to register EPC" << reset );
                CP0->EPC = writeValue;
                break;
	//----- Added on 29/01/2008 by Rajeesh ------//
	case REG_SR:
		DEBUG(dbgMachine,violet << "\n[ Stage4:RegisterWrite ] writing value "
				<< writeValue << " to register StatusReg" << reset );
		CP0->StatusReg = writeValue;
		break;
	//------------------------------------------//
	
//--------------------------------------------------------------------------------------//
		
     	default:
		/*
		sem_wait ( cout_mutex );
		cout << violet << "\n[ Stage4:RegisterWrite ] writing value "
			<< writeValue << " to register r"
			<< regNumber << reset << flush;
		sem_post ( cout_mutex );*/
		DEBUG(dbgMachine,violet << "\n[ Stage4:RegisterWrite ] writing value "
				<< writeValue << " to register r"
				<< regNumber << reset );
		reg[regNumber] = writeValue;
		break;
	}
	return true;
}

