/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/


# ifndef __LATCH_H
# define __LATCH_H

#include "instruction.h"

class Latch
{
public:
	u_word_32 PC;
	Inst inst;
	
	int targReg;
	int targReg2;	// This comes useful in MULT and DIV instructions.

	 
		
	ResultStage resultStage;
	
	word_32 A;
	word_32 B;
	bool dataFetchIncomplete;	// this flag is set if the instruction needs
			// the data only in stage3 and the data will become availabe
			// only in stage2
	RegisterFetchTarget FetchFailedFor;	// contains the destination needing fetch 
			// in stage2 when dataFetchIncomplete == 2
			// This is useful only for the RDOUT instruction.
	word_32 Imm;
	
	word_32 IDRes;
	
	word_32 ALUOutput;
	word_32 ALUOutputHi;
	
	word_32 LMD;
	
	bool finished;
	
	bool IsException; // 25/03/06 this is for noting the exceptions that occur during the instructions	
	void Initialise ( );

	
};

void LatchCopy ( Latch &ldest, Latch &lsource );

# endif
