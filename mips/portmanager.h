/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

 

# ifndef __PORTMANAGER_H
# define __PORTMANAGER_H

#include "types.h"
#include "debug.h"
# define MAX_PORTS 32

class PortManager
{
private:
	char hostname[32];
	int portMap [MAX_PORTS];
public:
	PortManager ( );
	~PortManager ( );
	void AtExit ( );
	int AddPort ( int portNo, int mapsTo );
	int RemovePort ( int portNo );
	int Write ( int portNo, word_32 oneWord );
	int Read ( int portNo, word_32 & oneWord );
};

# endif
