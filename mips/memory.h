/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/


# ifndef __MEMORY_H
# define __MEMORY_H

#include "instruction.h"

#include "translate.h"		// For ExceptionTypr ...

# define TYPEFIELDSIZE 16


class Cache
{
public:
	virtual ~Cache() {};
	virtual void Statistics ( ) = 0;

	virtual ExceptionType Read ( word_32 address, word_32 & result, int noOfBytes ) = 0;
	virtual bool Read_nofetch ( word_32 address, word_32 & result, int noOfBytes ) = 0;
	
	virtual ExceptionType Write ( word_32 address, word_32 value, int noOfBytes ) = 0;
	
	virtual void AtExit ( ) = 0;
};


#include "translate.h"		// For Page Table

class MainMemory : public Cache
{
private:
	char * memory;
	int size;
public:
	MainMemory ( int sz );
	virtual ~MainMemory ();
	
	// The following two functions are provided just to make 
	// MainMemory conform to the Cache interface.
	void Statistics ( );
	bool Read_nofetch ( word_32 address, word_32 & result, int noOfBytes );
	
	ExceptionType Read ( word_32 address, word_32 & result, int noOfBytes );
					//--------------modified----------27/03/2006-------
	
	ExceptionType Write ( word_32 address, word_32 value, int noOfBytes );
					//--------------modified----------27/03/2006-------
	bool Load_MIPS_program ( const char * filename );
	
	void AtExit ( );	

};



class NoCache : public Cache
{
// This is something absolutely unnecessay and does nothing but simply
// acts as an interface between the cache and the memory...
// This is a vestegial class from a previous design, the overhaul
// of which obviated the necessity for this class...
private:
	Cache * mem;
	int accesses;
	
	char type[TYPEFIELDSIZE];
	int level;
public:
	NoCache ( Cache * memory, char * ty, int lev );
	virtual ~NoCache() {};
	void Statistics ( );
	ExceptionType Read ( word_32 address, word_32 & result, int noOfBytes );
	bool Read_nofetch ( word_32 address, word_32 & result, int noOfBytes );
	ExceptionType Write ( word_32 address, word_32 value, int noOfBytes );
	void AtExit ( );
};

# endif

