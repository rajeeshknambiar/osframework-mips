/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

 
//#include "processor.h"

#include <iostream>
using std::cout;
using std::cin;
using std::flush;

#include <iomanip>
using std::setw;
#include <cstdlib>
using std::exit;

#include <signal.h>

#include "color.h"

#include <semaphore.h>
extern sem_t * cout_mutex;	// Defined in main.cpp
// Clock runs when other threads are blocked,
// Therefore there is no need to control couts by 
// mutual exclusion in the clocks...
// But the clock function has the responsibility to 
// close the semaphore when exiting the program...


#include "main.h"
#include "errorhandler.h"


void Processor :: Clock ( int clk )
{	
	if ( requestProgramTermination == true ) {

		// As we are using the 'exit()' to exit from the program,
		// the various destructors don't get invoked...
		// Therefore, instead of a destructor, we have provided
		// an 'AtExit()' functions wherever applicable
		dataCache -> AtExit ( );
		instrCache -> AtExit ( );
		pman -> AtExit ( );

		int cout_mutex_value;
		if ( sem_getvalue ( cout_mutex, &cout_mutex_value ) == -1 )
			cout << red << "\nError polling the value of cout_mutex"
				<< reset << flush;
		else
			cout << gray << "\nThe value of cout_mutex at closing = "
				<< cout_mutex_value <<  reset << flush;

		sem_close ( cout_mutex );
		sem_unlink ( "coutmutex" );	// Close the cout_mutex

		AtExit ( );

		// Kill all the processor pipeline threads  -
	        // This will be done above Processor::AtExit()
		//for ( int i = 0; i < 5; i++ )                                              	
		//	pthread_kill (stagethread[i],SIGTERM);	// Kill all the Threads ...

	    	cout << "\n\n" << reset << flush;
		exit ( -99 );
	}

	
//--------------------------- 09/04/2006 -------------------------//
//---------------------------   Rajeesh  -------------------------//

	CP0 -> Count -- ;	// Decrement Count register
	
	if( CP0 -> Count == 0 ) {
		CP0 -> Count = 10 ;
		SetException( true );

		if( CP0 -> GetInterruptStatus() == false ) {	// Interrupts not allowed
			SetException( false );
			/*
			sem_wait( cout_mutex );
			cout << red << "\n\tTimer - but Interrupt Disabled !!\n" << reset;
			sem_post (cout_mutex );*/
			DEBUG(dbgMachine,red << "\n\tTimer - but Interrupt Disabled. ");
		}
		else {
			CP0 -> SetInterruptStatus( false );	// Disable further interrupts
			//CP0 -> EPC = outLatch[4].PC + 4;
			CP0 -> SignalException( TIMERINTERRUPT ); 
			//SetException( false ); 
		}
	}

//----------------------------------------------------------------//

//	cout << blue << "\n[** Clock: " << clk << " **] Executed..." << reset << flush;

	DEBUG(dbgMachine,blue << "\n[** Clock: " << clk << " **] Executed..." );
		
	for ( int i = 0; i < 5; i++ )
		if ( flushStage[i] == true ) {
			
		//	cout << blue << "\n[** Clock: " << clk << " **] Flushing stage "
		//		<< i << reset << flush;
			DEBUG(dbgMachine, blue << "\n[** Clock: " << clk << " **] Flushing stage "
					<< i );
			inLatch[i].Initialise ( );
			inLatch[i].finished = true;
			outLatch[i].Initialise ( );
			outLatch[i].finished = true;
		}
	
	if ( outLatch[4].finished == true ) {
		if ( outLatch[3].finished == true ) {
			//cout << "\n[** Clock: " << clk 
			//	<< " **] inLatch[4] <- outLatch[3]"
			//	<< flush;
			LatchCopy ( inLatch[4], outLatch[3] );
			
			if ( outLatch[2].finished == true ) {
				//cout << "\n[** Clock: " << clk 
				//	<< " **] inLatch[3] <- outLatch[2]"
				//	<< flush;
				LatchCopy ( inLatch[3], outLatch[2] );
				
				if ( outLatch[1].finished == true ) {
					//cout << "\n[** Clock: " << clk 
					//	<< " **] inLatch[2] <- outLatch[1]"
					//	<< flush;
					LatchCopy ( inLatch[2], outLatch[1] );
					
					if ( outLatch[0].finished == true ) {
						//cout << "\n[** Clock: " << clk 
						//	<< " **] inLatch[1] <- outLatch[0]"
						//	<< flush;
						LatchCopy ( inLatch[1], outLatch[0] );
						inLatch[0].Initialise ( );
						
						NPCfrom = NOT_WRITTEN;
		// If pipeline was stalled at ID, but a branch in EX had completed, then
		// the NPCreg value should be preserved.  NPCfrom is updated only if 
		// there were no stalls.
						PCreg = NPCreg;
					}
					else {
					//	cout << blue << "\n[** Clock: " << clk 
						//	<< " **] inLatch[1].Initialise ( )"
						//	<< reset << flush;
						DEBUG(dbgMachine,blue << "\n[** Clock: " << clk
								<< " **] inLatch[1].Initialise ( )");
						inLatch[1].Initialise ( );
					}
				}
				else {
				//	cout << blue << "\n[** Clock: " << clk 
				//		<< " **] inLatch[2].Initialise ( )"
				//		<< reset << flush;
					DEBUG(dbgMachine,blue << "\n[** Clock: " << clk
							<< " **] inLatch[2].Initialise ( )");
					inLatch[2].Initialise ( );
				}
			}
			else {
				
			//	cout << blue << "\n[** Clock: " << clk 
			//		<< " **] inLatch[3].Initialise ( )"
			//		<< reset << flush;
				DEBUG(dbgMachine,blue << "\n[** Clock: " << clk
						<< " **] inLatch[3].Initialise ( )");
				inLatch[3].Initialise ( );
			}
		}
		else {
//			cout << blue << "\n[** Clock: " << clk 
//				<< " **] inLatch[4].Initialise ( )"
//				<< reset << flush;
			DEBUG(dbgMachine,blue << "\n[** Clock: " << clk
					<< " **] inLatch[4].Initialise ( )");
			inLatch[4].Initialise ( );
		}
	}
	
	for ( int i = 0; i < 5; i++ ) {
		outLatch[i].Initialise ( );
		flushStage[i] = false;
	}
	// The above is required because latchcopy from inlatch to outlatch
	// happens only once the stage thread got a chance to run
	
	if ( continueCount > 0 ) {
		continueCount --;
		for ( int i = 0; i < BREAKPOINTARRAYSIZE; i++ ) {
			if ( PCreg == static_cast<u_word_32>(breakPointArray[i]) ) {
				continueCount = 0;
				break;
			}
		}
	}
	else if ( continueCount < 0 ) {
//		cout << red << "\n[** Clock: " << clk 
//			<< " **] negative continue count, resetting" 
//			<< reset << flush;
		DEBUG(dbgMachine,red << "\n[** Clock: " << clk
				<< " **] negative continue count, resetting");
		continueCount = 0;
	}
	
	cout << "\n" << flush;

	
	
//--------------------------- 17/01/2006 -------------------------//
//---------------------------- Rajeesh ---------------------------//
	
/* 
	if ( continueCount == 0 )
	{
		

	We would like to execute the machine without any human intervention
	or prompt, in the perspective of an operating system. But when omitting
	all kinds of prompts, we should tell the machine exactly when to stop
	the execution.
	When we find no instruction in all the valid input latches, their 
	inst.iV will contain 0, which clearly indicates that all the executable
	instructions have been fetched and executed. But, this will easily fail
 	when you purposely put 5 NOPs in the mips program(Find out an alternative way!!!).
	
	But, we don't have to be concerned with halting the execution since the machine
	should go on until the power is out(literally), or an explicit 'Quit' message
	receives. This explicit Terminate() call could be made by the Operating System
	once it is implemented; which means, the following code snippet is unnecessary
	in that situation.	
*/	
	
//		if( inLatch[1].inst.iV == 0 && inLatch[2].inst.iV == 0 && 
//				inLatch[3].inst.iV == 0 && inLatch[4].inst.iV == 0 && PCreg > 1024 )
//			Terminate();

//-----------------------------------------------------------------//
		
	
 /*		
		char ch;
		do
		{
			cout << blue << "\nmips > " << reset << flush;
			cin >> ch;
			switch ( ch )
			{
			case 'p':	// small p
				cout << blue << "\nRegister Values are printed below." 
					<< reset << flush;
				for ( int i = 0; i < 32 ; i++ )
				{
					cout << "\n  r" << setw (2) << i << ": value = " 
						<< reg[i] << flush;
				}
				cout << "\n  Lo : value = " << Lo << flush;
				cout << "\n  Hi : value = " << Hi << flush;
				cout << "\n  PC : value = " << PCreg << flush;
				break;

			case 'P':	// capital P
				cout << blue << "\nNon-zero Register Values"
					<< " are printed below."
					<< reset << flush;
				for ( int i = 0; i < 32 ; i++ )
				{
					if ( reg[i]!= 0 )
						cout << "\n  r" << setw (2) << i 
							<< ": value = " 
							<< reg[i] << flush;
				}
				if ( Lo != 0 ) cout << "\n  Lo : value = " << Lo << flush;
				if ( Hi != 0 ) cout << "\n  Hi : value = " << Hi << flush;
				cout << "\n  PC : value = " << PCreg << flush;	
						// PC is always printed
				break;

			case 'q':
				Terminate ( );
				//requestProgramTermination = true;

			case 'n':
				break;	// Do nothing... Avoid going into "default"

			case 'm': {
				int address;
				cin >> address;

				word_32 result;
				if ( mem -> Read ( address, result, 4 ) == true )
					cout << blue << "\nMemory[" << address << "] = " 
						<< result << " " 
						<< static_cast<char>(result) 
						<< reset << flush;
				else cout << red << "\nMemory read failed" << reset << flush;

				break;
				}

			case 'd': {
				int address;
				cin >> address;

				word_32 result;
				if ( dataCache -> Read_nofetch ( address, result, 4 ) 
						== true )
					cout << blue << "\ndataCache[" << address << "] = " 
						<< result << reset << flush;
				else cout << red << "\ndataCache read missed" 
					<< reset << flush;

				break;
				}

			case 'i': {
				int address;
				cin >> address;

				word_32 result;
				if ( instrCache -> Read_nofetch ( address, result, 4 ) 
						== true )
					cout << blue << "\ninstrCache[" << address << "] = " 
						<< result << reset << flush;
				else cout << red << "\ninstrCache read missed" 
					<< reset << flush;

				break;
				}

			case 's':
				cout << blue << "\ndataCache Statistics : " 
					<< reset << flush;
				dataCache -> Statistics ( );
				cout << blue << "\ninstrCache Statistics : " 
					<< reset << flush;
				instrCache -> Statistics ( );
				break;
				
			case 'c': cin >> continueCount;
				break;
				
			case 'b':{
				int breakPointIndex;
				cin >> breakPointIndex;
				cin >> breakPointArray[breakPointIndex];
				break;
				}
				
			case 'B':
				cout << blue << "\n[** Clock: " << clk 
					<< " **] The break points are listed below\n"
					<< reset << flush;
				for ( int i = 0; i < BREAKPOINTARRAYSIZE; i ++ )
					if ( breakPointArray[i] != -1 )
						cout << "\t" << i << "\t" 
							<< breakPointArray[i] << "\n";
				cout << flush;
				break;

			default:
				cout << red << "\n[** Clock: " << clk 
					<< " **] Unrecognised command,"
					<< " Ignoring... " << reset << flush;
				continueCount = 0;
				break;
			};
		}
		while ( ch != 'q' && ch != 'n' && ch != 'c' );
	} 
	       	*/
}
