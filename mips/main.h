/********************************************************************************
 *										*
 *	Copyright (c) 2007, 2008 Rajeesh					*
 *	Copyright (c) 2006 Rajeesh, Dinesh, Thamanna				*
 *	Copyright (c) 2005 Varghese Mathew					*
 *										*
 *	This program is free software; you can redistribute it and/or		*
 *	modify it under the terms of the GNU General Public License		*
 *	as published by the Free Software Foundation; either			*
 *	version 3 of the License, or (at your option) any later version.	*
 *										*
 *	This program is distributed in the hope that it will be useful,		*
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of		*
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU	*
 *	General Public License for more details.				*
 *										*
 ********************************************************************************/

//------------------------- 20/01/2006 ------------------------//
//-------------------------   Rajeesh  ------------------------//

/*
 *  This file is part of the source code of the project done by Rajeesh, Dinesh and Thamanna
 *  during 2005 December- 2006 April. This file or any part of it could be used,
 *  modified or reproduced by strictly adhering to the terms and conditions in "copyleft.h"
 */

#include "copyleft.h"
#include "addressspace.h"

#ifndef __MAIN_H
#define __MAIN_H

/*
   This header file serve the purpose of collecting all the
   frequently used or accessed kernel datastructures and
   other system components.
*/

#include "memory.h"
#include "processor.h"
#include "portmanager.h"
#include "translate.h"
#include <semaphore.h>

#include "kernel.h"
#include "thread.h"


extern MainMemory *mem;			// main.cpp
extern AddressSpace *addrSpace;		// main.cpp
extern Processor *proc;			// main.cpp
extern PortManager *pMan;		// main.cpp
extern SystemControlCoprocessor *CP0;	// main.cpp

extern u_word_32 RegisterArray[50];	// main.cpp
extern TranslationEntry TLBValues;	// main.cpp

extern word_32 StartingAddress;		// main.cpp

extern sem_t * cout_mutex;		// main.cpp

extern Status mode;			// processor.cpp

extern Kernel * kernel;			//kernel.cpp

extern Scheduler * scheduler;		//scheduler.cpp

extern Thread *thread;			//thread.cpp

//extern int clock_count = 0;			// processor.cpp

#endif
